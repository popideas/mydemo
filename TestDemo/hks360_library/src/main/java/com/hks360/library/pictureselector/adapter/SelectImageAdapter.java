package com.hks360.library.pictureselector.adapter;

import android.content.Context;
import android.os.Vibrator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.Headers;
import com.bumptech.glide.request.RequestOptions;
import com.hks360.library.R;
import com.hks360.library.pictureselector.ImageGalleryActivity;
import com.hks360.library.pictureselector.util.CollectionUtil;
import com.hks360.library.pictureselector.widget.PicturesPreviewerItemTouchCallback;
import com.hks360.library.util.CommonUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author by JuQiu
 * on 16/7/15.
 */
public class SelectImageAdapter extends RecyclerView.Adapter<SelectImageAdapter.SelectImageHolder> implements PicturesPreviewerItemTouchCallback.ItemTouchHelperAdapter {
    private final int MAX_SIZE = 9;
    private final int TYPE_NONE = 0;
    private final int TYPE_ADD = 1;
    private final List<Model> mModels = new ArrayList<>();
    private Callback mCallback;
    private boolean isPreviewMode;
    private static Headers mHeaders = new Headers() {
        @Override
        public Map<String, String> getHeaders() {
            return CommonUtil.getHeaderMap();
        }
    };

    public SelectImageAdapter(Callback callback) {
        mCallback = callback;
    }

    @Override
    public int getItemViewType(int position) {
        int size = mModels.size();
        if (size >= MAX_SIZE) {
            return TYPE_NONE;
        } else if (position == size) {
            return TYPE_ADD;
        } else {
            return TYPE_NONE;
        }
    }

    @Override
    public SelectImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pictureselector_item_list_picture_selecter, parent, false);
        if (viewType == TYPE_NONE) {
            return new SelectImageHolder(view, new SelectImageHolder.HolderListener() {
                @Override
                public void onDelete(Model model) {
                    Callback callback = mCallback;
                    if (callback != null) {
                        int pos = mModels.indexOf(model);
                        if (pos == -1) {
                            return;
                        }
                        mModels.remove(pos);
                        if (mModels.size() > 0) {
                            notifyItemRemoved(pos);
                        } else {
                            notifyDataSetChanged();
                        }
                    }
                }

                @Override
                public void onDrag(SelectImageHolder holder) {
                    Callback callback = mCallback;
                    if (callback != null) {
                        // Start a drag whenever the handle view it touched
                        mCallback.onStartDrag(holder);
                    }
                }

                @Override
                public void onClick(Model model) {
                    ImageGalleryActivity.show(mCallback.getContext(), model.path, false);
                }
            });
        } else {
            return new SelectImageHolder(view, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Callback callback = mCallback;
                    if (callback != null) {
                        callback.onLoadMoreClick();
                    }
                }
            });
        }
    }

    @Override
    public void onBindViewHolder(final SelectImageHolder holder, int position) {
        int size = mModels.size();
        if (size >= MAX_SIZE || size != position) {
            Model model = mModels.get(position);
            holder.bind(position, isPreviewMode, model, mCallback.getImgLoader());
        }
    }

    @Override
    public void onViewRecycled(SelectImageHolder holder) {
        if (mCallback != null) {
            mCallback.getImgLoader().clear(holder.mImage);
        }
    }

    @Override
    public int getItemCount() {
        int size = mModels.size();
        if (isPreviewMode) {
            return size;
        }
        if (size == MAX_SIZE) {
            return size;
        } else if (size == 0) {
            return 1;
        } else {
            return size + 1;
        }
    }

    public void clear() {
        mModels.clear();
    }

    public void add(Model model) {
        if (mModels.size() >= MAX_SIZE) {
            return;
        }
        mModels.add(model);
    }

    /**
     * 是否是预览模式
     */
    public void setPreviewMode() {
        isPreviewMode = true;
    }

    public void add(String path) {
        add(new Model(path));
    }

    public String[] getPaths() {
        int size = mModels.size();
        if (size == 0) {
            return null;
        }
        String[] paths = new String[size];
        int i = 0;
        for (Model model : mModels) {
            paths[i++] = model.path;
        }
        return paths;
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        //Collections.swap(mModels, fromPosition, toPosition);
        if (fromPosition == toPosition) {
            return false;
        }

        // Move fromPosition to toPosition
        CollectionUtil.move(mModels, fromPosition, toPosition);

        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        mModels.remove(position);
        notifyItemRemoved(position);
    }

    public static class Model {
        public Model(String path) {
            this.path = path;
        }

        public String path;
        public boolean isUpload;
    }

    public interface Callback {
        void onLoadMoreClick();

        RequestManager getImgLoader();

        Context getContext();

        /**
         * Called when a view is requesting a start of a drag.
         *
         * @param viewHolder The holder of the view to drag.
         */
        void onStartDrag(RecyclerView.ViewHolder viewHolder);
    }

    /**
     * TweetSelectImageHolder
     */
    static class SelectImageHolder extends RecyclerView.ViewHolder implements PicturesPreviewerItemTouchCallback.ItemTouchHelperViewHolder {
        private ImageView mImage;
        private ImageView mDelete;
        private ImageView mGifMask;
        private HolderListener mListener;

        private SelectImageHolder(View itemView, HolderListener listener) {
            super(itemView);
            mListener = listener;
            mImage = itemView.findViewById(R.id.iv_content);
            mDelete = itemView.findViewById(R.id.iv_delete);
            mGifMask = itemView.findViewById(R.id.iv_is_gif);

            mDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Object obj = v.getTag();
                    final HolderListener holderListener = mListener;
                    if (holderListener != null && obj != null && obj instanceof SelectImageAdapter.Model) {
                        holderListener.onDelete((SelectImageAdapter.Model) obj);
                    }
                }
            });
            mImage.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    final HolderListener holderListener = mListener;
                    if (holderListener != null) {
                        holderListener.onDrag(SelectImageHolder.this);
                    }
                    return true;
                }
            });
            mImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Object obj = mDelete.getTag();
                    final HolderListener holderListener = mListener;
                    if (holderListener != null && obj != null && obj instanceof SelectImageAdapter.Model) {
                        holderListener.onClick((SelectImageAdapter.Model) obj);
                    }
                }
            });
            mImage.setBackgroundColor(0xffdadada);
        }

        private SelectImageHolder(View itemView, View.OnClickListener clickListener) {
            super(itemView);

            mImage = itemView.findViewById(R.id.iv_content);
            mDelete = itemView.findViewById(R.id.iv_delete);

            mDelete.setVisibility(View.GONE);
            mImage.setImageResource(R.drawable.pictureselector_ic_add);
            mImage.setOnClickListener(clickListener);
            mImage.setBackgroundDrawable(null);
        }

        public void bind(int position, boolean isPreviewMode, SelectImageAdapter.Model model, RequestManager loader) {
            mDelete.setTag(model);
            // In this we need clear before load
            loader.clear(mImage);
            // Load image

            if (isPreviewMode) {
                mDelete.setVisibility(View.GONE);
            } else {
                mDelete.setVisibility(View.VISIBLE);
            }

            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .error(R.drawable.pictureselector_ic_split_graph);

            if (model.path.toLowerCase().endsWith("pictureselector_gif")) {
                loader.asBitmap().load(model.path)
                        .apply(options)
                        .into(mImage);
                // Show pictureselector_gif mask
                mGifMask.setVisibility(View.VISIBLE);
            } else if (model.path.startsWith("http")) {
                loader.asBitmap().load(new GlideUrl(model.path, mHeaders))
                        .apply(options).into(mImage);
                mGifMask.setVisibility(View.GONE);
            } else {
                loader.load(model.path)
                        .apply(options)
                        .into(mImage);
                mGifMask.setVisibility(View.GONE);
            }
        }


        @Override
        public void onItemSelected() {
            try {
                Vibrator vibrator = (Vibrator) itemView.getContext().getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(20);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onItemClear() {

        }

        /**
         * Holder 与Adapter之间的桥梁
         */
        interface HolderListener {
            void onDelete(SelectImageAdapter.Model model);

            void onDrag(SelectImageHolder holder);

            void onClick(SelectImageAdapter.Model model);
        }
    }

}
