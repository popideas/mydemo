package com.hks360.library.util;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.text.TextUtils;

/**
 * @author by Xiaote<br>
 * 系统剪切板
 */
public class ClipBoardUtil {
    private static final String LABEL = "content";

    /**
     * 复制文本到剪切板
     */
    public static void copyText(Context context, String content) {
        //获取剪切板对象
        ClipboardManager cmb = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText(LABEL, content);
        cmb.setPrimaryClip(clipData);
    }

    /**
     * 实现粘贴功能
     */
    public static String pasteText(Context context) {
        String text = null;
        //获取剪切板对象
        ClipboardManager cmb = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        if (cmb.hasPrimaryClip()) {
            ClipData clipData = cmb.getPrimaryClip();
            if (clipData != null && clipData.getItemCount() > 0) {
                ClipData.Item item = clipData.getItemAt(0);
                if (item != null && !TextUtils.isEmpty(item.getText())) {
                    text = item.getText().toString();
                }
            }
        }
        return text;
    }

    /**
     * 清空剪切板
     */
    public static void clear(Context context) {
        ClipboardManager cmb = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        cmb.setPrimaryClip(ClipData.newPlainText(null, ""));
    }
}
