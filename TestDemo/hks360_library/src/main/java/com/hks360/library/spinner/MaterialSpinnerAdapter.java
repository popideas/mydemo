/*
 * Copyright (C) 2016 Jared Rummler
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hks360.library.spinner;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hks360.library.R;

import java.util.List;

public class MaterialSpinnerAdapter<T> extends BaseQuickAdapter<T, BaseViewHolder> {
    private int textColor;
    private int backgroundSelected;

    public MaterialSpinnerAdapter(List<T> items) {
        super(R.layout.ms_list_item, items);
    }

    public MaterialSpinnerAdapter setTextColor(int textColor) {
        this.textColor = textColor;
        return this;
    }

    public MaterialSpinnerAdapter setBackgroundResource(int backgroundSelected) {
        this.backgroundSelected = backgroundSelected;
        return this;
    }

    @Override
    protected void convert(BaseViewHolder helper, T item) {
        helper.setText(R.id.tv_tinted_spinner, getItemText(item));
        if (backgroundSelected != 0) {
            helper.setBackgroundRes(R.id.tv_tinted_spinner, backgroundSelected);
        }
        if (textColor != 0) {

            helper.setTextColor(R.id.tv_tinted_spinner, textColor);
        }
    }

    public String getItemText(T item) {
        return item.toString();
    }
}