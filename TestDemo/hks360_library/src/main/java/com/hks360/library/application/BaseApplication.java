package com.hks360.library.application;

import android.app.Application;
import android.content.Context;

/**
 * @author Xiaote
 * @date 2018-12-26 17：15
 * @email xiao_t@hks360.com
 */
public class BaseApplication extends Application {
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static Context getContext() {
        return mContext;
    }
}
