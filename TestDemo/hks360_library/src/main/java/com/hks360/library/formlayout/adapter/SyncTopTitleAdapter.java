package com.hks360.library.formlayout.adapter;

import android.support.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hks360.library.R;

/**
 * @author ：xiaote
 * @date ：2020-07-11 10：40
 * 邮箱   ：xiao_t@hks360.com
 */
public class SyncTopTitleAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public SyncTopTitleAdapter() {
        super(R.layout.sync_item_top_title);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, String item) {
        helper.setText(R.id.top_title_text, item);
    }
}
