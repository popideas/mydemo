package com.hks360.library.manager;

import android.app.Activity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AppManager {
	private static AppManager instance;
	private List<Activity> activities;
	
	
	private AppManager() {
		activities = new ArrayList<>();
	}
	
	public static AppManager getInstance() {
		if(instance == null) {
			instance = new AppManager();
		}
		return instance;
	}
	
	public static void release() {
		instance = null;
	}
	
	public void addActivity(Activity activity) {
		activities.add(activity);
	}

	public void clearActivities() {
		if(activities != null) {
			activities.clear();
		}
	}

	public void exitActivity() {
		Iterator<Activity> iterator = activities.iterator();
		while(iterator.hasNext()) {
			Activity activity = iterator.next();
			activity.finish();
		}
		activities.clear();
	}
}
