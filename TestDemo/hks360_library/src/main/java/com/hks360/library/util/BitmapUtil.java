package com.hks360.library.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;

import com.hks360.library.xlog.XLog;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class BitmapUtil {

    public static Bitmap scale(Bitmap bm, int width, int height, boolean keep) {
        // 获得图片的宽高
        int w = bm.getWidth();
        int h = bm.getHeight();
        // 计算缩放比例
        float scaleWidth = ((float) width) / w;
        float scaleHeight = ((float) height) / h;
        if (keep) {
            // float s = Math.max(scaleWidth, scaleHeight);
            // scaleWidth = s;
            // scaleHeight = s;
            scaleWidth = scaleHeight;
        }
        // 取得想要缩放的matrix参数
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        // 得到新的图片
        Bitmap newbm = Bitmap.createBitmap(bm, 0, 0, w, h, matrix, true);
        return newbm;
    }

    public static File scale(File orifile, File descFile, int sampleSize) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = sampleSize;
        Bitmap bitmap = BitmapFactory.decodeFile(orifile.getAbsolutePath(), options);

        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(descFile));
            bitmap.compress(CompressFormat.JPEG, 80, bos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            bitmap.recycle();
        }

        return descFile;
    }

    public static int computeSampleSize(File file, int maxsize) {
        int initialSize = (int) ((float) (file.length() / 1000) / (float) maxsize + 0.5);
        int roundedSize;
        if (initialSize <= 8) {
            roundedSize = 1;
            while (roundedSize < initialSize) {
                roundedSize <<= 1;
            }
        } else {
            roundedSize = (initialSize + 7) / 8 * 8;
        }
        return roundedSize;
    }

    public static File checkFile(Context ctx, File file, String name) {
        int sampleSize = computeSampleSize(file, 1000);

        File cache = ctx.getExternalCacheDir();
        File descFile = new File(cache.getAbsolutePath() + "/upload_temp_" + name + ".jpg");
        if (sampleSize > 1) {
            scale(file, descFile, sampleSize);
            return descFile;
        } else {
            return file;
        }
    }

    // 缩放图片
    public static Bitmap zoomImg(Bitmap bm, int newWidth, int newHeight) {
        if (bm == null)
            return null;
        // 获得图片的宽高
        int width = bm.getWidth();
        int height = bm.getHeight();
        // 计算缩放比例
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 取得想要缩放的matrix参数
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        // 得到新的图片
        Bitmap newbm = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true);
        return newbm;
    }

    // 缩放图片
    public static Bitmap zoomImg(Bitmap bm, int max) {
        // 获得图片的宽高
        int width = bm.getWidth();
        int height = bm.getHeight();
        // 计算缩放比例
        float scaleWidth = 1;
        float scaleHeight = 1;
        if (width > height) {
            scaleWidth = ((float) max) / width;
            scaleHeight = scaleWidth;
        } else {
            scaleHeight = scaleWidth = ((float) max) / height;
        }
        // 取得想要缩放的matrix参数
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        // 得到新的图片
        Bitmap newbm = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true);
        return newbm;
    }

    public static void saveToFile(Bitmap bitmap, String path) {
        if (bitmap == null) {
            XLog.e("图片不存在");
            return;
        }
        File f = new File(path);
        FileOutputStream fOut = null;
        try {
            f.createNewFile();
            fOut = new FileOutputStream(f);
            bitmap.compress(CompressFormat.JPEG, 100, fOut);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fOut.flush();
                fOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getPhotoBase64(Bitmap bitmap, int quality) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, stream);
        byte[] bytes = stream.toByteArray();
        // 将图片流以字符串形式存储下来
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    public static String getPhotoBase64NoWrap(Bitmap bitmap, int quality) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, stream);
        byte[] bytes = stream.toByteArray();
        // 将图片流以字符串形式存储下来
        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }

    public static Bitmap getBitmapByBase64(String base64Str) {
        byte[] bytes = Base64.decode(base64Str, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }
}
