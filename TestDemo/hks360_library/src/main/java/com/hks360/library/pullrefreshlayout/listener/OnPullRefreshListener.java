package com.hks360.library.pullrefreshlayout.listener;

import com.hks360.library.pullrefreshlayout.PullRefreshLayout;

/**
 * @author by Xiaote<br>
 */
public abstract class OnPullRefreshListener implements PullRefreshLayout.OnRefreshListener {
    @Override
    public void onLoading() {

    }
}
