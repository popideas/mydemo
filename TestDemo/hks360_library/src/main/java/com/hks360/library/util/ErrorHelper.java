package com.hks360.library.util;

import android.content.Context;
import android.net.ParseException;

import com.google.gson.JsonParseException;
import com.hks360.library.rxeasyhttp.exception.ApiException;
import com.hks360.library.xlog.XLog;

import org.json.JSONException;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * @author Xiaote
 * @date 2018-08-15 14：35
 * @email xiao_t@hks360.com
 */
public class ErrorHelper {
    public static void onError(Context context, Throwable throwable) {
        try {
            if (throwable != null) {
                XLog.e("errorMsg:" + throwable.getMessage());
                if (throwable instanceof SocketTimeoutException) {
                    ToastUtil.showToast(context, "服务器连接超时");
                } else if (throwable instanceof ConnectException || throwable instanceof UnknownHostException) {
                    ToastUtil.showToast(context, "服务器连接失败");
                } else if (throwable instanceof JsonParseException
                        || throwable instanceof JSONException
                        || throwable instanceof ParseException) {
                    ToastUtil.showToast(context, "数据解析异常");
                } else {
                    ToastUtil.showToast(context, "服务器错误");
                }
            }
        } catch (Exception e1) {

        }
    }

    public static void onError(Context context, ApiException e) {
        ToastUtil.showToast(context, e.getMessage());
    }
}
