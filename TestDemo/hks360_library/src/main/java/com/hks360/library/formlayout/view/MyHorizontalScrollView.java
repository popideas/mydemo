package com.hks360.library.formlayout.view;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.ViewConfiguration;
import android.widget.HorizontalScrollView;


/**
 * @author ：xiaote
 * @date ：2020-07-08 15：03
 * 邮箱   ：xiao_t@hks360.com
 */
public class MyHorizontalScrollView extends HorizontalScrollView {
    private int downX;
    private int downY;
    private int mTouchSlop;

    public MyHorizontalScrollView(Context context) {
        super(context);
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public MyHorizontalScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public MyHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MyHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }


    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);

        if (mScrollViewListener != null) {
            mScrollViewListener.onScrollChange(l, t, oldl, oldt);
        }
    }

    private onScrollViewListener mScrollViewListener;

    public void setOnScrollViewListener(onScrollViewListener listener) {
        this.mScrollViewListener = listener;
    }

    public interface onScrollViewListener {
        void onScrollChange(int x, int y, int oldx, int oldy);

    }
}
