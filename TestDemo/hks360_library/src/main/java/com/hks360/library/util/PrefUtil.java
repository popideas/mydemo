package com.hks360.library.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class PrefUtil {
    private final static String PREF_TAG = "pref_hks";
    private static SharedPreferences pref;

    /**
     * 初始化SharedPreferences
     */
    public static void init(Context context) {
        init(context, PREF_TAG);
    }

    public static void init(Context context, String prefTag) {
        pref = context.getSharedPreferences(prefTag, Context.MODE_PRIVATE);
    }

    /**
     * 保存字符串数据
     */
    public static boolean putString(String key, String value) {
        return pref.edit().putString(key, value).commit();
    }


    /**
     * 获取字符串数据
     */
    public static String getString(String key, String defValue) {
        return pref.getString(key, defValue);
    }

    /**
     * 保存整型数据
     */
    public static boolean putInt(String key, int value) {
        return pref.edit().putInt(key, value).commit();
    }

    /**
     * 保存整型数据
     */
    public static int getInt(String key, int defValue) {
        return pref.getInt(key, defValue);
    }


    /**
     * 保存长整型数据
     */
    public static boolean putLong(String key, long value) {
        return pref.edit().putLong(key, value).commit();
    }

    /**
     * 获取长整型数据
     */
    public static long getLong(String key, long defValue) {
        return pref.getLong(key, defValue);
    }

    /**
     * 保存布尔值数据
     */
    public static boolean putBoolean(String key, boolean value) {
        return pref.edit().putBoolean(key, value).commit();
    }

    /**
     * 获取布尔值数据
     */
    public static boolean getBoolean(String key, boolean defValue) {
        return pref.getBoolean(key, defValue);
    }


    /**
     * 保存浮点型数据
     */
    public static boolean putFloat(String key, float value) {
        return pref.edit().putFloat(key, value).commit();
    }

    /**
     * 获取浮点型数据
     */
    public static float getFloat(String key, float defValue) {
        return pref.getFloat(key, defValue);
    }

    /**
     * 保存String Set集合数据
     */
    public static boolean putSetString(String key, Set<String> value) {
        return pref.edit().putStringSet(key, value).commit();
    }

    public static Set<String> getSetString(String key, Set<String> defValue) {
        return pref.getStringSet(key, defValue);
    }

    public static boolean hasKey(final String key) {
        return pref.contains(key);
    }

    public static boolean removeKey(String key) {
        return pref.edit().remove(key).commit();
    }

    public Map<String, ?> getAll() {
        return pref.getAll();
    }

    public static boolean removeAll() {
        Map<String, ?> map = pref.getAll();
        if (!CommonUtil.isEmpty(map)) {
            SharedPreferences.Editor editor = pref.edit();
            Iterator<String> iterator = map.keySet().iterator();
            while (iterator.hasNext()) {
                String key = iterator.next();
                editor.remove(key);
            }
            return editor.commit();
        }
        return false;
    }

}
