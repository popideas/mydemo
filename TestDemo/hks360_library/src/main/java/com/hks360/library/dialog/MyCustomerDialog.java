package com.hks360.library.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.hks360.library.R;

/**
 * @author ：xiaote
 * @date ：2020-07-15 09：46
 * 邮箱   ：xiao_t@hks360.com
 */
public class MyCustomerDialog {
    private AlertDialog mAlertDialog;
    private TextView tvTitle;
    private TextView tvMessage;
    private TextView tvCancel;
    private TextView tvConfirm;

    public MyCustomerDialog(Context context) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.dialog_confirm_cancel, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(rootView);
        tvTitle = rootView.findViewById(R.id.tv_title);
        tvMessage = rootView.findViewById(R.id.tv_message);
        tvCancel = rootView.findViewById(R.id.tv_negative);
        tvConfirm = rootView.findViewById(R.id.tv_positive);
        mAlertDialog = builder.create();
    }

    public MyCustomerDialog setTitle(String title) {
        tvTitle.setText(title);
        return this;
    }

    public MyCustomerDialog setTitleTextColor(int color) {
        tvTitle.setTextColor(color);
        return this;
    }

    public MyCustomerDialog setTextSize(float textSize) {
        tvTitle.setTextSize(textSize);
        return this;
    }

    public MyCustomerDialog setMessage(String message) {
        tvMessage.setText(message);
        return this;
    }

    public MyCustomerDialog setMessageTextColor(int color) {
        tvMessage.setTextColor(color);
        return this;
    }

    public MyCustomerDialog setMessageTextSize(float textSize) {
        tvMessage.setTextSize(textSize);
        return this;
    }

    public MyCustomerDialog setMessageGravity(int gravity) {
        tvMessage.setGravity(gravity);
        return this;
    }

    public MyCustomerDialog setPositiveButton(String text) {
        tvConfirm.setText(text);
        return this;
    }

    public MyCustomerDialog setPositiveButtonTextColor(int color) {
        tvConfirm.setTextColor(color);
        return this;
    }

    public MyCustomerDialog setPositiveButtonTextSize(float textSize) {
        tvConfirm.setTextSize(textSize);
        return this;
    }

    public MyCustomerDialog setNegativeButton(String text, View.OnClickListener listener) {
        tvCancel.setText(text);
        tvCancel.setOnClickListener(listener);
        return this;
    }

    public MyCustomerDialog setNegativeTextColor(int color) {
        tvCancel.setTextColor(color);
        return this;
    }

    public MyCustomerDialog setNegativeTextSize(float textSize) {
        tvCancel.setTextSize(textSize);
        return this;
    }

    public MyCustomerDialog setCancelAble(boolean cancelAble) {
        mAlertDialog.setCancelable(cancelAble);
        mAlertDialog.setCancelable(cancelAble);
        return this;
    }

    public MyCustomerDialog setOnCancelListener(DialogInterface.OnCancelListener cancelListener) {
        mAlertDialog.setOnCancelListener(cancelListener);
        return this;
    }

    public MyCustomerDialog setConfirmClickListener(View.OnClickListener listener) {
        tvConfirm.setOnClickListener(listener);
        return this;
    }

    public MyCustomerDialog setCancelClickListener(View.OnClickListener listener) {
        tvCancel.setOnClickListener(listener);
        return this;
    }

    public MyCustomerDialog setOnClickListener(final DialogOnClickListener listener) {
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if(listener != null) {
                    listener.onCancel();
                }
            }
        });
        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if(listener != null) {
                    listener.onConfirm();
                }
            }
        });
        return this;
    }

    public void show() {
        if (mAlertDialog != null) {
            mAlertDialog.show();
        }
    }

    public void dismiss() {
        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
        }
    }


    public interface DialogOnClickListener {
        void onConfirm();

        void onCancel();
    }
}
