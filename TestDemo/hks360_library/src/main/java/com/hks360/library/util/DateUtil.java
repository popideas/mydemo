package com.hks360.library.util;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressLint("SimpleDateFormat")
public class DateUtil {

    public static String parseDate(long time, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(new Date(time));
    }

    public static String parseDate(String timeStr, String pattern) {
        if (CommonUtil.isEmpty(timeStr)) {
            return "";
        }
        long timeMillis = Long.parseLong(timeStr);
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(new Date(timeMillis));
    }

    public static long parseTime(String dateStr, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            return sdf.parse(dateStr).getTime();
        } catch (ParseException e) {
            return 0L;
        }
    }

    /**
     * 时间戳 转年月日 时分秒
     */
    public static String parseDateTimeByTimestamp(long time) {
        return parseDate(time * 1000L, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 时间戳 转年月日 时分秒
     */
    public static String parseDateTimeByTimeMillis(long time) {
        return parseDate(time, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 时间戳 转年月日
     */
    public static String parseDateByTimestamp(long time) {
        return parseDate(time * 1000L, "yyyy-MM-dd");
    }

    /**
     * 毫秒值 转成年月日
     */
    public static String parseDateByTimeMillis(long time) {
        return parseDate(time, "yyyy-MM-dd");
    }

    /**
     * 字符串日期转成秒值
     */
    public static long parseTimestampByDate(String dateStr) {
        return parseTime(dateStr, "yyyy-MM-dd HH:mm:ss") / 1000;
    }

    /**
     * 字符串日期转毫秒
     */
    public static long parseTimeMillisByDate(String dateStr) {
        return parseTime(dateStr, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 当前系统时间转成年月日
     */
    public static String parseDateByCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(new Date());
    }

    /**
     * 当前系统时间转成年月日时分
     */
    public static String parseDateByCurrentTime1() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sdf.format(new Date());
    }

    /**
     * 毫秒值转成时分秒
     */
    public static String parseSecondByTimeMillis(long time) {
        return parseDate(time, "HH:mm:ss");
    }

    /**
     * 获取本月第一天
     */
    public static String parseDateMonthFirstDay() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-01");
        return sdf.format(new Date());
    }

    /**
     * 日期解析成毫秒值
     */
    public static long parseStrToDateTime(String dateStr, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            return sdf.parse(dateStr).getTime();
        } catch (ParseException e) {
            return 0L;
        }
    }

    /**
     * 根据年月获取当月的最大的一天
     */
    public static int getMaxDay(int year, int month) {
        int day;
        boolean flag = isLeapYear(year);
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                day = 31;
                break;
            case 2:
                day = flag ? 29 : 28;
                break;
            default:
                day = 30;
                break;
        }
        return day;
    }

    /**
     * 判断当前输入年份是否为闰年
     */
    public static boolean isLeapYear(int year) {
        return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
    }
}
