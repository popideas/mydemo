package com.hks360.library.util;

import android.content.Context;
import android.widget.Toast;

/**
 * @author by Xiaote<br>
 */
public class ToastUtil {
    public static void showToast(Context context, int msgResId) {
        Toast.makeText(context, msgResId, Toast.LENGTH_SHORT).show();
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}
