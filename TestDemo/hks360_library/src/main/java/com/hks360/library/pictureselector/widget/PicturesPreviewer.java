package com.hks360.library.pictureselector.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.AttributeSet;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.hks360.library.pictureselector.SelectImageActivity;
import com.hks360.library.pictureselector.adapter.SelectImageAdapter;
import com.hks360.library.pictureselector.base.Common;
import com.hks360.library.pictureselector.config.SelectOptions;
import com.hks360.library.util.CommonUtil;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by JuQiu
 * on 16/7/18.
 * <p>
 * 动弹发布界面, 图片预览器
 * <p>
 * 提供图片预览/图片操作 返回选中图片等功能
 */

public class PicturesPreviewer extends RecyclerView implements SelectImageAdapter.Callback {
    private SelectImageAdapter mImageAdapter;
    private ItemTouchHelper mItemTouchHelper;
    private RequestManager mCurImageLoader;
    private int mSelectCount = Common.MAX_SIZE;
    private boolean isPreviewMode;

    public PicturesPreviewer(Context context) {
        super(context);
        init();
    }

    public PicturesPreviewer(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PicturesPreviewer(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        mImageAdapter = new SelectImageAdapter(this);

        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 4);
        this.setLayoutManager(layoutManager);
        this.setAdapter(mImageAdapter);
        this.setOverScrollMode(View.OVER_SCROLL_NEVER);

        ItemTouchHelper.Callback callback = new PicturesPreviewerItemTouchCallback(mImageAdapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(this);
    }

    public void setIsSingleSelected(boolean isSingleSelected) {
        mSelectCount = isSingleSelected ? Common.MIN_SIZE : Common.MAX_SIZE;
    }

    public void setImages(String[] paths) {
        List<String> pathList = getAllImageUrlsList();
        mImageAdapter.clear();
        if(!CommonUtil.isEmpty(pathList)) {
            for (String path : pathList) {
                mImageAdapter.add(path);
            }
        }
        for (String path : paths) {
            mImageAdapter.add(path);
        }
        mImageAdapter.notifyDataSetChanged();
    }

    private List<String> getAllImageUrlsList() {
        String[] paths = mImageAdapter.getPaths();
        if(!CommonUtil.isEmpty(paths)) {
            List<String> list = new ArrayList<>();
            for (String path : paths) {
                if (path.startsWith("http")) {
                    list.add(path);
                }
            }
            return list;
        }
        return null;
    }

    String[] imageUrls;

    /**
     * 设置网络图片
     *
     * @param paths
     */
    public void setImagesUrls(String[] paths) {
        imageUrls = paths;
    }

    public void addImageUrls() {
        if (imageUrls == null) {
            return;
        }
        for (String path : imageUrls) {
            mImageAdapter.add(path);
        }
        mImageAdapter.notifyDataSetChanged();
    }

    /**
     * 设置为预览模式 图片不能删除 不能添加
     */
    public void setPreviewMode() {
        isPreviewMode = true;
        mImageAdapter.setPreviewMode();
    }

    @Override
    public void onLoadMoreClick() {
        if (!isPreviewMode) {
            int selectedCount = 0;
            if (mImageAdapter.getPaths() != null) {
                for(String s : mImageAdapter.getPaths()) {
                    if(s.startsWith("http")) {
                        selectedCount++;
                    }
                }
            }

            SelectImageActivity.show(getContext(), new SelectOptions.Builder()
                    .setHasCam(true)
                    .setSelectCount(mSelectCount - selectedCount)
                    .setSelectedImages(mImageAdapter.getPaths())
                    .setCallback(new SelectOptions.Callback() {
                        @Override
                        public void doSelected(String[] images) {
                            setImages(images);
                        }
                    }).build());
        }
    }


    public void setSelectCount(int selectCount) {
        mSelectCount = selectCount;
    }

    @Override
    public RequestManager getImgLoader() {
        if (mCurImageLoader == null) {
            mCurImageLoader = Glide.with(getContext());
        }
        return mCurImageLoader;
    }

    @Override
    public void onStartDrag(ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    public String[] getPaths() {
        return mImageAdapter.getPaths();
    }
}
