package com.hks360.library.glide;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * @author Xiaote
 * @date 2018/7/5 14:02
 * @email xiao_t@hks360.com
 */
@GlideModule
public final class CustomAppGlideModule extends AppGlideModule {

}
