package com.hks360.library.pullrefreshlayout.widget;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.hks360.library.pullrefreshlayout.NestedRelativeLayout;
import com.hks360.library.pullrefreshlayout.PRLCommonUtils;
import com.hks360.library.pullrefreshlayout.PullRefreshLayout;
import com.hks360.library.util.UIUtil;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * 两个按钮下拉头部
 * Created by houJia on 2018/5/24.
 * from https://github.com/scwang90/SmartRefreshLayout
 */
public class TwoButtonHeader extends NestedRelativeLayout implements PullRefreshLayout.OnPullListener {


    protected Button mLeftButton;
    protected Button mRightButton;

    public TwoButtonHeader(Context context) {
        super(context);
        this.initView(context, null);
    }

    /**
     * 初始化View
     *
     * @param context
     * @param attrs
     */
    protected void initView(Context context, AttributeSet attrs) {
        setMinimumHeight(PRLCommonUtils.dipToPx(getContext(), 80));
        LinearLayout layout = new LinearLayout(context);
        layout.setId(android.R.id.widget_frame);
        layout.setGravity(Gravity.CENTER_HORIZONTAL);
        layout.setOrientation(LinearLayout.HORIZONTAL);
        mLeftButton = new Button(context);
        mRightButton = new Button(context);
        mLeftButton.setText("左边的按钮");
        mRightButton.setText("右边的按钮");
        mRightButton.setTextColor(ContextCompat.getColor(getContext(), android.R.color.black));
        mRightButton.setTextSize(16);
        mLeftButton.setTextColor(ContextCompat.getColor(getContext(), android.R.color.black));
        mLeftButton.setTextSize(16);
        LinearLayout.LayoutParams lpLeftBtn = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        layout.addView(mLeftButton, lpLeftBtn);
        layout.setPadding(0,0, UIUtil.dp2px(10),0);
        LinearLayout.LayoutParams lpRightBtn = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        layout.addView(mRightButton, lpRightBtn);
        layout.setPadding( UIUtil.dp2px(10),0,0,0);
        RelativeLayout.LayoutParams lpHeaderLayout = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        lpHeaderLayout.addRule(CENTER_IN_PARENT);
        addView(layout, lpHeaderLayout);
    }

    public TwoButtonHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initView(context, attrs);
    }

    public void setLeftId(int resId) {
        mLeftButton.setId(resId);
    }

    public void setRightId(int rightId) {
        mLeftButton.setId(rightId);
    }

    public void setTextColor(int textColor) {
        mLeftButton.setTextColor(textColor);
        mRightButton.setTextColor(textColor);
    }

    public void setBackGround(int backGroundResId) {
        mLeftButton.setBackgroundResource(backGroundResId);
        mRightButton.setBackgroundResource(backGroundResId);
    }

    public void setLeftText(CharSequence text) {
        mLeftButton.setText(text);
    }

    public void setRightText(CharSequence text) {
        mRightButton.setText(text);
    }

    public Button getLeftButton() {
        return mLeftButton;
    }

    public Button getRightButton() {
        return mRightButton;
    }

    /**
     * 设置点击事件
     * 在onClick方法中 通过 view==getRightButton 来判断是否点击右边按钮
     * @param listener
     */
    @Override
    public void setOnClickListener(View.OnClickListener listener) {
        mLeftButton.setOnClickListener(listener);
        mRightButton.setOnClickListener(listener);
    }


    @Override
    public void onPullChange(float percent) {

    }

    @Override
    public void onPullHoldTrigger() {
        //释放立即刷新

    }

    @Override
    public void onPullHoldUnTrigger() {
        //下拉可以刷新

    }

    @Override
    public void onPullHolding() {
        //正在刷新
    }

    @Override
    public void onPullFinish(boolean flag) {
        //刷新完成

    }

    @Override
    public void onPullReset() {
        onPullHoldUnTrigger();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

    }
}
