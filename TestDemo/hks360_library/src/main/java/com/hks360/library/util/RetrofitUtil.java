package com.hks360.library.util;

import com.hks360.library.jsonformat.util.GsonUtils;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * <p>描述： </p>
 *
 * @author by houJia<br>
 * 2018/4/10 <br>
 */
public class RetrofitUtil {
    public static RequestBody getJSONRequestBody(String jsonStr) {
        return RequestBody.create(MediaType.parse("application/json"), jsonStr);
    }

    public static <T> RequestBody getJSONRequestBodyForBean(T t) {
        String jsonStr = GsonUtils.toJson(t);
        return RequestBody.create(MediaType.parse("application/json"), jsonStr);
    }
}
