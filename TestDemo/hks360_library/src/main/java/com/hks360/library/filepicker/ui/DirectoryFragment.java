package com.hks360.library.filepicker.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.hks360.library.R;
import com.hks360.library.filepicker.filter.CompositeFilter;
import com.hks360.library.filepicker.utils.FileUtils;
import com.hks360.library.xlog.XLog;

import java.io.File;
import java.util.List;

/**
 * Created by Dimorinny on 24.10.15.
 */
public class DirectoryFragment extends Fragment {

    public void updateFile(String currentPath) {
        XLog.e("test");
        XLog.e("mPath=>" + currentPath);
        List<File> fileList = FileUtils.getFileListByDirPath(mPath, mFilter);
        XLog.e("filesSize=>" + fileList);
        mDirectoryAdapter.setNewData(fileList);
    }

    interface FileClickListener {
        void onFileClicked(File clickedFile);
    }

    private static final String ARG_FILE_PATH = "arg_file_path";
    private static final String ARG_FILTER = "arg_filter";

    private View mEmptyView;
    private String mPath;

    private CompositeFilter mFilter;

    private RecyclerView mDirectoryRecyclerView;
    private DirectoryAdapter mDirectoryAdapter;
    private FileClickListener mFileClickListener;

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mFileClickListener = (FileClickListener) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mFileClickListener = null;
    }

    public static DirectoryFragment getInstance(String path, CompositeFilter filter) {
        DirectoryFragment instance = new DirectoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_FILE_PATH, path);
        args.putSerializable(ARG_FILTER, filter);
        instance.setArguments(args);
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.filepicker_directory_fragment, container, false);
        mDirectoryRecyclerView = view.findViewById(R.id.directory_recycler_view);
        mEmptyView = LayoutInflater.from(getActivity()).inflate(R.layout.filepicker_empty_layout, null);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initArgs();
        initFilesList();
    }

    private void initFilesList() {
        mDirectoryAdapter = new DirectoryAdapter(FileUtils.getFileListByDirPath(mPath, mFilter));
        mDirectoryRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mDirectoryRecyclerView.setAdapter(mDirectoryAdapter);
        mDirectoryAdapter.setEmptyView(mEmptyView);
        mDirectoryAdapter.setOnItemClickListener(new MyOnItemClickListener());
    }

    @SuppressWarnings("unchecked")
    private void initArgs() {
        if (getArguments().getString(ARG_FILE_PATH) != null) {
            mPath = getArguments().getString(ARG_FILE_PATH);
        }
        mFilter = (CompositeFilter) getArguments().getSerializable(ARG_FILTER);
    }

    private class MyOnItemClickListener implements BaseQuickAdapter.OnItemClickListener {
        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            if (mFileClickListener != null) {
                mFileClickListener.onFileClicked(mDirectoryAdapter.getItem(position));
            }
        }
    }
}
