package com.hks360.library.util;

import android.content.Context;
import android.text.TextUtils;
import android.webkit.WebView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.hks360.library.common.Charset;

import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author by Xiaote<br>
 */
public class CommonUtil {

    public static boolean isEmpty(CharSequence str) {
        return TextUtils.isEmpty(str);
    }

    public static <T> boolean isEmpty(T[] arr) {
        return arr == null || arr.length == 0;
    }

    public static <T> boolean isEmpty(List<T> list) {
        return list == null || list.isEmpty();
    }

    public static <T> boolean isEmpty(Set<T> set) {
        return set == null || set.isEmpty();
    }

    public static <K, V> boolean isEmpty(Map<K, V> map) {
        return map == null || map.isEmpty();
    }

    public static boolean isEmpty(TextView textView) {
        return textView == null || textView.getText().toString().length() == 0;
    }

    public static boolean isEmpty(int[] arr) {
        return arr == null || arr.length == 0;
    }

    public static boolean isEmpty(long[] arr) {
        return arr == null || arr.length == 0;
    }

    public static boolean isEmpty(float[] arr) {
        return arr == null || arr.length == 0;
    }

    public static boolean isEmpty(double[] arr) {
        return arr == null || arr.length == 0;
    }

    public static boolean isEmpty(boolean[] arr) {
        return arr == null || arr.length == 0;
    }

    public static boolean equals(CharSequence sequence1, CharSequence sequence2) {
        return TextUtils.equals(sequence1, sequence2);
    }

    public static boolean isOtherViewType(int viewType) {
        boolean flag;
        switch (viewType) {
            case BaseQuickAdapter.HEADER_VIEW:
            case BaseQuickAdapter.FOOTER_VIEW:
            case BaseQuickAdapter.LOADING_VIEW:
            case BaseQuickAdapter.EMPTY_VIEW:
                flag = true;
                break;
            default:
                flag = false;
                break;
        }
        return flag;
    }

    public static String getH5CacheDir(Context context) {
        return context.getApplicationContext().getCacheDir().getAbsolutePath();
    }

    public static String getUrlEncodeStr(String content) {
        try {
            return URLEncoder.encode(content, Charset.UTF8);
        } catch (Exception e) {
            return content;
        }
    }

    public static String getMoneyValue(double price) {
        DecimalFormat format = new DecimalFormat("0.00");
        return format.format(price);
    }

    public static String getMoneyValue(String priceStr) {
        if(isEmpty(priceStr)) {
            return "0.00";
        }
        DecimalFormat format = new DecimalFormat("0.00");
        return format.format(Double.parseDouble(priceStr));
    }

    public static void setWebViewInitialScaleWithImage(WebView webView) {
        int width = UIUtil.getDisplayScreenWidth();
        if (width > 650) {
            webView.setInitialScale(190);
        } else if (width > 520) {
            webView.setInitialScale(160);
        } else if (width > 450) {
            webView.setInitialScale(140);
        } else if (width > 300) {
            webView.setInitialScale(120);
        } else {
            webView.setInitialScale(100);
        }
    }

    /**
     * 加载带有头信息的图片时，获取头信息
     */
    public static Map<String, String> getHeaderMap() {
        Map map = new HashMap<>();
        map.put("Referer", "http://www.kuaixiuge.com");
        return map;
    }


}
