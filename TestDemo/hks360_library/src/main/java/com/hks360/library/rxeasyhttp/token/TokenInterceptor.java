package com.hks360.library.rxeasyhttp.token;

import com.google.gson.JsonSyntaxException;
import com.hks360.library.rxeasyhttp.EasyHttp;
import com.hks360.library.rxeasyhttp.callback.SimpleCallBack;
import com.hks360.library.rxeasyhttp.contact.ComParamContact;
import com.hks360.library.rxeasyhttp.exception.ApiException;
import com.hks360.library.rxeasyhttp.interceptor.BaseExpiredInterceptor;
import com.hks360.library.rxeasyhttp.model.ApiResult;
import com.hks360.library.jsonformat.util.GsonUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * <p>描述： </p>
 *
 * @author by houJia<br>
 * 2018-9-5 <br>
 */
public class TokenInterceptor extends BaseExpiredInterceptor {

    public static final String TOKEN = "core/connect/token";
    public static final String WEB_API_TOKEN_URL = "https://test-040-9212.hks360.com";

    private ApiResult apiResult;
    private AccessTokenBean tokenBean;

    @Override
    public boolean isResponseExpired(Response response, String bodyString) {
        try {
            apiResult = GsonUtils.fromJson(bodyString, ApiResult.class);
            if (apiResult != null) {
                int code = apiResult.getFailure();
                if (code == ComParamContact.Code.TOKEN_EXPIRED) {
                    return true;
                }
            }
            return false;
        } catch (JsonSyntaxException e) {
            //注意 !!! 此处只json解析异常 其他错误交给上级
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Response responseExpired(Interceptor.Chain chain, String bodyString) {

        try {
            if (apiResult.getFailure() == ComParamContact.Code.TOKEN_EXPIRED) {
                refreshToken();
                if (tokenBean != null) {
                    return processAccessTokenError(chain, chain.request());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Response processAccessTokenError(Interceptor.Chain chain, Request request) throws IOException {
        Request newRequest = request.newBuilder().header(ComParamContact.Common.AUTH, TokenManager.getInstance().getToken()).build();
        EasyHttp.getInstance().getCommonHeaders().remove(ComParamContact.Common.AUTH);
        EasyHttp.getInstance().getCommonHeaders().put(ComParamContact.Common.AUTH, TokenManager.getInstance().getToken());
        return chain.proceed(newRequest);
    }

    private void refreshToken() {
        EasyHttp.post(TOKEN)
                .baseUrl(WEB_API_TOKEN_URL)
                .params("grant_type", "client_credentials")
                .params("scope", "write")
                .params("client_id", "za.client")
                .params("client_secret", "ZAHKS61B754C541BBCFC6A45A9E9EC5E47D8702B78C29")
                .accessToken(false)
                .syncRequest(true)
                .execute(new SimpleCallBack<String>() {
                    @Override
                    public void onError(ApiException e) {
                        //notifyLoginExit(e.message!!)
                    }
                    @Override
                    public void onSuccess(String response) {
                        tokenBean = GsonUtils.fromJson(response, AccessTokenBean.class);
                        TokenManager.getInstance().setToken(String.format("%s %s", tokenBean.getTokenType(), tokenBean.getAccessToken()));
                    }
                });
    }
}
