package com.hks360.library.pictureselector.base;

/**
 * @author ：xiaote
 * @date ：2019-09-06 11：41
 * 邮箱   ：xiao_t@hks360.com
 */
public class Common {
    public static final int MIN_SIZE = 1;
    public static final int MAX_SIZE = 9;
}
