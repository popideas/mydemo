package com.hks360.library.alertview;


/**
 * Created by Sai on 15/8/9.
 */
public interface OnAlertDismissListener {
    void onDismiss(AlertView alertView);
}
