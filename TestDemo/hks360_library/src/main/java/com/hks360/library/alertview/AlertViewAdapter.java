package com.hks360.library.alertview;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hks360.library.R;

import java.util.List;

/**
 * Created by Sai on 15/8/9.
 */
public class AlertViewAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    private List<String> mDestructive;

    public AlertViewAdapter(List<String> datas) {
        super(R.layout.alertview_item_alertbutton, datas);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        helper.setText(R.id.tvAlert, item);
    }
}