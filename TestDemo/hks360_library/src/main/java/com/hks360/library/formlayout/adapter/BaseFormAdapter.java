package com.hks360.library.formlayout.adapter;

import android.support.annotation.NonNull;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hks360.library.R;


/**
 * @author ：xiaote
 * @date ：2020-07-11 13：27
 * 邮箱   ：xiao_t@hks360.com
 */
public abstract class BaseFormAdapter<T> extends BaseQuickAdapter<T, BaseViewHolder> {
    public BaseFormAdapter() {
        super(R.layout.sync_item_base_form);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, T item) {
        int count = getColumnCount();
        LinearLayout view = helper.getView(R.id.rootView);
        view.removeAllViews();
        for (int i = 0; i < count; i++) {
            try {
                String title = getRowData(item, i);
                TextView textView = new TextView(mContext);
                textView.setText(title);
                textView.setTextSize(getTextSizeUnit(), getTextSize());
                textView.setLayoutParams(new LinearLayout.LayoutParams(getTextWidth(), getTextHeight()));
                textView.setGravity(Gravity.CENTER);
                textView.setTextColor(getTextColor());
                textView.setPadding(getTextPadding(), 0, getTextPadding(), 0);
                textView.setSingleLine(true);
                view.addView(textView);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (helper.getAdapterPosition() % 2 == 0) {
            view.setBackgroundResource(R.color.form_bg1);
        } else {
            view.setBackgroundResource(R.color.form_bg2);
        }
    }

    public abstract int getColumnCount();

    public abstract String getRowData(T item, int index);

    public abstract int getTextColor();

    public abstract int getTextWidth();

    public abstract int getTextHeight();

    public abstract int getTextPadding();

    public abstract int getTextSizeUnit();

    public abstract float getTextSize();
}
