package com.hks360.library.formlayout.adapter;

import android.support.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hks360.library.R;


/**
 * @author ：xiaote
 * @date ：2020-07-11 10：36
 * 邮箱   ：xiao_t@hks360.com
 */
public class SyncLeftTitleAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public SyncLeftTitleAdapter() {
        super(R.layout.sync_item_left_title);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, String item) {
        helper.setText(R.id.left_title_text, item);
        if (helper.getAdapterPosition() % 2 == 0) {
            helper.setBackgroundRes(R.id.left_title_text, R.color.form_bg1);
        } else {
            helper.setBackgroundRes(R.id.left_title_text, R.color.form_bg2);
        }
    }
}
