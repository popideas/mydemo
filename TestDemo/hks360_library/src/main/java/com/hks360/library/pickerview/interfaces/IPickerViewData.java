package com.hks360.library.pickerview.interfaces;

/**
 * Created by Sai on 2016/7/13.
 */
public interface IPickerViewData {
    String getPickerViewText();
}
