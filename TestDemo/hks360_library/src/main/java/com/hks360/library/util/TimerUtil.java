package com.hks360.library.util;

import android.os.CountDownTimer;
import android.widget.TextView;

/**
 * Timer 工具类
 */
public class TimerUtil {
    private static final String DEFAULT_GET_CODE = "获取验证码";
    private static final String FORMAT_TIME_STR = "重新发送(%d)";

    private static int COUNT = 60;
    private static CountDownTimer timer;

    public static void timerCancel() {
        if (timer != null) {
            timer.cancel();
            COUNT = 60;
        }
    }

    public static void createTimer(long inFuture, long interval, final TextView tvCode) {

        timer = new CountDownTimer(inFuture, interval) {

            @Override
            public void onTick(long l) {
                COUNT--;
                tvCode.setText(String.format(FORMAT_TIME_STR, COUNT));
            }

            @Override
            public void onFinish() {
                tvCode.setText(DEFAULT_GET_CODE);
                COUNT = 60;
                tvCode.setEnabled(true);
            }
        }.start();
    }

}
