package com.hks360.library.filepicker.ui;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hks360.library.R;
import com.hks360.library.filepicker.utils.FileTypeUtils;

import java.io.File;
import java.util.List;

/**
 * Created by Dimorinny on 24.10.15.
 */

public class DirectoryAdapter extends BaseQuickAdapter<File, BaseViewHolder> {

    public DirectoryAdapter(List<File> files) {
        super(R.layout.filepicker_item, files);
    }


    @Override
    protected void convert(BaseViewHolder helper, File item) {
        FileTypeUtils.FileType fileType = FileTypeUtils.getFileType(item);
        helper.setImageResource(R.id.item_file_image, fileType.getIcon());
        helper.setText(R.id.item_file_title, fileType.getDescription());
        helper.setText(R.id.item_file_subtitle, item.getName());
    }
}