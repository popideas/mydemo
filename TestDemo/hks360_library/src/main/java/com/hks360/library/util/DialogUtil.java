package com.hks360.library.util;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.hks360.library.R;
import com.hks360.library.alertview.AlertView;

/**
 * @author ：xiaote
 * @date ：2020-01-17 13：14
 * 邮箱   ：xiao_t@hks360.com
 */
public class DialogUtil {

    public static void showConfirmAlert(
            Context context, String title, String message, String confirmText) {
        new AlertView(context, title, message, null,
                new String[]{TextUtils.isEmpty(confirmText) ? "确定" : confirmText},
                AlertView.Style.Alert, null).show();
    }

    public static void showConfirmDialog(
            Context context, String title, String message, String confirmText) {
        final Dialog dialog = new Dialog(context, R.style.my_dialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_confirm, null);
        TextView titleTv = contentView.findViewById(R.id.title_tv);
        titleTv.setText(title);
        TextView messageTv = contentView.findViewById(R.id.message_tv);
        messageTv.setText(message);
        TextView confirmTv = contentView.findViewById(R.id.confirm_tv);
        confirmTv.setText(TextUtils.isEmpty(confirmText) ? "确定" : confirmText);
        confirmTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        dialog.setContentView(contentView);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }
}
