package com.hks360.library.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.hks360.library.R;
import com.hks360.library.rxeasyhttp.subsciber.IProgressDialog;


/**
 * <p>描述： </p>
 *
 * @author by houJia<br>
 * 2017-11-14 014 <br>
 */
public class LoadingDialog implements IProgressDialog {
    private Context mContext;
    private String mTitle;
    private Dialog mDialog;
    private TextView mTvTitle;

    public LoadingDialog(Context mContext) {
        this(mContext, "正在加载...");
    }

    public LoadingDialog(Context mContext, String title) {
        this.mTitle = title;
        this.mContext = mContext;
        initDialog();
    }


    @Override
    public Dialog getDialog() {
        return initDialog();
    }

    private Dialog initDialog() {
        if (mDialog == null) {
            View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_black_loading, null);
            mTvTitle = contentView.findViewById(R.id.tv_progress);
            mTvTitle.setText(mTitle);
            mDialog = new Dialog(mContext, R.style.LoadingDialog);
            mDialog.setContentView(contentView);
        }
        return mDialog;
    }

    public void setTitle(String title) {
        if (mTvTitle != null) {
            mTvTitle.setText(title);
        }
    }

    public void setCancelable(boolean cancelable) {
        if (mDialog != null) {
            mDialog.setCancelable(cancelable);
            mDialog.setCanceledOnTouchOutside(cancelable);
        }
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

    public void show() {
        if (mDialog != null) {
            mDialog.show();
        }
    }
}
