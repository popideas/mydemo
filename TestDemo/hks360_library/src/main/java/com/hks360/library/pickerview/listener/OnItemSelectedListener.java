package com.hks360.library.pickerview.listener;


public interface OnItemSelectedListener {
    void onItemSelected(int index);
}
