package com.hks360.library.flexbox.interfaces;

public interface TagWithListener<T> {

    void onItemSelect(T item);
}
