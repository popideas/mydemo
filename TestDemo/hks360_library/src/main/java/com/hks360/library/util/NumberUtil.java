package com.hks360.library.util;

import java.text.DecimalFormat;

/**
 * @author Xiaote
 * @date 2018/7/6 09:54
 * @email xiao_t@hks360.com
 */
public class NumberUtil {

    public static String formatMoneyValue(double value) {
        return getDoubleFormatString(value, "0.00");
    }

    public static String getDoubleFormatString(double value, String formatStr) {
        DecimalFormat decimalFormat = new DecimalFormat(formatStr);
        return decimalFormat.format(value);
    }

    public static double getDoubleValue(String str) {
        return CommonUtil.isEmpty(str) ? 0 : Double.parseDouble(str);
    }

    public static String getDoubleNumber(Double number) {
        return new DecimalFormat("#0.00").format(number);
    }

    public static String getIntNumber(Double number) {
        return new DecimalFormat("###.##").format(number);
    }

}
