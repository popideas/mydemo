package com.hks360.library.recyclerview.util;

import android.app.Activity;

import com.hks360.library.recyclerview.swipe.SwipeMenuItem;

/**
 * @author by Xiaote<br>
 */
public class SwipeMenuCreatorUtil {
    public static SwipeMenuItem createSwipeMeunItem(Activity activity, int bgResId, int imgResId, int width,
                                                    int height) {
        return new SwipeMenuItem(activity).setBackground(bgResId).setImage(imgResId).setWidth(width).setHeight(height);

    }

    public static SwipeMenuItem createSwipeMenuItem(Activity activity, int bgResId, int txtResId, int textSize,
                                                    int txtColorResId, int width, int height) {
        return new SwipeMenuItem(activity).setBackground(bgResId).setText(txtResId).setTextColorResource(txtColorResId)
                .setTextSize(textSize).setWidth(width).setHeight(height);
    }

    public static SwipeMenuItem createSwipeMenuItem(Activity activity, int bgResId, String text, int textSize,
                                                    int textColor, int width, int height) {
        return new SwipeMenuItem(activity).setBackground(bgResId).setText(text).setTextColor(textColor)
                .setTextSize(textSize).setWidth(width).setHeight(height);
    }
}
