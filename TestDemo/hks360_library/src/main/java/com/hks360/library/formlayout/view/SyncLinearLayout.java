package com.hks360.library.formlayout.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hks360.library.R;
import com.hks360.library.formlayout.adapter.BaseFormAdapter;
import com.hks360.library.formlayout.adapter.SyncLeftTitleAdapter;
import com.hks360.library.formlayout.adapter.SyncTopTitleAdapter;
import com.hks360.library.xlog.XLog;

import java.util.List;

/**
 * @author ：张 奎
 * @date ：2019-09-16 10：52
 * 邮箱   ：JiaYang627@163.com / zhang_k@hks360.com
 */
public abstract class SyncLinearLayout extends LinearLayout {

    private LinearLayout mLeftTitleLayout;
    private TextView mLeftTitleText;
    private MyHorizontalScrollView mTopTitleScrollView;
    private RecyclerView mTopTitleRv;
    private RecyclerView mLeftTitleRv;
    private MyHorizontalScrollView mFormScrollView;
    private RecyclerView mFormRv;
    private LinearLayout mLoadingLayout;

    private SyncLeftTitleAdapter mLeftTitleAdapter;
    private SyncTopTitleAdapter mTopTitleAdapter;

    public int mColumnCount;
    private boolean mIsLoadMoreEnd = true;

    public SyncLinearLayout(Context context) {
        this(context, null);
    }

    public SyncLinearLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SyncLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {

        LayoutInflater.from(context).inflate(R.layout.sync_view, this, true);

        // 左上角 文字
        mLeftTitleText = findViewById(R.id.left_title_text);

        // 左上角 布局
        mLeftTitleLayout = findViewById(R.id.left_title_layout);

        // 右侧顶部 横向滑动的ScrollView
        mTopTitleScrollView = findViewById(R.id.top_title_scroll_view);

        // 右侧顶部 不可滑动的RecyclerView
        mTopTitleRv = findViewById(R.id.top_title_rv);

        // 左侧底部 可滑动的RecyclerView
        mLeftTitleRv = findViewById(R.id.left_title_rv);

        // 右侧底部 下拉刷新控件
        //        mSwipeLayout = findViewById(R.id.swipeLayout);
        //        mSwipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        // 右侧底部 横向滑动的ScrollView
        mFormScrollView = findViewById(R.id.form_scroll_view);

        // 右侧底部 竖向滑动的RecyclerView
        mFormRv = findViewById(R.id.from_rv);

        //加载更多Layout
        mLoadingLayout = findViewById(R.id.loading_layout);

        TypedArray mTypedArray = context.obtainStyledAttributes(attrs, R.styleable.SyncLinearLayout);

        if (mTypedArray == null) {
            return;
        }

        String mLeftTitleText = mTypedArray.getString(R.styleable.SyncLinearLayout_sync_leftTitleText);


        int backgroundColor = mTypedArray.getColor(R.styleable.SyncLinearLayout_sync_leftTitleBackgroundColor, 0);
        int leftUpTextColor = mTypedArray.getColor(R.styleable.SyncLinearLayout_sync_leftTitleTextColor, 0);

        mColumnCount = mTypedArray.getInt(R.styleable.SyncLinearLayout_sync_formColumnCount, 0);
        //        int width = UIUtil.dip2px(mIsChange ? 60f : 136f);
        //        int recy = UIUtil.dip2px(mIsChange ? 60f : 136f);
        //        ViewGroup.LayoutParams layoutParams = mLeftTitleLayout.getLayoutParams();
        //        layoutParams.width = width;
        //        mLeftTitleLayout.setLayoutParams(layoutParams);
        //
        //        ViewGroup.LayoutParams params = mLeftTitleLayout.getLayoutParams();
        //        params.width = recy;
        //        mLeftTitleLayout.setLayoutParams(params);


        if (!TextUtils.isEmpty(mLeftTitleText)) {
            setLeftTitleText(mLeftTitleText);
        }

        if (backgroundColor != 0) {
            setLeftTitleBackground(backgroundColor);
        }

        if (leftUpTextColor != 0) {
            setLeftTitleTextColor(leftUpTextColor);
        }

        initRecyclerView(context);
        initSync();

    }

    public void setLoadMoreEnd(boolean isLoadMoreEnd) {
        mIsLoadMoreEnd = isLoadMoreEnd;
        mLoadingLayout.setVisibility(View.GONE);
    }

    public List<String> getLeftTitleList() {
        return mLeftTitleAdapter.getData();
    }

    public List<String> getTopTitleList() {
        return mTopTitleAdapter.getData();
    }

    public onLoadMoreListener mOnLoadMoreListener;

    public void setOnLoadMoreListener(onLoadMoreListener loadMoreListener) {
        this.mOnLoadMoreListener = loadMoreListener;
    }


    private void initSync() {
        mLeftTitleRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, final int dx, final int dy) {
                if (recyclerView.getScrollState() != RecyclerView.SCROLL_STATE_IDLE) {
                    try {
                        mFormRv.scrollBy(dx, dy);
                        checkIsLoadMore(recyclerView);
                    } catch (Exception ignored) {
                    }
                }
            }
        });
        mFormRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, final int dx, final int dy) {
                if (recyclerView.getScrollState() != RecyclerView.SCROLL_STATE_IDLE) {

                    mLeftTitleRv.scrollBy(dx, dy);
                    checkIsLoadMore(recyclerView);
                }
            }
        });

        mFormScrollView.setOnScrollViewListener(new MyHorizontalScrollView.onScrollViewListener() {
            @Override
            public void onScrollChange(int x, int y, int oldx, int oldy) {
                mTopTitleScrollView.scrollTo(x, y);
            }
        });

        mTopTitleScrollView.setOnScrollViewListener(new MyHorizontalScrollView.onScrollViewListener() {
            @Override
            public void onScrollChange(int x, int y, int oldx, int oldy) {
                mFormScrollView.scrollTo(x, y);
            }
        });
    }

    private void checkIsLoadMore(RecyclerView recyclerView) {
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager != null) {
            View lastChildView = layoutManager.getChildAt(layoutManager.getChildCount() - 1);
            if (lastChildView != null) {
                //得到lastChildView的bottom坐标值
                int lastChildBottom = lastChildView.getBottom();
                //得到Recyclerview的底部坐标减去底部padding值，也就是显示内容最底部的坐标
                int recyclerBottom = recyclerView.getBottom() - recyclerView.getPaddingBottom();
                //通过这个lastChildView得到这个view当前的position值
                int lastPosition = recyclerView.getLayoutManager().getPosition(lastChildView);
                //判断lastChildView的bottom值跟recyclerBottom
                //判断lastPosition是不是最后一个position
                //如果两个条件都满足则说明是真正的滑动到了底部
                //如果不是最后一页继续加载数据
                if (lastChildBottom == recyclerBottom && lastPosition == recyclerView.getLayoutManager().getItemCount() - 1 && !mIsLoadMoreEnd) {
                    mLoadingLayout.setVisibility(View.VISIBLE);
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                }
            }
        }
    }

    /**
     * 设置 左上角文字内容
     *
     * @param leftUpText TextView要展示的内容
     */
    public void setLeftTitleText(String leftUpText) {
        mLeftTitleText.setText(leftUpText);
    }

    /**
     * 设置 左上角文字控件的背景色
     *
     * @param color 要设置的颜色值
     */
    public void setLeftTitleBackground(int color) {
        getLeftTitleLayout().setBackgroundColor(color);
    }

    /**
     * 设置 左上角文字颜色
     *
     * @param leftUpTextColor 要设置的颜色值
     */
    public void setLeftTitleTextColor(int leftUpTextColor) {

        getLeftTitleText().setTextColor(leftUpTextColor);

    }

    private TextView getLeftTitleText() {
        return mLeftTitleText;
    }

    private LinearLayout getLeftTitleLayout() {
        return mLeftTitleLayout;
    }

    private void initRecyclerView(Context context) {

        mLeftTitleRv.setLayoutManager(new LinearLayoutManager(context));

        // 右侧上部 设置为横向 但是不可滑动
        mTopTitleRv.setLayoutManager(new LinearLayoutManager(context) {
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public void setOrientation(int orientation) {
                super.setOrientation(HORIZONTAL);

            }
        });

        mFormRv.setItemViewCacheSize(200);
        mFormRv.setLayoutManager(new LinearLayoutManager(context) {
            private int[] mMeasuredDimension = new int[2];

            @Override
            public void onMeasure(@NonNull RecyclerView.Recycler recycler, @NonNull RecyclerView.State state, int widthSpec, int heightSpec) {
                super.onMeasure(recycler, state, widthSpec, heightSpec);
                final int widthMode = View.MeasureSpec.getMode(widthSpec);
                final int heightMode = View.MeasureSpec.getMode(heightSpec);
                final int widthSize = View.MeasureSpec.getSize(widthSpec);
                final int heightSize = View.MeasureSpec.getSize(heightSpec);

                XLog.e("onMeasure called. \nwidthMode " + widthMode
                        + " \nheightMode " + heightSpec
                        + " \nwidthSize " + widthSize
                        + " \nheightSize " + heightSize
                        + " \ngetItemCount() " + getItemCount());

                int width = 0;
                int height = 0;
                for (int i = 0; i < getItemCount(); i++) {
                    measureScrapChild(recycler, i,
                            View.MeasureSpec.makeMeasureSpec(i, View.MeasureSpec.UNSPECIFIED),
                            View.MeasureSpec.makeMeasureSpec(i, View.MeasureSpec.UNSPECIFIED),
                            mMeasuredDimension);

                    if (getOrientation() == HORIZONTAL) {
                        width = width + mMeasuredDimension[0];
                        if (i == 0) {
                            height = mMeasuredDimension[1];
                        }
                    } else {
                        height = height + mMeasuredDimension[1];
                        if (i == 0) {
                            width = mMeasuredDimension[0];
                        }
                    }
                }
                switch (widthMode) {
                    case View.MeasureSpec.EXACTLY:
                        width = widthSize;
                    case View.MeasureSpec.AT_MOST:
                    case View.MeasureSpec.UNSPECIFIED:
                }

                switch (heightMode) {
                    case View.MeasureSpec.EXACTLY:
                        height = heightSize;
                    case View.MeasureSpec.AT_MOST:
                    case View.MeasureSpec.UNSPECIFIED:
                }

                setMeasuredDimension(width, height);
            }


            private void measureScrapChild(RecyclerView.Recycler recycler, int position, int widthSpec,
                                           int heightSpec, int[] measuredDimension) {
                try {
                    View view = recycler.getViewForPosition(0);//fix 动态添加时报IndexOutOfBoundsException

                    RecyclerView.LayoutParams p = (RecyclerView.LayoutParams) view.getLayoutParams();

                    int childWidthSpec = ViewGroup.getChildMeasureSpec(widthSpec,
                            getPaddingLeft() + getPaddingRight(), p.width);

                    int childHeightSpec = ViewGroup.getChildMeasureSpec(heightSpec,
                            getPaddingTop() + getPaddingBottom(), p.height);

                    view.measure(childWidthSpec, childHeightSpec);
                    measuredDimension[0] = view.getMeasuredWidth() + p.leftMargin + p.rightMargin;
                    measuredDimension[1] = view.getMeasuredHeight() + p.bottomMargin + p.topMargin;
                    recycler.recycleView(view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        mLeftTitleAdapter = new SyncLeftTitleAdapter();
        mLeftTitleRv.setAdapter(mLeftTitleAdapter);

        mTopTitleAdapter = new SyncTopTitleAdapter();
        mTopTitleRv.setAdapter(mTopTitleAdapter);
    }

    public void setNewData(List<String> leftTitleList, List<String> topTitleList) {
        mLeftTitleAdapter.setNewData(leftTitleList);
        mTopTitleAdapter.setNewData(topTitleList);
    }

    public void addData(List<String> leftTitleList) {
        mLeftTitleAdapter.addData(leftTitleList);
    }

    public void setFormAdapter(BaseFormAdapter formAdapter) {
        mFormRv.setAdapter(formAdapter);
    }

    public interface onLoadMoreListener {
        void onLoadMore();
    }
}
