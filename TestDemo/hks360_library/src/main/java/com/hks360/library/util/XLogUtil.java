package com.hks360.library.util;

import com.hks360.library.xlog.LogConfiguration;
import com.hks360.library.xlog.LogLevel;
import com.hks360.library.xlog.XLog;

/**
 * Created by xiaote on 2017/4/6.
 */

public class XLogUtil {
    public static void init(String tag, boolean isDebug, boolean isShowBoard) {
        XLog.init(new LogConfiguration.Builder().tag(tag).logLevel(isDebug ? LogLevel.ALL : LogLevel.NONE)
                .showBoarder(isShowBoard).build());
    }
}
