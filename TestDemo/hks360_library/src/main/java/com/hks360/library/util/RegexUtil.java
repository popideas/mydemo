package com.hks360.library.util;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则表达式工具类
 */
public class RegexUtil {
    //手机号或固定电话
    public static final String PHONE = "^((\\+86)|(86))?(1)\\d{10}$" +
            "|^(0[0-9]{2,3}\\-?)?([2-9][0-9]{6,7})+(\\-[0-9]{1,4})?$";
    //邮箱
    public static final String EMAIL = "^(\\w)+(\\.\\w+)*@(\\w)+((\\.\\w{2,3}){1,3})$";
    public static final String MOBILE = "^((\\+86)|(86))?(1)\\d{10}$";
    public static final String LICENSE =
            "^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]" +
                    "{1}[A-Z]{1}[A-Z0-9]{4,5}[A-Z0-9挂学警港澳]{1}$";
    public static final String NUMANDLETTER = "^[A-Za-z0-9]+$";
    public static final String NUM = "^[0-9]*$";

    public static boolean checkMobile(String str) {
        if (CommonUtil.isEmpty(str)) {
            return false;
        }
        return checkStr(MOBILE, str);
    }

    public static boolean checkPhone(String str) {
        if (CommonUtil.isEmpty(str)) {
            return false;
        }
        return checkStr(PHONE, str);
    }

    public static boolean checkEmail(String str) {
        if (CommonUtil.isEmpty(str)) {
            return false;
        }
        return checkStr(EMAIL, str);
    }

    public static boolean checkLicense(String str) {
        if (CommonUtil.isEmpty(str)) {
            return false;
        }
        if (str.length() == 7 || str.length() == 8) {
            return checkStr(LICENSE, str);
        }
        return false;
    }
    public static boolean checkNumAndLetter(String str) {
    	return checkStr(NUMANDLETTER, str);
    }
    public static boolean checkNum(String str) {
    	return checkStr(NUM, str);
    }



    private static boolean checkStr(String pattern, String input) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(input);
        return m.matches();
    }

    public static boolean checkVin(String vin) {
        if (CommonUtil.isEmpty(vin)) {
            return false;
        }
        if (vin.length() < 17) {
            return false;
        }
        boolean resultFlag = false;
        try {
            String upperVin = vin.toUpperCase();
            //排除字母O、I
            if (upperVin.indexOf("O") >= 0 || upperVin.indexOf("I") >= 0) {
                return false;
            }
            Map<Integer, Integer> vinMapWeighting = new HashMap<>();
            vinMapWeighting.put(1, 8);
            vinMapWeighting.put(2, 7);
            vinMapWeighting.put(3, 6);
            vinMapWeighting.put(4, 5);
            vinMapWeighting.put(5, 4);
            vinMapWeighting.put(6, 3);
            vinMapWeighting.put(7, 2);
            vinMapWeighting.put(8, 10);
            vinMapWeighting.put(9, 0);
            vinMapWeighting.put(10, 9);
            vinMapWeighting.put(11, 8);
            vinMapWeighting.put(12, 7);
            vinMapWeighting.put(13, 6);
            vinMapWeighting.put(14, 5);
            vinMapWeighting.put(15, 4);
            vinMapWeighting.put(16, 3);
            vinMapWeighting.put(17, 2);
            Map<Character, Integer> vinMapValue = new HashMap<>();
            vinMapValue.put('0', 0);
            vinMapValue.put('1', 1);
            vinMapValue.put('2', 2);
            vinMapValue.put('3', 3);
            vinMapValue.put('4', 4);
            vinMapValue.put('5', 5);
            vinMapValue.put('6', 6);
            vinMapValue.put('7', 7);
            vinMapValue.put('8', 8);
            vinMapValue.put('9', 9);
            vinMapValue.put('A', 1);
            vinMapValue.put('B', 2);
            vinMapValue.put('C', 3);
            vinMapValue.put('D', 4);
            vinMapValue.put('E', 5);
            vinMapValue.put('F', 6);
            vinMapValue.put('G', 7);
            vinMapValue.put('H', 8);
            vinMapValue.put('J', 1);
            vinMapValue.put('K', 2);
            vinMapValue.put('M', 4);
            vinMapValue.put('L', 3);
            vinMapValue.put('N', 5);
            vinMapValue.put('P', 7);
            vinMapValue.put('Q', 8);
            vinMapValue.put('R', 9);
            vinMapValue.put('S', 2);
            vinMapValue.put('T', 3);
            vinMapValue.put('U', 4);
            vinMapValue.put('V', 5);
            vinMapValue.put('W', 6);
            vinMapValue.put('X', 7);
            vinMapValue.put('Y', 8);
            vinMapValue.put('Z', 9);
            //1:长度为17
            if (vin.length() == 17) {
                char[] vinArr = upperVin.toCharArray();
                int amount = 0;
                for (int i = 0; i < vinArr.length; i++) {
                    if (vinMapValue.containsKey(vinArr[i]) && vinMapWeighting.containsKey(i + 1)) {
                        //VIN码从从第一位开始，码数字的对应值×该位的加权值，计算全部17位的乘积值相加
                        amount += vinMapValue.get(vinArr[i]).intValue()
                                * vinMapWeighting.get(i + 1).intValue();
                    }
                }
                //乘积值相加除以11、若余数为10，即为字母Ｘ
                if (amount % 11 == 10) {
                    if (vinArr[8] == 'X') {
                        resultFlag = true;
                    } else {
                        resultFlag = false;
                    }
                } else {
                    //VIN码从从第一位开始，码数字的对应值×该位的加权值，
                    //计算全部17位的乘积值相加除以11，所得的余数，即为第九位校验值
                    if (amount % 11 != vinMapValue.get(vinArr[8])) {
                        resultFlag = false;
                    } else {
                        resultFlag = true;
                    }
                }
            }
        } catch (Exception e) {
            resultFlag = false;
        }
        return resultFlag;
    }

}
