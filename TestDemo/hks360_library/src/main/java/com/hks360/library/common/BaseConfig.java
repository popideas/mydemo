package com.hks360.library.common;

/**
 * Created by xiaote on 2016/11/4.
 */

public class BaseConfig {
    public static final int CONNECTION_TIMEOUT = 30;
    public static final int READ_TIMEOUT = 30;
    public static final int WRITE_TIMEOUT = 30;
}
