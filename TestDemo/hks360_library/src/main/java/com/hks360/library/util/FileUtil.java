package com.hks360.library.util;

import java.io.File;

/**
 * @author by Xiaote<br>
 */
public class FileUtil {
    public static boolean isExistFile(File file, long contentLength) {
        if (file.exists() && file.length() == contentLength) {
            return true;
        }
        return false;
    }

    public static void deleteFile(String filePath) {
        File file = new File(filePath);
        if (file.isFile() && file.exists()) {
            file.delete();
        }
    }
}
