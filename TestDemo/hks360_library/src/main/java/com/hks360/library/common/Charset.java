package com.hks360.library.common;

/**
 * @author by Xiaote<br>
 */
public class Charset {
    public static final String UTF8 = "UTF-8";
    public static final String GBK = "GBK";
    public static final String GB2312 = "GB2312";
}
