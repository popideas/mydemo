package com.xiaote.mydemo.testdemo.activity.oss;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.sdk.android.oss.ClientConfiguration;
import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.OSS;
import com.alibaba.sdk.android.oss.OSSClient;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.callback.OSSCompletedCallback;
import com.alibaba.sdk.android.oss.callback.OSSProgressCallback;
import com.alibaba.sdk.android.oss.common.OSSLog;
import com.alibaba.sdk.android.oss.common.auth.OSSCredentialProvider;
import com.alibaba.sdk.android.oss.common.auth.OSSStsTokenCredentialProvider;
import com.alibaba.sdk.android.oss.internal.OSSAsyncTask;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.bumptech.glide.Glide;
import com.google.gson.reflect.TypeToken;
import com.hks360.library.jsonformat.util.GsonUtils;
import com.hks360.library.mndialog.MProgressDialog;
import com.hks360.library.pictureselector.SelectImageActivity;
import com.hks360.library.pictureselector.config.SelectOptions;
import com.hks360.library.rxeasyhttp.EasyHttp;
import com.hks360.library.rxeasyhttp.callback.SimpleCallBack;
import com.hks360.library.rxeasyhttp.exception.ApiException;
import com.hks360.library.rxeasyhttp.model.ApiResult;
import com.hks360.library.rxeasyhttp.token.TokenInterceptor;
import com.hks360.library.util.ErrorHelper;
import com.hks360.library.util.RetrofitUtil;
import com.hks360.library.xlog.XLog;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.entity.oss.CredentialData;
import com.xiaote.mydemo.testdemo.entity.oss.OssData;
import com.xiaote.mydemo.testdemo.web.Api;
import com.xiaote.mydemo.testdemo.web.WebSite;

import java.util.HashMap;
import java.util.Map;

public class OssTestActivity extends BaseActivity {
    private Button btnUploadEasy;
    private TextView tvSelectImage;
    private ImageView ivImage;

    private String ossToken;
    private String accessKeySecret;
    private String accessKeyId;
    private OSS mOss;
    private String mFilePath;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_oss;
    }

    @Override
    public int getTitleStringId() {
        return R.string.title_test_oss;
    }

    @Override
    public void setupView() {
        btnUploadEasy = findViewById(R.id.btn_upload_easy);
        tvSelectImage = findViewById(R.id.tv_select_image);
        ivImage = findViewById(R.id.iv_image);
    }

    private void initOss() {
        OSSCredentialProvider credentialProvider = new OSSStsTokenCredentialProvider(accessKeyId, accessKeySecret, ossToken);

        // 配置类如果不设置，会有默认配置。
        ClientConfiguration conf = new ClientConfiguration();
        conf.setConnectionTimeout(15 * 1000); // 连接超时，默认15秒。
        conf.setSocketTimeout(15 * 1000); // socket超时，默认15秒。
        conf.setMaxConcurrentRequest(5); // 最大并发请求数，默认5个。
        conf.setMaxErrorRetry(2); // 失败后最大重试次数，默认2次。
        OSSLog.enableLog();
        mOss = new OSSClient(getApplicationContext(), OSSConfig.END_POINT, credentialProvider);
    }

    @Override
    public void initData() {
        getOssToken();
    }

    @Override
    public void addListener() {
        btnUploadEasy.setOnClickListener(this);
        tvSelectImage.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_upload_easy:
                uploadEasy();
                break;
            case R.id.tv_select_image:
                selectImage();
                break;
            default:
                break;
        }
    }

    /**
     * 简单上传
     */
    private void uploadEasy() {
        XLog.e("简单上传");
        if (TextUtils.isEmpty(mFilePath)) {
            showToastMsg("请选择图片");
            return;
        }
        final String obejectKey = System.currentTimeMillis() + ".jpg";
        PutObjectRequest put = new PutObjectRequest(OSSConfig.BUCKET_NAME, obejectKey, mFilePath);
        put.setProgressCallback(new OSSProgressCallback<PutObjectRequest>() {
            @Override
            public void onProgress(PutObjectRequest request, long currentSize, long totalSize) {
                XLog.e("currentSize: " + currentSize + " totalSize: " + totalSize);
            }
        });
        OSSAsyncTask task = mOss.asyncPutObject(put, new OSSCompletedCallback<PutObjectRequest, PutObjectResult>() {
            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                XLog.e("UploadSuccess");
                XLog.e("eTag:" + result.getETag());
                XLog.e("requestId:" + result.getRequestId());
                XLog.e("body:" + result.getServerCallbackReturnBody());
            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientExcepion, ServiceException serviceException) {
                // 请求异常。
                if (clientExcepion != null) {
                    // 本地异常，如网络异常等。
                    clientExcepion.printStackTrace();
                }
                if (serviceException != null) {
                    // 服务异常。
                    XLog.e(serviceException.getErrorCode());
                    XLog.e(serviceException.getRequestId());
                    XLog.e(serviceException.getHostId());
                    XLog.e(serviceException.getRawMessage());
                }
            }
        });
    }

    /**
     * 选择图片
     */
    private void selectImage() {
        SelectImageActivity.show(this, new SelectOptions.Builder()
                .setHasCam(true)
                .setSelectCount(1)
                .setCallback(new SelectOptions.Callback() {
                    @Override
                    public void doSelected(String[] images) {
                        setImage(images[0]);
                    }
                }).build());
    }

    private void setImage(String path) {
        mFilePath = path;
        Glide.with(this).load(path).into(ivImage);
    }

    /**
     * 获取OSSToken
     */
    private void getOssToken() {
        MProgressDialog.showProgress(this);
        Map<String, String> params = new HashMap<>();
        params.put("createUser", "admin");
        EasyHttp.post(Api.GET_OSS_TOKEN)
                .baseUrl(WebSite.IM_API_BASE_URL_DEBUG)
                .requestBody(RetrofitUtil.getJSONRequestBodyForBean(params))
                .accessToken(true)
                .addInterceptor(new TokenInterceptor())
                .execute(new SimpleCallBack<String>() {
                    @Override
                    public void onError(ApiException e) {
                        MProgressDialog.dismissProgress();
                        ErrorHelper.onError(OssTestActivity.this, e);
                    }

                    @Override
                    public void onSuccess(String s) {
                        MProgressDialog.dismissProgress();
                        ApiResult<OssData> apiResult = GsonUtils.fromJson(s, new TypeToken<ApiResult<OssData>>() {
                        }.getType());
                        if (apiResult.isSuccess()) {
                            OssData ossData = apiResult.getRows();
                            CredentialData credentialData = ossData.getCredentials();
                            ossToken = credentialData.getSecurityToken();
                            accessKeyId = credentialData.getAccessKeyId();
                            accessKeySecret = credentialData.getAccessKeySecret();
                            XLog.e("ossToken:" + ossToken);
                            XLog.e("accessKeyId:" + accessKeyId);
                            XLog.e("accessKeySecret:" + accessKeySecret);
                            initOss();
                        } else {
                            showToastMsg(apiResult.getMsg());
                        }
                    }
                });
    }
}
