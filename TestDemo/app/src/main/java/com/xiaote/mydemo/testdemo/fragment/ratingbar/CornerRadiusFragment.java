package com.xiaote.mydemo.testdemo.fragment.ratingbar;

import android.view.MenuItem;
import android.view.View;

import com.hks360.library.widget.SimpleRatingBar;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.fragment.BaseFragment;

public class CornerRadiusFragment extends BaseFragment {

    private SimpleRatingBar ratingBar1;
    private SimpleRatingBar ratingBar2;
    private SimpleRatingBar ratingBar3;
    private SimpleRatingBar ratingBar4;
    private SimpleRatingBar ratingBar5;

    @Override
    public int getViewId() {
        return R.layout.fragment_corner_radius;
    }

    @Override
    public void setupView() {
        ratingBar1 = mRootView.findViewById(R.id.ratingBar1);
        ratingBar2 = mRootView.findViewById(R.id.ratingBar2);
        ratingBar3 = mRootView.findViewById(R.id.ratingBar3);
        ratingBar4 = mRootView.findViewById(R.id.ratingBar4);
        ratingBar5 = mRootView.findViewById(R.id.ratingBar5);
    }

    @Override
    public void initData() {
        setHasOptionsMenu(true);
    }

    @Override
    public void addListener() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_action_refresh:
                if (ratingBar1.getStarCornerRadius() <= 10) {
                    ratingBar1.setStarCornerRadius(ratingBar1.getStarCornerRadius() + 2);
                    ratingBar2.setStarCornerRadius(ratingBar2.getStarCornerRadius() + 2);
                    ratingBar3.setStarCornerRadius(ratingBar3.getStarCornerRadius() + 2);
                    ratingBar4.setStarCornerRadius(ratingBar4.getStarCornerRadius() + 2);
                    ratingBar5.setStarCornerRadius(ratingBar5.getStarCornerRadius() + 2);
                } else {
                    ratingBar1.setStarCornerRadius(0);
                    ratingBar2.setStarCornerRadius(2);
                    ratingBar3.setStarCornerRadius(4);
                    ratingBar4.setStarCornerRadius(6);
                    ratingBar5.setStarCornerRadius(8);
                }
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {

    }
}
