package com.xiaote.mydemo.testdemo.fragment.flyco;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.fragment.BaseFragment;

@SuppressLint("ValidFragment")
public class SimpleCardFragment extends BaseFragment {
    private TextView tvCardTitle;

    private String mTitle;

    public static SimpleCardFragment getInstance(String title) {
        SimpleCardFragment sf = new SimpleCardFragment();
        sf.mTitle = title;
        return sf;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getViewId() {
        return R.layout.fr_simple_card;
    }

    @Override
    public void setupView() {
        tvCardTitle = mRootView.findViewById(R.id.card_title_tv);
    }

    @Override
    public void initData() {
        tvCardTitle.setText(mTitle);
    }

    @Override
    public void addListener() {

    }

    @Override
    public void onClick(View v) {

    }
}