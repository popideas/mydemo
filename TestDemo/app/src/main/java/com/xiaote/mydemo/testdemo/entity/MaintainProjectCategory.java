package com.xiaote.mydemo.testdemo.entity;

import com.chad.library.adapter.base.entity.AbstractExpandableItem;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.google.gson.annotations.SerializedName;
import com.xiaote.mydemo.testdemo.adapter.MaintainProjectAdapter;

/**
 * @author ：xiaote
 * @date ：2019-08-08 14：17
 * 邮箱   ：xiao_t@hks360.com
 */
public class MaintainProjectCategory extends AbstractExpandableItem<MaintainProjectItem> implements MultiItemEntity {

    @SerializedName("fldm")
    private String categoryCode;
    @SerializedName("flname")
    private String categoryName;
    private int count;
    private String status;

    public String getCategoryCode() {
        return categoryCode == null ? "" : categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryName() {
        return categoryName == null ? "" : categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isValid() {
        return "true".equalsIgnoreCase(status);
    }

    public String getContentValue() {
        return getCategoryName() + "(" + getCount() + ")";
    }

    @Override
    public int getLevel() {
        return MaintainProjectAdapter.ITEM_CATEGORY;
    }

    @Override
    public int getItemType() {
        return MaintainProjectAdapter.ITEM_CATEGORY;
    }
}
