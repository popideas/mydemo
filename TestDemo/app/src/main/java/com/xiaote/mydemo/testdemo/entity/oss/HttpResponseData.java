package com.xiaote.mydemo.testdemo.entity.oss;

import com.google.gson.annotations.SerializedName;

public class HttpResponseData {
    @SerializedName("Status")
    private int status;
    @SerializedName("Url")
    private String url;
    @SerializedName("Method")
    private int method;
    @SerializedName("ContentType")
    private int contentType;
    @SerializedName("Content")
    private String content;
    @SerializedName("Encoding")
    private String encoding;
    @SerializedName("Headers")
    private HeaderBean headers;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getMethod() {
        return method;
    }

    public void setMethod(int method) {
        this.method = method;
    }

    public int getContentType() {
        return contentType;
    }

    public void setContentType(int contentType) {
        this.contentType = contentType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public HeaderBean getHeaders() {
        return headers;
    }

    public void setHeaders(HeaderBean headers) {
        this.headers = headers;
    }

    public static class HeaderBean {
        @SerializedName("Content-Type")
        private String contentType;
        @SerializedName("Content-Length")
        private String contentLength;

        public String getContentType() {
            return contentType;
        }

        public void setContentType(String contentType) {
            this.contentType = contentType;
        }

        public String getContentLength() {
            return contentLength;
        }

        public void setContentLength(String contentLength) {
            this.contentLength = contentLength;
        }
    }
}
