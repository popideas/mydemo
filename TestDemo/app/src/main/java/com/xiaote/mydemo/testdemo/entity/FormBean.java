package com.xiaote.mydemo.testdemo.entity;

/**
 * @author ：xiaote
 * @date ：2020-07-14 17：07
 * 邮箱   ：xiao_t@hks360.com
 */
public class FormBean {
    private String title1;
    private String title2;
    private String title3;
    private String title4;

    public FormBean() {
    }

    public FormBean(String title1, String title2, String title3, String title4) {
        this.title1 = title1;
        this.title2 = title2;
        this.title3 = title3;
        this.title4 = title4;
    }

    public String getTitle1() {
        return title1 == null ? "" : title1;
    }

    public void setTitle1(String title1) {
        this.title1 = title1;
    }

    public String getTitle2() {
        return title2 == null ? "" : title2;
    }

    public void setTitle2(String title2) {
        this.title2 = title2;
    }

    public String getTitle3() {
        return title3 == null ? "" : title3;
    }

    public void setTitle3(String title3) {
        this.title3 = title3;
    }

    public String getTitle4() {
        return title4 == null ? "" : title4;
    }

    public void setTitle4(String title4) {
        this.title4 = title4;
    }
}
