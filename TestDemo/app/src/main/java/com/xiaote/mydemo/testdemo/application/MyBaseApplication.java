package com.xiaote.mydemo.testdemo.application;


import com.hks360.library.application.BaseApplication;
import com.hks360.library.rxeasyhttp.EasyHttp;
import com.hks360.library.rxeasyhttp.cache.converter.SerializableDiskConverter;
import com.hks360.library.rxeasyhttp.model.HttpHeaders;
import com.hks360.library.rxeasyhttp.token.DynamicTokenInterceptor;
import com.hks360.library.rxeasyhttp.token.TokenInterceptor;
import com.hks360.library.util.PrefUtil;
import com.hks360.library.util.XLogUtil;
import com.xiaote.mydemo.testdemo.BuildConfig;
import com.xiaote.mydemo.testdemo.common.AppConfig;
import com.xiaote.mydemo.testdemo.web.WebSite;

/**
 * @author Xiaote
 */
public class MyBaseApplication extends BaseApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        PrefUtil.init(this, AppConfig.TAG);
        XLogUtil.init(AppConfig.TAG, BuildConfig.DEBUG, AppConfig.SHOW_BOARD);
        initEasyHttp();
    }

    public void initEasyHttp() {

        EasyHttp.init(this);
        //全局设置请求头
        HttpHeaders headers = new HttpHeaders();
        headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        //headers.put("Accept-Encoding", "gzip, deflate") //如果加这一句 log会乱码
        headers.put("Connection", "keep-alive");
        headers.put("Accept", "*/*");

        EasyHttp.getInstance()
                .debug(AppConfig.TAG, BuildConfig.DEBUG)
                .setBaseUrl(WebSite.WEB_API_BASE_URL_DEBUG)
                //如果使用默认的60秒,以下三行也不需要设置
                .setReadTimeOut(60 * 1000)
                .setWriteTimeOut(60 * 1000)
                .setConnectTimeout(60 * 1000)
                .addCommonHeaders(headers)//设置全局公共头
                .setCacheDiskConverter(new SerializableDiskConverter())//默认缓存使用序列化转化
                .addInterceptor(new DynamicTokenInterceptor()) //添加参数签名拦截器
                .addInterceptor(new TokenInterceptor())//Token异常拦截器
                .setCacheMaxSize(50 * 1024 * 1024)//设置缓存大小为50M
                .setCacheVersion(1)//缓存版本为1
                .setCertificates();//信任所有证书
        //.addConverterFactory(GsonConverterFactory.create(gson))//本框架没有采用Retrofit的Gson转化，所以不用配置
    }
}
