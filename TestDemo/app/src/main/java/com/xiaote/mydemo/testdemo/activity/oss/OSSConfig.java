package com.xiaote.mydemo.testdemo.activity.oss;

public class OSSConfig {
    public static final String END_POINT = "https://oss-cn-qingdao.aliyuncs.com";
    public static final String STS_URL = "https://hks-dev-test.oss-cn-qingdao.aliyuncs.com/";
    public static final String BUCKET_NAME = "hks-dev-test";
}
