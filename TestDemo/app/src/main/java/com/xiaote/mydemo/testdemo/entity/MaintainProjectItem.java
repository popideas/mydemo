package com.xiaote.mydemo.testdemo.entity;

import android.text.TextUtils;

import com.chad.library.adapter.base.entity.AbstractExpandableItem;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.google.gson.annotations.SerializedName;
import com.hks360.library.util.CommonUtil;
import com.xiaote.mydemo.testdemo.adapter.MaintainProjectAdapter;

/**
 * @author ：xiaote
 * @date ：2019-08-08 14：20
 * 邮箱   ：xiao_t@hks360.com
 */
public class MaintainProjectItem extends AbstractExpandableItem implements MultiItemEntity {

    @SerializedName("xmdm")
    private String projectCode;
    @SerializedName("wxxm")
    private String projectName;
    @SerializedName("gs")
    private String maintainHours;
    @SerializedName("fldm")
    private String categoryCode;
    private String position;
    private String bz;
    private String gz;
    private String je;
    private String price;
    @SerializedName("OrderNum")
    private String orderNum;
    private boolean isSelected;

    @Override
    public int getLevel() {
        return MaintainProjectAdapter.ITEM_PROJECT;
    }

    @Override
    public int getItemType() {
        return MaintainProjectAdapter.ITEM_PROJECT;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getMaintainHours() {
        return maintainHours;
    }

    public void setMaintainHours(String maintainHours) {
        this.maintainHours = maintainHours;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public String getGz() {
        return gz;
    }

    public void setGz(String gz) {
        this.gz = gz;
    }

    public String getJe() {
        return je;
    }

    public void setJe(String je) {
        this.je = je;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getPriceValue() {
        return TextUtils.isEmpty(price) ? "0.00" : CommonUtil.getMoneyValue(price);
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public void toggle() {
        this.isSelected = !this.isSelected;
    }
}
