package com.xiaote.mydemo.testdemo.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hks360.library.util.CommonUtil;
import com.hks360.library.util.ToastUtil;
import com.xiaote.mydemo.testdemo.R;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * @author Xiaote
 */
public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener {
    public LinearLayout mCommonTitleLayout;
    private TextView mCommonTitleTv;
    private View mTitleBarDividerView;
    public List<Disposable> mDisposables;

    /**
     * TODO 设置Activity加载布局LayoutId
     */
    public abstract int getLayoutId();

    /**
     * TODO 设置标题
     */
    public abstract int getTitleStringId();

    /**
     * TODO 初始化控件
     */
    public abstract void setupView();

    /**
     * TODO 初始化数据
     */
    public abstract void initData();

    /**
     * TODO 设置监听
     */
    public abstract void addListener();

    /**
     * TODO 初始化界面
     */
    private void initView() {
        mDisposables = new ArrayList<>();
        initTitleBar(getTitleStringId());
        setupView();
        initData();
        addListener();
    }

    private void initTitleBar(int titleResId) {
        if (titleResId != 0) {
            mCommonTitleLayout = findViewById(R.id.common_titlebar_layout);
            mTitleBarDividerView = findViewById(R.id.titlebar_divider);
            mCommonTitleTv = findViewById(R.id.common_title_tv);
            mCommonTitleTv.setText(titleResId);
        }
    }

    public void headTitleBarDivider() {
        mTitleBarDividerView.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        initView();
    }

    public void showToastMsg(String msg) {
        ToastUtil.showToast(this, msg);
    }

    public void back(View view) {
        finish();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(!CommonUtil.isEmpty(mDisposables)) {
            for(Disposable disposable : mDisposables) {
                if(disposable != null) {
                    disposable.dispose();
                }
            }
        }
    }

}
