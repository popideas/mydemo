package com.xiaote.mydemo.testdemo.adapter;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.entity.DistanceLearningEntity;

import java.util.List;

/**
 * @author Xiaote
 * @date 2018-09-12 11：10
 * @email xiao_t@hks360.com
 */
public class DistanceLearningAdapter extends BaseMultiItemQuickAdapter<DistanceLearningEntity, BaseViewHolder> {

    public DistanceLearningAdapter(List<DistanceLearningEntity> data) {
        super(data);
        addItemType(DistanceLearningEntity.ITEM_HEADER, R.layout.item_distance_learning_header);
        addItemType(DistanceLearningEntity.ITEM_CONTENT, R.layout.item_distance_learning_child);
        addItemType(DistanceLearningEntity.ITEM_FOOTER, R.layout.item_distance_learning_footer);
    }

    @Override
    protected void convert(BaseViewHolder helper, DistanceLearningEntity item) {
        switch (helper.getItemViewType()) {
            case DistanceLearningEntity.ITEM_HEADER:
                helper.setText(R.id.distance_learning_header_tv, item.getName());
                break;
            case DistanceLearningEntity.ITEM_CONTENT:
                helper.setText(R.id.distance_learning_child_tv, item.getName());
                break;
            case DistanceLearningEntity.ITEM_FOOTER:
                helper.setGone(R.id.distance_learning_footer_view, item.isShow());
                break;
            default:
                break;
        }
    }
}
