package com.xiaote.mydemo.testdemo.adapter;

import android.support.v4.content.ContextCompat;
import android.util.TypedValue;

import com.hks360.library.formlayout.adapter.BaseFormAdapter;
import com.hks360.library.util.UIUtil;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.entity.FormBean;

/**
 * @author ：xiaote
 * @date ：2020-07-14 17：09
 * 邮箱   ：xiao_t@hks360.com
 */
public class FormAdapter extends BaseFormAdapter<FormBean> {
    private int columnCount;

    public FormAdapter(int columCount) {
        this.columnCount = columCount;
    }

    @Override
    public int getColumnCount() {
        return columnCount;
    }

    @Override
    public String getRowData(FormBean item, int index) {
        switch (index) {
            case 0:
                return item.getTitle1();
            case 1:
                return item.getTitle2();
            case 2:
                return item.getTitle3();
            case 3:
                return item.getTitle4();
        }
        return "";
    }

    @Override
    public int getTextColor() {
        return ContextCompat.getColor(mContext,R.color.gray_33);
    }

    @Override
    public int getTextWidth() {
        return UIUtil.dp2px(120);
    }

    @Override
    public int getTextHeight() {
        return UIUtil.dp2px(40);
    }

    @Override
    public int getTextPadding() {
        return UIUtil.dp2px(5);
    }

    @Override
    public int getTextSizeUnit() {
        return TypedValue.COMPLEX_UNIT_DIP;
    }

    @Override
    public float getTextSize() {
        return 14f;
    }

}
