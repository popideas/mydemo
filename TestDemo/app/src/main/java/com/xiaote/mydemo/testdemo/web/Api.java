package com.xiaote.mydemo.testdemo.web;

/**
 * @author Xiaote
 * @date 2018-12-26 17：26
 * @email xiao_t@hks360.com
 */
public class Api {
    public static final String GET_ALL_SERVICES = "WXinfoservice/GetAllServices";
    public static final String GET_PACKAGE = "Package/GetPackage";
    public static final String GET_PARTS_INFO = "PartsInfo/GetPartsInfo";
    public static final String GET_OSS_TOKEN = "MessageCenter/v1/Common/GetStsToken";

    public static final String GET_MAINTAIN_PROJECT_CATEGORY = "recept/GetXmFlStore.aspx";
    public static final String GET_MAINTAIN_PROJECT_LIST = "recept/GetXmItemsStore.aspx";
    public static final String GET_PARTS_CASSIFY = "parts/GetPartsClassify.aspx";
    public static final String SEARCH_GCH_COUNT = "recept/SearchGchCount.aspx";
    public static final String SEARCH_GCH = "recept/SearchGch.aspx";
}
