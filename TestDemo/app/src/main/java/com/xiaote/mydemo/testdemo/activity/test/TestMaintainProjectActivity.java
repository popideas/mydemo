package com.xiaote.mydemo.testdemo.activity.test;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.google.gson.reflect.TypeToken;
import com.hks360.library.alertview.AlertView;
import com.hks360.library.jsonformat.util.GsonUtils;
import com.hks360.library.mndialog.MProgressDialog;
import com.hks360.library.rxeasyhttp.EasyHttp;
import com.hks360.library.rxeasyhttp.callback.SimpleCallBack;
import com.hks360.library.rxeasyhttp.exception.ApiException;
import com.hks360.library.util.CommonUtil;
import com.hks360.library.util.ErrorHelper;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.adapter.MaintainProjectAdapter;
import com.xiaote.mydemo.testdemo.entity.MaintainProjectCategory;
import com.xiaote.mydemo.testdemo.entity.MaintainProjectItem;
import com.xiaote.mydemo.testdemo.web.Api;
import com.xiaote.mydemo.testdemo.web.WebSite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：xiaote
 * @date ：2019-08-08 14：12
 * 邮箱   ：xiao_t@hks360.com
 */
public class TestMaintainProjectActivity extends BaseActivity {
    private RecyclerView mMaintainProjectRv;
    private Button mConfirmBtn;
    private ImageView mHeaderIv;

    private MaintainProjectAdapter mAdapter;
    private List<MaintainProjectCategory> mCategories;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_maintain_project;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_title_maintain_project;
    }

    @Override
    public void setupView() {
        mMaintainProjectRv = findViewById(R.id.maintain_project_rv);
        mConfirmBtn = findViewById(R.id.select_confirm_btn);
    }

    private void initHeaderView() {
        View headerView = LayoutInflater.from(this).inflate(R.layout.layout_maintain_project_header,
                mMaintainProjectRv, false);
        mHeaderIv = headerView.findViewById(R.id.image_view1);
        mAdapter.addHeaderView(headerView);
    }

    @Override
    public void initData() {
        mMaintainProjectRv.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new MaintainProjectAdapter();
        mMaintainProjectRv.setAdapter(mAdapter);
        initHeaderView();
        String url = "http://inthecheesefactory.com/uploads/source/glidepicasso/cover.jpg";
        Glide.with(this).load(url).into(mHeaderIv);
        getMaintainProjectCategory();
    }

    @Override
    public void addListener() {
        mConfirmBtn.setOnClickListener(this);
        mAdapter.setOnItemClickListener(new MyOnItemClickListener());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.select_confirm_btn:
                selectAll();
                break;
            default:
                break;
        }
    }

    private void selectAll() {
        List<MaintainProjectItem> selectItems = mAdapter.getSelectProjectList();
        if (CommonUtil.isEmpty(selectItems)) {
            showToastMsg("没有选择维修项目");
        } else {
            StringBuffer buffer = new StringBuffer();
            for (MaintainProjectItem projectItem : selectItems) {
                buffer.append(projectItem.getProjectName()).append(",");
            }
            buffer.deleteCharAt(buffer.length() - 1);
            new AlertView(this, "提示", "选择的项目有：" + buffer.toString(),
                    "取消", new String[]{"确定"}, AlertView.Style.Alert,
                    null).show();
        }
    }

    private class MyOnItemClickListener implements BaseQuickAdapter.OnItemClickListener {

        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            MultiItemEntity multiItemEntity = mAdapter.getItem(position);
            switch (multiItemEntity.getItemType()) {
                case MaintainProjectAdapter.ITEM_CATEGORY:
                    MaintainProjectCategory category = (MaintainProjectCategory) multiItemEntity;
                    if (category.isExpanded()) {
                        mAdapter.collapse(position + 1);
                    } else {
                        mAdapter.expand(position + 1);
                    }
                    break;
                case MaintainProjectAdapter.ITEM_PROJECT:
                    MaintainProjectItem item = (MaintainProjectItem) multiItemEntity;
                    item.toggle();
                    mAdapter.notifyItemChanged(position + 1);
                    break;
            }
        }
    }

    //TODO 接口调用在这里

    /**
     * 获取项目分类
     */
    private void getMaintainProjectCategory() {
        MProgressDialog.showProgress(this, "加载中...");
        Map<String, String> params = new HashMap<>();
        params.put("cid", "88888");
        params.put("ttime", "0");
        EasyHttp.post(Api.GET_MAINTAIN_PROJECT_CATEGORY)
                .baseUrl(WebSite.BASE_STORE_URL_DEBUG)
                .params("data", GsonUtils.toJson(params))
                .accessToken(false)
                .execute(new SimpleCallBack<String>() {
                    @Override
                    public void onError(ApiException e) {
                        MProgressDialog.dismissProgress();
                        ErrorHelper.onError(TestMaintainProjectActivity.this, e);
                    }

                    @Override
                    public void onSuccess(String s) {
                        List<MaintainProjectCategory> list = GsonUtils.fromJson(s,
                                new TypeToken<List<MaintainProjectCategory>>() {
                                }.getType());
                        if (!CommonUtil.isEmpty(list)) {
                            if (mCategories == null) {
                                mCategories = new ArrayList<>();
                            } else {
                                mCategories.clear();
                            }
                            for (MaintainProjectCategory category : list) {
                                if (category.isValid()) {
                                    mCategories.add(category);
                                }
                            }
                            getMaintainProjectItem();
                        } else {
                            MProgressDialog.dismissProgress();
                            showToastMsg("没有数据");
                        }
                    }
                });
    }

    /**
     * 获取维修项目
     */
    private void getMaintainProjectItem() {
        Map<String, String> params = new HashMap<>();
        params.put("cid", "88888");
        params.put("ttime", "0");
        EasyHttp.post(Api.GET_MAINTAIN_PROJECT_LIST)
                .baseUrl(WebSite.BASE_STORE_URL_DEBUG)
                .params("data", GsonUtils.toJson(params))
                .accessToken(false)
                .execute(new SimpleCallBack<String>() {

                    @Override
                    public void onError(ApiException e) {
                        MProgressDialog.dismissProgress();
                        ErrorHelper.onError(TestMaintainProjectActivity.this, e);
                    }

                    @Override
                    public void onSuccess(String s) {
                        MProgressDialog.dismissProgress();
                        List<MaintainProjectItem> list = GsonUtils.fromJson(s,
                                new TypeToken<List<MaintainProjectItem>>() {
                                }.getType());
                        parseMaintainProjectItem(list);
                    }
                });
    }

    private void parseMaintainProjectItem(List<MaintainProjectItem> itemList) {
        List<MultiItemEntity> entities = new ArrayList<>();
        for (MaintainProjectCategory category : mCategories) {
            int count = 0;
            for (MaintainProjectItem item : itemList) {
                if (TextUtils.equals(category.getCategoryCode(), item.getCategoryCode())) {
                    category.addSubItem(item);
                    count++;
                }
            }
            category.setCount(count);
            entities.add(category);
        }
        mAdapter.setNewData(entities);
    }
}
