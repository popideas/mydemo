package com.xiaote.mydemo.testdemo.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hks360.library.widget.ExpandableLayout;
import com.xiaote.mydemo.testdemo.R;

import java.util.List;

/**
 * @author Xiaote
 * @date 2018/7/2 16:16
 * @email xiao_t@hks360.com
 */
public class ExpandableTextViewAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public ExpandableTextViewAdapter(@Nullable List<String> data) {
        super(R.layout.item_expandable_textview, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        ExpandableLayout expandableLayout = helper.getView(R.id.expand_text_layout);
        expandableLayout.setText(item);
    }
}
