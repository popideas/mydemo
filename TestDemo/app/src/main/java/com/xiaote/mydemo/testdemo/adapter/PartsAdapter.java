package com.xiaote.mydemo.testdemo.adapter;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.entity.PartsCategory;
import com.xiaote.mydemo.testdemo.entity.PartsClassify;
import com.xiaote.mydemo.testdemo.entity.PartsInfo;

/**
 * @author ：xiaote
 * @date ：2019-09-09 09：19
 * 邮箱   ：xiao_t@hks360.com
 */
public class PartsAdapter extends BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder> {
    public static final int ITEM_CLASSIFY = 0;
    public static final int ITEM_CATEGORY = 1;
    public static final int ITEM_PARTS = 2;

    public PartsAdapter() {
        super(null);
        addItemType(ITEM_CLASSIFY, R.layout.item_parts_classify);
        addItemType(ITEM_CATEGORY, R.layout.item_parts_category);
        addItemType(ITEM_PARTS, R.layout.item_parts_info);
    }

    @Override
    protected void convert(BaseViewHolder helper, MultiItemEntity item) {
        switch (item.getItemType()) {
            case ITEM_CLASSIFY:
                PartsClassify partsClassify = (PartsClassify) item;
                helper.setText(R.id.classify_content_tv, partsClassify.getClassifyContent());
                helper.setImageResource(R.id.classify_arrow_iv, partsClassify.
                        isExpanded() ? R.drawable.arrowup : R.drawable.arrowdown);
                break;
            case ITEM_CATEGORY:
                PartsCategory partsCategory = (PartsCategory) item;
                if (partsCategory.isCategory()) {
                    helper.setGone(R.id.parts_category_layout, true);
                    helper.setGone(R.id.category_to_parts_info_layout, false);
                    helper.setImageResource(R.id.parts_category_iv, partsCategory.isExpanded() ?
                            R.drawable.outline_list_expand : R.drawable.outline_list_collapse);
                    PartsClassify classify = partsCategory.getPartsClassify();
                    helper.setText(R.id.parts_category_tv, classify.getClassifyCount());
                } else {
                    helper.setGone(R.id.parts_category_layout, false);
                    helper.setGone(R.id.category_to_parts_info_layout, true);
                    PartsInfo info = partsCategory.getPartsInfo();
                    helper.setText(R.id.parts_info_name_tv, info.getPname());
                    helper.setText(R.id.parts_info_count_unit_tv, info.getCountUnit());
                    helper.setText(R.id.parts_info_code_tv, info.getPcode());
                    helper.setText(R.id.parts_info_price_tv, info.getMoneyUnit());
                    switch (info.getType()) {
                        case -1:
                            helper.setBackgroundRes(R.id.category_to_parts_info_layout, R.color.gray_e4);
                            break;
                        case 0:
                        default:
                            helper.setBackgroundRes(R.id.category_to_parts_info_layout, R.color.white);
                            break;
                        case 1:
                            helper.setBackgroundRes(R.id.category_to_parts_info_layout, R.color.yellow_color3);
                            break;
                    }
                }
                helper.addOnClickListener(R.id.parts_info_code_tv);
                break;
            case ITEM_PARTS:
                PartsInfo partsInfo = (PartsInfo) item;
                helper.setText(R.id.parts_name_tv, partsInfo.getPname());
                helper.setText(R.id.part_count_unit_tv, partsInfo.getCountUnit());
                helper.setText(R.id.parts_code_tv, partsInfo.getPcode());
                helper.setText(R.id.parts_price_tv, partsInfo.getMoneyUnit());
                switch (partsInfo.getType()) {
                    case -1:
                        helper.setBackgroundRes(R.id.parts_info_layout, R.color.gray_e4);
                        break;
                    case 0:
                    default:
                        helper.setBackgroundRes(R.id.parts_info_layout, R.color.white);
                        break;
                    case 1:
                        helper.setBackgroundRes(R.id.parts_info_layout, R.color.yellow_color3);
                        break;
                }
                helper.addOnClickListener(R.id.parts_code_tv);
                break;
            default:
                break;
        }
    }
}
