package com.xiaote.mydemo.testdemo.common;

/**
 * @author Xiaote
 */
public class AppConfig {
    public static final String TAG = "mydemo";
    public static final boolean SHOW_BOARD = false;
}
