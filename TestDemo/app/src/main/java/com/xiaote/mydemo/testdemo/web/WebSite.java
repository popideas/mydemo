package com.xiaote.mydemo.testdemo.web;

/**
 * @author Xiaote
 * @date 2018-08-15 13：50
 * @email xiao_t@hks360.com
 */
public class WebSite {
    public static final String BASE_STORE_URL_DEBUG = "http://121.42.248.165:13004/";
    public static final String WEB_API_BASE_URL_DEBUG = "http://121.42.248.165:84/";
    public static final String IM_API_BASE_URL_DEBUG = "https://test-040-13011.hks360.com/";
}
