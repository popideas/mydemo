package com.xiaote.mydemo.testdemo.entity;

/**
 * @author ：xiaote
 * @date ：2020-07-15 16：20
 * 邮箱   ：xiao_t@hks360.com
 */
public class ReportCustomer {
    private String customerName;
    private String overdueReceivable;
    private String accountReceivable;
    private String accountBalance;

    public ReportCustomer() {
    }

    public String getCustomerName() {
        return customerName == null ? "" : customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getOverdueReceivable() {
        return overdueReceivable == null ? "" : overdueReceivable;
    }

    public void setOverdueReceivable(String overdueReceivable) {
        this.overdueReceivable = overdueReceivable;
    }

    public String getAccountReceivable() {
        return accountReceivable == null ? "" : accountReceivable;
    }

    public void setAccountReceivable(String accountReceivable) {
        this.accountReceivable = accountReceivable;
    }

    public String getAccountBalance() {
        return accountBalance == null ? "" : accountBalance;
    }

    public void setAccountBalance(String accountBalance) {
        this.accountBalance = accountBalance;
    }
}
