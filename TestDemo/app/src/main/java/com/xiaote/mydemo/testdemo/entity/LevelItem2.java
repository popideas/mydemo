package com.xiaote.mydemo.testdemo.entity;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.xiaote.mydemo.testdemo.adapter.ExpandableAdapter;

/**
 * @author Xiaote
 * @date 2018/7/2 11:51
 * @email xiao_t@hks360.com
 */
public class LevelItem2 implements MultiItemEntity {

    private String code;
    private String name;

    public LevelItem2() {

    }

    public LevelItem2(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getItemType() {
        return ExpandableAdapter.TYPE_LEVEL_2;
    }
}
