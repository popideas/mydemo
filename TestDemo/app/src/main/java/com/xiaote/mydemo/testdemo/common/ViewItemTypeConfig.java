package com.xiaote.mydemo.testdemo.common;

/**
 * @author Xiaote
 * @date 2018-09-14 13：26
 * @email xiao_t@hks360.com
 */
public class ViewItemTypeConfig {
    public static final int ITEM_HEADER = 1;
    public static final int ITEM_CONTENT = 2;
    public static final int ITEM_FOOTER = 3;
}
