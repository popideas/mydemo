package com.xiaote.mydemo.testdemo.activity.parts;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.reflect.TypeToken;
import com.hks360.library.jsonformat.util.GsonUtils;
import com.hks360.library.pullrefreshlayout.PullRefreshLayout;
import com.hks360.library.pullrefreshlayout.listener.OnPullRefreshListener;
import com.hks360.library.pullrefreshlayout.widget.ClassicsHeader;
import com.hks360.library.recyclerview.divider.DividerListItemDecoration;
import com.hks360.library.rxeasyhttp.EasyHttp;
import com.hks360.library.rxeasyhttp.callback.SimpleCallBack;
import com.hks360.library.rxeasyhttp.exception.ApiException;
import com.hks360.library.rxeasyhttp.model.ApiResult;
import com.hks360.library.util.CommonUtil;
import com.hks360.library.util.ErrorHelper;
import com.hks360.library.util.RetrofitUtil;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.adapter.PartsInfoAdapter;
import com.xiaote.mydemo.testdemo.common.IntentKey;
import com.xiaote.mydemo.testdemo.entity.PartsInfo;
import com.xiaote.mydemo.testdemo.entity.RequestPartsInfo;
import com.xiaote.mydemo.testdemo.web.Api;
import com.xiaote.mydemo.testdemo.web.WebSite;

import java.util.List;

/**
 * @author ：xiaote
 * @date ：2019-09-10 09：13
 * 邮箱   ：xiao_t@hks360.com
 */
public class PartsInfoAllActivity extends BaseActivity {
    private PullRefreshLayout mPartsInfoRefreshLayout;
    private RecyclerView mPartsInfoRv;

    private PartsInfoAdapter mAdapter;

    private int mPageId = 1;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_parts_info_all;
    }

    @Override
    public int getTitleStringId() {
        return R.string.parts_info_all;
    }

    @Override
    public void setupView() {
        mPartsInfoRefreshLayout = findViewById(R.id.parts_info_all_pull_refresh_layout);
        mPartsInfoRv = findViewById(R.id.parts_info_all_rv);
    }

    @Override
    public void initData() {
        mPageId = 1;
        mPartsInfoRefreshLayout.setRefreshEnable(true);
        mPartsInfoRefreshLayout.setLoadMoreEnable(false);
        mPartsInfoRefreshLayout.setHeaderView(new ClassicsHeader(this));
        mPartsInfoRv.setLayoutManager(new LinearLayoutManager(this));
        mPartsInfoRv.addItemDecoration(new DividerListItemDecoration(this));
        mAdapter = new PartsInfoAdapter();
        mPartsInfoRv.setAdapter(mAdapter);
        mPartsInfoRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                getDataList(true);
            }
        }, 150);
    }

    @Override
    public void addListener() {
        mPartsInfoRefreshLayout.setOnRefreshListener(new MyOnRefreshListener());
        mAdapter.setOnLoadMoreListener(new MyOnLoadMoreListener(), mPartsInfoRv);
        mAdapter.setOnItemChildClickListener(new MyOnItemChildClickListener());
    }

    @Override
    public void onClick(View view) {

    }

    private class MyOnRefreshListener extends OnPullRefreshListener {

        @Override
        public void onRefresh() {
            mPageId = 1;
            getDataList(true);
        }
    }

    private class MyOnLoadMoreListener implements BaseQuickAdapter.RequestLoadMoreListener {

        @Override
        public void onLoadMoreRequested() {
            mPageId++;
            getDataList(false);
        }
    }

    private class MyOnItemChildClickListener implements BaseQuickAdapter.OnItemChildClickListener {

        @Override
        public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
            PartsInfo partsInfo = mAdapter.getItem(position);
            switch (view.getId()) {
                case R.id.parts_code_tv:
                    showDetail(partsInfo);
                    break;
                default:
                    break;
            }
        }
    }

    private void showDetail(PartsInfo partsInfo) {
        Intent intent = new Intent();
        intent.setClass(this, PartsInfoDetailActivity.class);
        intent.putExtra(IntentKey.PARTS_INFO, partsInfo);
        startActivity(intent);
    }

    private RequestPartsInfo createRequestPartsInfo() {
        RequestPartsInfo partsInfo = new RequestPartsInfo();
        partsInfo.setCid("88888");
        partsInfo.setPcode("");
        partsInfo.setPname("");
        partsInfo.setCount("");
        partsInfo.setValid("1");
        partsInfo.setClassifycode("");
        partsInfo.setSearchinfo("");
        partsInfo.setPym("");
        partsInfo.setCartypecode("");
        partsInfo.setHkscode("");
        String pageId = String.valueOf(mPageId);
        partsInfo.setPageId(pageId);
        if (!TextUtils.isEmpty(pageId)) {
            partsInfo.setPageSize("10");
        }
        return partsInfo;
    }

    private void getDataList(final boolean isRefresh) {
        RequestPartsInfo requestPartsInfo = createRequestPartsInfo();
        EasyHttp.post(Api.GET_PARTS_INFO)
                .baseUrl(WebSite.WEB_API_BASE_URL_DEBUG)
                .requestBody(RetrofitUtil.getJSONRequestBodyForBean(requestPartsInfo))
                .accessToken(true)
                .execute(new SimpleCallBack<String>() {
                    @Override
                    public void onError(ApiException e) {
                        mPartsInfoRefreshLayout.refreshComplete();
                        mAdapter.loadMoreComplete();
                        ErrorHelper.onError(PartsInfoAllActivity.this, e);
                    }

                    @Override
                    public void onSuccess(String s) {
                        mPartsInfoRefreshLayout.refreshComplete();
                        mAdapter.loadMoreComplete();
                        ApiResult<List<PartsInfo>> apiResult = GsonUtils.fromJson(s,
                                new TypeToken<ApiResult<List<PartsInfo>>>() {
                                }.getType());
                        if (apiResult.isSuccess()) {
                            List<PartsInfo> partsInfoList = apiResult.getRows();
                            parsePartsInfoList(partsInfoList, isRefresh);
                        } else {
                            showToastMsg(apiResult.getMsg());
                        }
                    }

                });
    }

    private void parsePartsInfoList(List<PartsInfo> partsInfoList, boolean isRefresh) {
        if (isRefresh) {
            mAdapter.setNewData(partsInfoList);
        } else {
            mAdapter.addData(partsInfoList);
        }
        if (CommonUtil.isEmpty(partsInfoList) || partsInfoList.size() < 10) {
            mAdapter.loadMoreEnd();
        }
    }
}
