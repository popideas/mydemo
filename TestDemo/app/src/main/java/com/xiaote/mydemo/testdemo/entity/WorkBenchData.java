package com.xiaote.mydemo.testdemo.entity;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * @author Xiaote
 * @date 2018-09-27 10：42
 * @email xiao_t@hks360.com
 */
public class WorkBenchData implements MultiItemEntity {
    public static final int ITEM_HEADER  = 1;
    public static final int ITEM_CONTENT = 2;
    public static final int ITEM_FOOTER = 3;

    private int imgResId;
    private String name;
    private int type;
    private boolean showRightLine;
    private boolean showBottomLine;

    public WorkBenchData() {

    }

    public WorkBenchData(String name, int type) {
        this.name = name;
        this.type = type;
    }

    public WorkBenchData(int imgResId, String name, int type) {
        this.imgResId = imgResId;
        this.name = name;
        this.type = type;
    }

    public WorkBenchData(int imgResId, String name, int type, boolean showRightLine, boolean showBottomLine) {
        this.imgResId = imgResId;
        this.name = name;
        this.type = type;
        this.showRightLine = showRightLine;
        this.showBottomLine = showBottomLine;
    }

    public int getImgResId() {
        return imgResId;
    }

    public void setImgResId(int imgResId) {
        this.imgResId = imgResId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isShowRightLine() {
        return showRightLine;
    }

    public void setShowRightLine(boolean showRightLine) {
        this.showRightLine = showRightLine;
    }

    public boolean isShowBottomLine() {
        return showBottomLine;
    }

    public void setShowBottomLine(boolean showBottomLine) {
        this.showBottomLine = showBottomLine;
    }

    @Override
    public int getItemType() {
        return type;
    }
}
