package com.xiaote.mydemo.testdemo.activity.test;

import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.hks360.library.widget.NumberProgressBar;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;

/**
 * @author Xiaote
 * @date 2018/7/2 16:59
 * @email xiao_t@hks360.com
 */
public class TestNumberProgressBarActivity extends BaseActivity {
    private NumberProgressBar progressBar1;

    private MyRunnable mRunnable;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    handler.postDelayed(mRunnable, 200);
                    break;
            }
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_numberprogressbar;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_title_number_progress_bar;
    }

    @Override
    public void setupView() {
        progressBar1 = findViewById(R.id.numberbar1);
    }

    @Override
    public void initData() {
        mRunnable = new MyRunnable();
        handler.postDelayed(mRunnable, 1000);
    }

    @Override
    public void addListener() {

    }

    @Override
    public void onClick(View v) {

    }

    private class MyRunnable implements Runnable {

        @Override
        public void run() {
            progressBar1.incrementProgressBy(1);
            Message msg = Message.obtain();
            msg.what = 1;
            handler.sendMessage(msg);
        }
    }
}
