package com.xiaote.mydemo.testdemo.activity.test;

import android.view.View;
import android.widget.Button;

import com.hks360.library.dialog.MyCustomerDialog;
import com.hks360.library.util.DialogUtil;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;

/**
 * @author ：xiaote
 * @date ：2020-01-17 13：27
 * 邮箱   ：xiao_t@hks360.com
 */
public class TestDialogUtilActivity extends BaseActivity {
    private Button btnShowAlert;
    private Button btnShowDialog;
    private Button btnMyCustomerDialog;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_dialog_util;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_title_dialog_util;
    }

    @Override
    public void setupView() {
        btnShowAlert = findViewById(R.id.btn_alert_view_confirm);
        btnShowDialog = findViewById(R.id.btn_dialog_confirm);
        btnMyCustomerDialog = findViewById(R.id.btn_dialog_customer);
    }

    @Override
    public void initData() {

    }

    @Override
    public void addListener() {
        btnShowAlert.setOnClickListener(this);
        btnShowDialog.setOnClickListener(this);
        btnMyCustomerDialog.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_alert_view_confirm:
                DialogUtil.showConfirmAlert(this, "提示", "这是一个AlertView", "");
                break;
            case R.id.btn_dialog_confirm:
                DialogUtil.showConfirmDialog(this, "提示", "这是一个AlertDialog", "");
                break;
            case R.id.btn_dialog_customer:
                new MyCustomerDialog(this).setTitle("提示").setMessage("这是一个MyCustomerDialog")
                        .setOnClickListener(new MyCustomerDialog.DialogOnClickListener() {
                            @Override
                            public void onConfirm() {

                            }

                            @Override
                            public void onCancel() {

                            }
                        }).show();
                return;
            default:
                break;
        }
    }
}
