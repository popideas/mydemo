package com.xiaote.mydemo.testdemo.entity.oss;

import com.google.gson.annotations.SerializedName;

public class OssData {
    @SerializedName("RequestId")
    private String requestId;
    @SerializedName("Credentials")
    private CredentialData credentials;
    @SerializedName("AssumedRoleUser")
    private AssumedRoleUserData assumedRoleUser;
    @SerializedName("HttpResponse")
    private HttpResponseData httpResponse;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public CredentialData getCredentials() {
        return credentials;
    }

    public void setCredentials(CredentialData credentials) {
        this.credentials = credentials;
    }

    public AssumedRoleUserData getAssumedRoleUser() {
        return assumedRoleUser;
    }

    public void setAssumedRoleUser(AssumedRoleUserData assumedRoleUser) {
        this.assumedRoleUser = assumedRoleUser;
    }

    public HttpResponseData getHttpResponse() {
        return httpResponse;
    }

    public void setHttpResponse(HttpResponseData httpResponse) {
        this.httpResponse = httpResponse;
    }
}
