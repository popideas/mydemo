package com.xiaote.mydemo.testdemo.entity;

import com.chad.library.adapter.base.entity.AbstractExpandableItem;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.xiaote.mydemo.testdemo.adapter.PartsAdapter;

/**
 * @author ：xiaote
 * @date ：2019-09-06 09：46
 * 邮箱   ：xiao_t@hks360.com
 */
public class PartsCategory extends AbstractExpandableItem<PartsInfo> implements MultiItemEntity {
    private boolean isCategory;
    private PartsClassify partsClassify;
    private PartsInfo mPartsInfo;

    public boolean isCategory() {
        return isCategory;
    }

    public void setCategory(boolean category) {
        isCategory = category;
    }

    public PartsClassify getPartsClassify() {
        return partsClassify;
    }

    public void setPartsClassify(PartsClassify partsClassify) {
        this.partsClassify = partsClassify;
    }

    public PartsInfo getPartsInfo() {
        return mPartsInfo;
    }

    public void setPartsInfo(PartsInfo partsInfo) {
        mPartsInfo = partsInfo;
    }

    @Override
    public int getLevel() {
        return PartsAdapter.ITEM_CATEGORY;
    }

    @Override
    public int getItemType() {
        return PartsAdapter.ITEM_CATEGORY;
    }
}
