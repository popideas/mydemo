package com.xiaote.mydemo.testdemo.entity;

import com.chad.library.adapter.base.entity.AbstractExpandableItem;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.xiaote.mydemo.testdemo.adapter.ExpandableAdapter;

/**
 * @author Xiaote
 * @date 2018/7/2 11:50
 * @email xiao_t@hks360.com
 */
public class LevelItem1 extends AbstractExpandableItem<LevelItem2> implements MultiItemEntity {
    private String name;

    public LevelItem1() {

    }

    public LevelItem1(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getLevel() {
        return 1;
    }

    @Override
    public int getItemType() {
        return ExpandableAdapter.TYPE_LEVEL_1;
    }
}
