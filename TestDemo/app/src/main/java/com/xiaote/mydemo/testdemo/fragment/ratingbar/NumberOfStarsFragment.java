package com.xiaote.mydemo.testdemo.fragment.ratingbar;

import android.view.MenuItem;
import android.view.View;

import com.hks360.library.widget.SimpleRatingBar;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.fragment.BaseFragment;

public class NumberOfStarsFragment extends BaseFragment {

    private SimpleRatingBar ratingBar1;
    private SimpleRatingBar ratingBar2;
    private SimpleRatingBar ratingBar3;
    private SimpleRatingBar ratingBar4;
    private SimpleRatingBar ratingBar5;

    @Override
    public int getViewId() {
        return R.layout.fragment_number_of_stars;
    }

    @Override
    public void setupView() {
        ratingBar1 = mRootView.findViewById(R.id.ratingBar1);
        ratingBar2 = mRootView.findViewById(R.id.ratingBar2);
        ratingBar3 = mRootView.findViewById(R.id.ratingBar3);
        ratingBar4 = mRootView.findViewById(R.id.ratingBar4);
        ratingBar5 = mRootView.findViewById(R.id.ratingBar5);
    }

    @Override
    public void initData() {
        setHasOptionsMenu(true);
    }

    @Override
    public void addListener() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_action_refresh:
                if (ratingBar1.getNumberOfStars() == 5) {
                    ratingBar1.setNumberOfStars(1);
                    ratingBar2.setNumberOfStars(2);
                    ratingBar3.setNumberOfStars(3);
                    ratingBar4.setNumberOfStars(4);
                    ratingBar5.setNumberOfStars(5);
                } else {
                    ratingBar1.setNumberOfStars(5);
                    ratingBar2.setNumberOfStars(4);
                    ratingBar3.setNumberOfStars(3);
                    ratingBar4.setNumberOfStars(2);
                    ratingBar5.setNumberOfStars(1);
                }
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {

    }
}
