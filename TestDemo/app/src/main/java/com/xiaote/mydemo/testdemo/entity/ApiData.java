package com.xiaote.mydemo.testdemo.entity;

/**
 * Created by xiaote on 2018/3/24.
 */

public class ApiData {

    private String msg;
    private String obj;
    private Object rows;
    private boolean success;
    private int total;
    private int failure;
    private String failureMsg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getObj() {
        return obj;
    }

    public void setObj(String obj) {
        this.obj = obj;
    }

    public Object getRows() {
        return rows;
    }

    public void setRows(Object rows) {
        this.rows = rows;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getFailure() {
        return failure;
    }

    public void setFailure(int failure) {
        this.failure = failure;
    }

    public String getFailureMsg() {
        return failureMsg;
    }

    public void setFailureMsg(String failureMsg) {
        this.failureMsg = failureMsg;
    }

    @Override
    public String toString() {
        return "ApiData{" +
                "msg='" + msg + '\'' +
                ", obj='" + obj + '\'' +
                ", rows='" + rows + '\'' +
                ", success=" + success +
                ", total=" + total +
                ", failure=" + failure +
                ", failureMsg='" + failureMsg + '\'' +
                '}';
    }
}
