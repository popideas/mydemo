package com.xiaote.mydemo.testdemo.activity.test;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.fragment.test.TestExpandableTextFragment;
import com.xiaote.mydemo.testdemo.fragment.test.TestExpandableTextListFragment;

/**
 * @author Xiaote
 * @date 2018/7/2 15:29
 * @email xiao_t@hks360.com
 */
public class TestExpandableTextActivity extends BaseActivity {
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private SectionsPagerAdapter mSectionsPagerAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_expandable_textview;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_title_expandable_textview;
    }

    @Override
    public void setupView() {
        mTabLayout = findViewById(R.id.expandable_textview_tablayout);
        mViewPager = findViewById(R.id.expandable_textview_viewpager);
    }

    @Override
    public void initData() {
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public void addListener() {

    }

    @Override
    public void onClick(View v) {

    }

    class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return new TestExpandableTextFragment();
            } else {
                return new TestExpandableTextListFragment();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "In ScrollView";
                case 1:
                    return "In RecyclerView";
            }
            return null;
        }
    }
}
