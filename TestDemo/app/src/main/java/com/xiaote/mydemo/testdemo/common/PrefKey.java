package com.xiaote.mydemo.testdemo.common;

/**
 * @author Xiaote
 * @date 2018-08-15 13：55
 * @email xiao_t@hks360.com
 */
public class PrefKey {
    public static final String ACCESS_TOKEN = "access_token";
}
