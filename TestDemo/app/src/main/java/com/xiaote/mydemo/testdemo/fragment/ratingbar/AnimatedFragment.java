package com.xiaote.mydemo.testdemo.fragment.ratingbar;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import com.hks360.library.widget.SimpleRatingBar;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.fragment.BaseFragment;

public class AnimatedFragment extends BaseFragment {

    private SimpleRatingBar ratingBar1;
    private SimpleRatingBar ratingBar2;
    private SimpleRatingBar ratingBar3;
    private SimpleRatingBar ratingBar4;
    private SimpleRatingBar ratingBar5;

    @Override
    public int getViewId() {
        return R.layout.fragment_animated;
    }

    @Override
    public void setupView() {
        ratingBar1 = mRootView.findViewById(R.id.ratingBar1);
        ratingBar2 = mRootView.findViewById(R.id.ratingBar2);
        ratingBar3 = mRootView.findViewById(R.id.ratingBar3);
        ratingBar4 = mRootView.findViewById(R.id.ratingBar4);
        ratingBar5 = mRootView.findViewById(R.id.ratingBar5);
    }

    @Override
    public void initData() {
        startAnimations();
    }

    @Override
    public void addListener() {

    }

    private void startAnimations() {
        ratingBar1.getAnimationBuilder()
                .setRepeatCount(ValueAnimator.INFINITE)
                .setRepeatMode(ValueAnimator.REVERSE)
                .setInterpolator(new LinearInterpolator())
                .setRatingTarget(4)
                .start();

        ratingBar2.getAnimationBuilder()
                .setRepeatCount(ValueAnimator.INFINITE)
                .setRepeatMode(ValueAnimator.REVERSE)
                .setInterpolator(new AccelerateInterpolator(1.5f))
                .setRatingTarget(4)
                .start();

        ratingBar3.getAnimationBuilder()
                .setRepeatCount(ValueAnimator.INFINITE)
                .setRepeatMode(ValueAnimator.REVERSE)
                .setInterpolator(new DecelerateInterpolator(1.5f))
                .setRatingTarget(4)
                .setDuration(1500)
                .start();

        ratingBar4.getAnimationBuilder()
                .setRepeatCount(ValueAnimator.INFINITE)
                .setRepeatMode(ValueAnimator.RESTART)
                .setInterpolator(new BounceInterpolator())
                .setRatingTarget(4)
                .start();

        ratingBar5.getAnimationBuilder()
                .setRepeatCount(ValueAnimator.INFINITE)
                .setRepeatMode(ValueAnimator.RESTART)
                .setInterpolator(new OvershootInterpolator(2f))
                .setRatingTarget(4)
                .start();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {

    }
}
