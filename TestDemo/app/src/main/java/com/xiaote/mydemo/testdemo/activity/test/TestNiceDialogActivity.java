package com.xiaote.mydemo.testdemo.activity.test;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.hks360.library.nicedialog.BaseNiceDialog;
import com.hks360.library.nicedialog.NiceDialog;
import com.hks360.library.nicedialog.ViewConvertListener;
import com.hks360.library.nicedialog.ViewHolder;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;

/**
 * @author Xiaote
 * @date 2018-07-13 11：36
 * @email xiao_t@hks360.com
 */
public class TestNiceDialogActivity extends BaseActivity {
    private Button btnShowDialog1;
    private Button btnShowDialog2;
    private Button btnShowDialog3;
    private Button btnShowDialog4;
    private Button btnShowDialog5;
    private Button btnShowDialog6;
    private Button btnShowDialog7;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_nice_dialog;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_title_nice_dialog;
    }

    @Override
    public void setupView() {
        btnShowDialog1 = findViewById(R.id.showDialog1);
        btnShowDialog2 = findViewById(R.id.showDialog2);
        btnShowDialog3 = findViewById(R.id.showDialog3);
        btnShowDialog4 = findViewById(R.id.showDialog4);
        btnShowDialog5 = findViewById(R.id.showDialog5);
        btnShowDialog6 = findViewById(R.id.showDialog6);
        btnShowDialog7 = findViewById(R.id.showDialog7);
    }

    @Override
    public void initData() {

    }

    @Override
    public void addListener() {
        btnShowDialog1.setOnClickListener(this);
        btnShowDialog2.setOnClickListener(this);
        btnShowDialog3.setOnClickListener(this);
        btnShowDialog4.setOnClickListener(this);
        btnShowDialog5.setOnClickListener(this);
        btnShowDialog6.setOnClickListener(this);
        btnShowDialog7.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.showDialog1:
                showDialogOne();
                break;
            case R.id.showDialog2:
                showDialogTwo();
                break;
            case R.id.showDialog3:
                showDialogThree();
                break;
            case R.id.showDialog4:
                showDialogFour();
                break;
            case R.id.showDialog5:
                showDialogFive();
                break;
            case R.id.showDialog6:
                showDialogSix();
                break;
            case R.id.showDialog7:
                showDialogSeven();
                break;
        }
    }

    private void showDialogOne() {
        NiceDialog.init()
                .setLayoutId(R.layout.layout_share)
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    public void convertView(ViewHolder holder, final BaseNiceDialog dialog) {
                        holder.setOnClickListener(R.id.wechat, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showToastMsg("分享成功");
                            }
                        });
                    }
                })
                .setDimAmount(0.3f)
                .setShowBottom(true)
                .show(getSupportFragmentManager());
    }

    private void showDialogTwo() {
        NiceDialog.init()
                .setLayoutId(R.layout.layout_friend_set)
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    public void convertView(ViewHolder holder, final BaseNiceDialog dialog) {

                    }
                })
                .setShowBottom(true)
                .setHeight(310)
                .show(getSupportFragmentManager());
    }

    private void showDialogThree() {
        NiceDialog.init()
                .setLayoutId(R.layout.layout_commit)
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    public void convertView(ViewHolder holder, final BaseNiceDialog dialog) {
                        final EditText editText = holder.getView(R.id.edit_input);
                        editText.post(new Runnable() {
                            @Override
                            public void run() {
                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.showSoftInput(editText, 0);
                            }
                        });
                    }
                })
                .setShowBottom(true)
                .show(getSupportFragmentManager());
    }

    private void showDialogFour() {
        NiceDialog.init()
                .setLayoutId(R.layout.layout_ad)
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    public void convertView(ViewHolder holder, final BaseNiceDialog dialog) {
                        holder.setOnClickListener(R.id.close, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                    }
                })
                .setWidth(210)
                .setOutCancel(false)
                .setAnimStyle(R.style.EnterExitAnimation)
                .show(getSupportFragmentManager());
    }

    private void showDialogFive() {
        NiceDialog.init()
                .setLayoutId(R.layout.layout_loading)
                .setWidth(100)
                .setHeight(100)
                .setDimAmount(0)
                .show(getSupportFragmentManager());
    }

    private void showDialogSix() {
        ConfirmDialog.newInstance("1")
                .setMargin(60)
                .setOutCancel(false)
                .show(getSupportFragmentManager());
    }

    private void showDialogSeven() {
        ConfirmDialog.newInstance("2")
                .setMargin(60)
                .setOutCancel(false)
                .show(getSupportFragmentManager());
    }

    public static class ConfirmDialog extends BaseNiceDialog {
        private String type;

        public static ConfirmDialog newInstance(String type) {
            Bundle bundle = new Bundle();
            bundle.putString("type", type);
            ConfirmDialog dialog = new ConfirmDialog();
            dialog.setArguments(bundle);
            return dialog;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle bundle = getArguments();
            if (bundle == null) {
                return;
            }
            type = bundle.getString("type");
        }

        @Override
        public int intLayoutId() {
            return R.layout.layout_confirm;
        }

        @Override
        public void convertView(ViewHolder holder, final BaseNiceDialog dialog) {
            if ("1".equals(type)) {
                holder.setText(R.id.title, "提示");
                holder.setText(R.id.message, "您已支付成功！");
            } else if ("2".equals(type)) {
                holder.setText(R.id.title, "警告");
                holder.setText(R.id.message, "您的账号已被冻结！");
            }
            holder.setOnClickListener(R.id.cancel, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            holder.setOnClickListener(R.id.ok, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }
    }
}
