package com.xiaote.mydemo.testdemo.activity.test;

import android.view.View;

import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;

/**
 * @author Xiaote
 */
public class TestStateButtonActivity extends BaseActivity {

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_statebutton;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_title_state_button;
    }

    @Override
    public void setupView() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void addListener() {

    }

    @Override
    public void onClick(View v) {

    }
}
