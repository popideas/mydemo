package com.xiaote.mydemo.testdemo.adapter;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.entity.LevelItem0;
import com.xiaote.mydemo.testdemo.entity.LevelItem1;
import com.xiaote.mydemo.testdemo.entity.LevelItem2;

import java.util.List;

/**
 * @author Xiaote
 * @date 2018/7/2 11:51
 * @email xiao_t@hks360.com
 */
public class ExpandableAdapter extends BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder> {

    public static final int TYPE_LEVEL_0 = 0;
    public static final int TYPE_LEVEL_1 = 1;
    public static final int TYPE_LEVEL_2 = 2;

    public ExpandableAdapter(List<MultiItemEntity> data) {
        super(data);
        addItemType(TYPE_LEVEL_0, R.layout.item_expandable_lv0);
        addItemType(TYPE_LEVEL_1, R.layout.item_expandable_lv1);
        addItemType(TYPE_LEVEL_2, R.layout.item_expandable_lv2);
    }

    @Override
    protected void convert(final BaseViewHolder helper, MultiItemEntity item) {
        switch (helper.getItemViewType()) {
            case TYPE_LEVEL_0:
                final LevelItem0 levelItem0 = (LevelItem0) item;
                helper.setText(R.id.item_level0_tv, levelItem0.getName());
                helper.setImageResource(R.id.item_level0_iv, levelItem0.isExpanded() ?
                        R.drawable.list_icon_arrowup : R.drawable.list_icon_dropdown);
                break;
            case TYPE_LEVEL_1:
                final LevelItem1 levelItem1 = (LevelItem1) item;
                helper.setText(R.id.item_level1_tv, levelItem1.getName());
                helper.setImageResource(R.id.item_level1_iv, levelItem1.isExpanded() ?
                        R.drawable.outline_list_expand : R.drawable.outline_list_collapse);
                break;
            case TYPE_LEVEL_2:
                LevelItem2 levelItem2 = (LevelItem2) item;
                helper.setText(R.id.item_level2_code_tv, levelItem2.getCode());
                helper.setText(R.id.item_level2_name_tv, levelItem2.getName());
                break;
        }
    }
}
