package com.xiaote.mydemo.testdemo.fragment.test;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.hks360.library.recyclerview.divider.DividerListItemDecoration;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.adapter.ExpandableTextViewAdapter;
import com.xiaote.mydemo.testdemo.fragment.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Xiaote
 * @date 2018/7/2 16:04
 * @email xiao_t@hks360.com
 */
public class TestExpandableTextListFragment extends BaseFragment {
    private RecyclerView mExpandableTextViewRv;
    private ExpandableTextViewAdapter mAdapter;

    @Override
    public int getViewId() {
        return R.layout.fragment_test_expandable_textview2;
    }

    @Override
    public void setupView() {
        mExpandableTextViewRv = mRootView.findViewById(R.id.expandable_textview_rv);
    }

    @Override
    public void initData() {
        mExpandableTextViewRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        mExpandableTextViewRv.addItemDecoration(new DividerListItemDecoration(getActivity()));
        mAdapter = new ExpandableTextViewAdapter(createData());
        mExpandableTextViewRv.setAdapter(mAdapter);
    }

    private List<String> createData() {
        List<String> list = new ArrayList<>();
        String[] sampleArr = getResources().getStringArray(R.array.expandable_text_samples);
        for (int i = 0; i < sampleArr.length; i++) {
            list.add(sampleArr[i]);
        }
        return list;
    }

    @Override
    public void addListener() {

    }

    @Override
    public void onClick(View v) {

    }
}
