package com.xiaote.mydemo.testdemo.entity;

/**
 * @author Xiaote
 * @date 2019-01-23 09：36
 * @email xiao_t@hks360.com
 */
public class RequestPartsInfo {

    private String cid;
    private String pcode;
    private String pname;
    private String count;
    private String valid;
    private String classifycode;
    private String searchinfo;
    private String pym;
    private String cartypecode;
    private String hkscode;
    private String pageId;
    private String pageSize;
    private String isinventory;

    public RequestPartsInfo() {
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public String getClassifycode() {
        return classifycode;
    }

    public void setClassifycode(String classifycode) {
        this.classifycode = classifycode;
    }

    public String getSearchinfo() {
        return searchinfo;
    }

    public void setSearchinfo(String searchinfo) {
        this.searchinfo = searchinfo;
    }

    public String getPym() {
        return pym;
    }

    public void setPym(String pym) {
        this.pym = pym;
    }

    public String getCartypecode() {
        return cartypecode;
    }

    public void setCartypecode(String cartypecode) {
        this.cartypecode = cartypecode;
    }

    public String getHkscode() {
        return hkscode;
    }

    public void setHkscode(String hkscode) {
        this.hkscode = hkscode;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getIsinventory() {
        return isinventory;
    }

    public void setIsinventory(String isinventory) {
        this.isinventory = isinventory;
    }
}
