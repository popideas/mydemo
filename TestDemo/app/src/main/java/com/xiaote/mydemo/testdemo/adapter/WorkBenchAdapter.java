package com.xiaote.mydemo.testdemo.adapter;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.entity.DistanceLearningEntity;
import com.xiaote.mydemo.testdemo.entity.WorkBenchData;

import java.util.List;

/**
 * @author Xiaote
 * @date 2018-09-12 11：10
 * @email xiao_t@hks360.com
 */
public class WorkBenchAdapter extends BaseMultiItemQuickAdapter<WorkBenchData, BaseViewHolder> {

    public WorkBenchAdapter(List<WorkBenchData> data) {
        super(data);
        addItemType(DistanceLearningEntity.ITEM_HEADER, R.layout.item_distance_learning_header);
        addItemType(DistanceLearningEntity.ITEM_CONTENT, R.layout.item_workbench_content);
        addItemType(DistanceLearningEntity.ITEM_FOOTER, R.layout.item_distance_learning_footer);
    }

    @Override
    protected void convert(BaseViewHolder helper, WorkBenchData item) {
        switch (helper.getItemViewType()) {
            case DistanceLearningEntity.ITEM_HEADER:
                helper.setText(R.id.distance_learning_header_tv, item.getName());
                break;
            case DistanceLearningEntity.ITEM_CONTENT:
                helper.setImageResource(R.id.work_bench_img_iv, item.getImgResId());
                helper.setText(R.id.work_bench_text_tv, item.getName());
                helper.setGone(R.id.work_bench_right_line_view, item.isShowRightLine());
                helper.setGone(R.id.work_bench_bottom_line_view, item.isShowBottomLine());
                break;
            default:
                break;
        }
    }
}
