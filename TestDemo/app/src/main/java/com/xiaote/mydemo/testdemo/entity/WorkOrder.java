package com.xiaote.mydemo.testdemo.entity;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.hks360.library.util.CommonUtil;
import com.hks360.library.util.DateUtil;

/**
 * @author ：xiaote
 * @date ：2020-05-11 11：20
 * 邮箱   ：xiao_t@hks360.com
 */
public class WorkOrder {
    @SerializedName("rowid")
    private String rowId;
    private String gch;
    private String count;
    @SerializedName("vehicletype")
    private String vehicleType;
    @SerializedName("licenseno")
    private String licenseNo;
    @SerializedName("indate")
    private String inDate;
    @SerializedName("outdate")
    private String outDate;
    private String status;
    @SerializedName("carid")
    private String carId;
    private String total;
    @SerializedName("imagecount")
    private String imageCount;
    @SerializedName("payamount")
    private String payAmount;
    @SerializedName("paystyle")
    private String payStyle;
    @SerializedName("wxxmcount")
    private String wxxmCount;
    private String username;
    @SerializedName("truename")
    private String trueName;
    @SerializedName("settlementtime")
    private String settlementTime;

    public String getRowId() {
        return rowId == null ? "" : rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getGch() {
        return gch == null ? "" : gch;
    }

    public void setGch(String gch) {
        this.gch = gch;
    }

    public String getCount() {
        return count == null ? "" : count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getVehicleType() {
        return vehicleType == null ? "" : vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getLicenseNo() {
        return licenseNo == null ? "" : licenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }

    public String getInDate() {
        return inDate == null ? "" : inDate;
    }

    public void setInDate(String inDate) {
        this.inDate = inDate;
    }

    public String getOutDate() {
        return outDate == null ? "" : outDate;
    }

    public void setOutDate(String outDate) {
        this.outDate = outDate;
    }

    public String getStatus() {
        return status == null ? "" : status;
    }

    public int getStatusIntValue() {
        if (TextUtils.equals("已付款", status)) {
            return 3;
        }
        if (TextUtils.equals("已结算", status)) {
            return 2;
        }
        return 1;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCarId() {
        return carId == null ? "" : carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getTotal() {
        return total == null ? "" : total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getImageCount() {
        return imageCount == null ? "" : imageCount;
    }

    public void setImageCount(String imageCount) {
        this.imageCount = imageCount;
    }

    public String getPayAmount() {
        return payAmount == null ? "" : payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    public String getPayStyle() {
        return payStyle == null ? "" : payStyle;
    }

    public void setPayStyle(String payStyle) {
        this.payStyle = payStyle;
    }

    public String getWxxmCount() {
        return wxxmCount == null ? "" : wxxmCount;
    }

    public void setWxxmCount(String wxxmCount) {
        this.wxxmCount = wxxmCount;
    }

    public String getUsername() {
        return username == null ? "" : username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTrueName() {
        return trueName == null ? "" : trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public String getSettlementTime() {
        return settlementTime == null ? "" : settlementTime;
    }

    public void setSettlementTime(String settlementTime) {
        this.settlementTime = settlementTime;
    }

    public String getNewInDate() {
        if (!CommonUtil.isEmpty(inDate) && inDate.contains(":")) {
            long time = DateUtil.parseStrToDateTime(inDate, "yyyy-MM-dd HH:mm");
            return time == 0 ? inDate : DateUtil.parseDate(time, "yyyy-MM-dd HH:mm");
        }
        return inDate;
    }

    public int getCountValue() {
        if (TextUtils.isEmpty(count)) {
            return 0;
        }
        return Integer.parseInt(count);
    }
}
