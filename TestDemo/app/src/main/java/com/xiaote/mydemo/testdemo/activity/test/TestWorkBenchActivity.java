package com.xiaote.mydemo.testdemo.activity.test;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.hks360.library.util.CommonUtil;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.adapter.WorkBenchAdapter;
import com.xiaote.mydemo.testdemo.entity.DistanceLearningEntity;
import com.xiaote.mydemo.testdemo.entity.WorkBenchData;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Xiaote
 * @date 2018-09-27 10：52
 * @email xiao_t@hks360.com
 */
public class TestWorkBenchActivity extends BaseActivity {
    private RecyclerView mWorkBenchRv;
    private WorkBenchAdapter mAdapter;
    private List<WorkBenchData> mWorkBenchList;
    private GridLayoutManager mLayoutManager;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_workbench;
    }

    @Override
    public int getTitleStringId() {
        return R.string.workbench;
    }

    @Override
    public void setupView() {
        mWorkBenchRv = findViewById(R.id.work_bench_rv);
    }

    @Override
    public void initData() {
        mLayoutManager = new GridLayoutManager(this, 4);
        mWorkBenchRv.setLayoutManager(mLayoutManager);
        initWorkBenchList();
        mAdapter = new WorkBenchAdapter(mWorkBenchList);
        mWorkBenchRv.setAdapter(mAdapter);
    }

    private void initWorkBenchList() {
        mWorkBenchList = new ArrayList<>();
        /** 维修 */
        mWorkBenchList.add(new WorkBenchData("维修", WorkBenchData.ITEM_HEADER));
        //TODO 报表
        mWorkBenchList.add(new WorkBenchData(R.drawable.forms, "报表", WorkBenchData.ITEM_CONTENT,
                true, true));
        //TODO 套餐
        mWorkBenchList.add(new WorkBenchData(R.drawable.icon_set_meal, "套餐", WorkBenchData.ITEM_CONTENT,
                true, true));
        //TODO 维修预约
        mWorkBenchList.add(new WorkBenchData(R.drawable.order, "维修预约", WorkBenchData.ITEM_CONTENT,
                true, true));
        //TODO 车辆保险
        mWorkBenchList.add(new WorkBenchData(
                R.drawable.icon_insurance, "车辆保险", WorkBenchData.ITEM_CONTENT,
                false, true));
        //TODO 二手车
        mWorkBenchList.add(new WorkBenchData(
                R.drawable.secendhandcar, "二手车", WorkBenchData.ITEM_CONTENT,
                true, false));
        //TODO O2O服务
        mWorkBenchList.add(new WorkBenchData(
                R.drawable.list_icon_o2o_service, "O2O服务", WorkBenchData.ITEM_CONTENT,
                true, false));
        mWorkBenchList.add(new WorkBenchData(0, "", WorkBenchData.ITEM_FOOTER));
        /** 仓库 */
        mWorkBenchList.add(new WorkBenchData("仓库", WorkBenchData.ITEM_HEADER));
        //TODO 库存
        mWorkBenchList.add(new WorkBenchData(R.drawable.stock, "库存", WorkBenchData.ITEM_CONTENT,
                true, true));
        //TODO 入库
        mWorkBenchList.add(new WorkBenchData(R.drawable.putin, "入库", WorkBenchData.ITEM_CONTENT,
                true, true));
        //TODO 收货
        mWorkBenchList.add(new WorkBenchData(R.drawable.tackover, "收货", WorkBenchData.ITEM_CONTENT,
                true, true));
        //TODO 配件维护
        mWorkBenchList.add(new WorkBenchData(
                R.drawable.icon_parts_protect, "配件维护", WorkBenchData.ITEM_CONTENT,
                false, true));
        //TODO 汽配商城
        mWorkBenchList.add(new WorkBenchData(R.drawable.mall, "汽配商城", WorkBenchData.ITEM_CONTENT,
                true, false));
        mWorkBenchList.add(new WorkBenchData(0, "", WorkBenchData.ITEM_FOOTER));
        /** 财务 */
        mWorkBenchList.add(new WorkBenchData("财务", WorkBenchData.ITEM_HEADER));
        //TODO 采购订单
        mWorkBenchList.add(new WorkBenchData(
                R.drawable.icon_purchasepayment, "采购订单", WorkBenchData.ITEM_CONTENT,
                true, false));
        //TODO 挂账
        mWorkBenchList.add(new WorkBenchData(
                R.drawable.icon_onaccount, "挂账", WorkBenchData.ITEM_CONTENT,
                true, false));
        //TODO 百凌白条
        mWorkBenchList.add(new WorkBenchData(
                R.drawable.list_icon_bailing, "百凌白条", WorkBenchData.ITEM_CONTENT,
                true, false));
        mWorkBenchList.add(new WorkBenchData(0, "", WorkBenchData.ITEM_FOOTER));
        /** 帮助中心 */
        mWorkBenchList.add(new WorkBenchData("帮助中心", WorkBenchData.ITEM_HEADER));
        //TODO 力矩查询
        mWorkBenchList.add(new WorkBenchData(
                R.drawable.icon_force, "力矩查询", WorkBenchData.ITEM_CONTENT,
                true, true));
        //TODO 远程教学
        mWorkBenchList.add(
                new WorkBenchData(R.drawable.icon_distance, "远程教学", WorkBenchData.ITEM_CONTENT,
                        true, true));
        //TODO 操作指南
        mWorkBenchList.add(
                new WorkBenchData(R.drawable.icon_use_skill, "操作指南", WorkBenchData.ITEM_CONTENT,
                        true, true));
        //TODO 违章代办
        mWorkBenchList.add(new WorkBenchData(
                R.drawable.icon_peccancy, "违章代办", WorkBenchData.ITEM_CONTENT,
                false, true));
        //TODO 在线客服
        mWorkBenchList.add(new WorkBenchData(R.drawable.help, "在线客服", WorkBenchData.ITEM_CONTENT,
                true, false));
    }

    @Override
    public void addListener() {
        mAdapter.setSpanSizeLookup(new MySpanSizeLookup());
        mAdapter.setOnItemClickListener(new MyOnItemClickListener());
    }

    @Override
    public void onClick(View v) {

    }

    private class MySpanSizeLookup implements BaseQuickAdapter.SpanSizeLookup {

        @Override
        public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
            WorkBenchData workBench = mWorkBenchList.get(position);
            return workBench.getItemType() == DistanceLearningEntity.ITEM_CONTENT ? 1 :
                    mLayoutManager.getSpanCount();
        }
    }

    private class MyOnItemClickListener implements BaseQuickAdapter.OnItemClickListener {

        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            int itemType = adapter.getItemViewType(position);
            switch (itemType) {
                case WorkBenchData.ITEM_CONTENT:
                    WorkBenchData workBench = (WorkBenchData) adapter.getItem(position);
                    if (!CommonUtil.isEmpty(workBench.getName())) {
                        showToastMsg("点击的内容是：" + workBench.getName());
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
