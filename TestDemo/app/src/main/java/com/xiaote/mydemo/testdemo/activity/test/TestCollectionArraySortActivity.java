package com.xiaote.mydemo.testdemo.activity.test;

import android.view.View;
import android.widget.TextView;

import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.entity.SortBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author ：xiaote
 * @date ：2020-05-12 11：17
 * 邮箱   ：xiao_t@hks360.com
 */
public class TestCollectionArraySortActivity extends BaseActivity {
    private TextView mTextCollectionSort;
    private TextView mTextArraySort;
    private TextView mTextArrayBeanSort;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_collection_array_sort;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_collections_or_array_sort;
    }

    @Override
    public void setupView() {
        mTextCollectionSort = findViewById(R.id.collection_sort_tv);
        mTextArraySort = findViewById(R.id.array_sort_tv);
        mTextArrayBeanSort = findViewById(R.id.array_bean_sort_tv);
    }

    @Override
    public void initData() {
        List<String> sourceList = createList();
        Collections.sort(sourceList);
        String collectionSortStr = "collections sort :" + createByList(sourceList);
        mTextCollectionSort.setText(collectionSortStr);
        String[] arr = new String[sourceList.size()];
        for (int i = 0, size = sourceList.size(); i < size; i++) {
            arr[i] = sourceList.get(i);
        }
        Arrays.sort(arr);
        String arraySortStr = "array sort :" + createByList(Arrays.asList(arr));
        mTextArraySort.setText(arraySortStr);
        List<SortBean> sortBeans = createSortBean();
        SortBean[] sortBeanArr = new SortBean[sortBeans.size()];
        for (int i = 0, size = sortBeans.size(); i < size; i++) {
            sortBeanArr[i] = sortBeans.get(i);
        }
        Arrays.sort(sortBeanArr, new Comparator<SortBean>() {
            @Override
            public int compare(SortBean sortBean1, SortBean sortBean2) {
                double area1 = sortBean1.getX() * sortBean1.getY();
                double area2 = sortBean2.getX() * sortBean2.getY();
                return Double.compare(area1, area2);
            }
        });
        String sortBeanStr = "array bean sort:" + getAreaStr(Arrays.asList(sortBeanArr));
        mTextArrayBeanSort.setText(sortBeanStr);
    }

    @Override
    public void addListener() {

    }

    @Override
    public void onClick(View view) {

    }

    private List<String> createList() {
        List<String> list = new ArrayList<>();
        list.add("Q");
        list.add("W");
        list.add("E");
        list.add("R");
        list.add("T");
        list.add("Y");
        list.add("U");
        list.add("I");
        list.add("O");
        list.add("P");
        list.add("A");
        list.add("S");
        list.add("D");
        list.add("E");
        list.add("G");
        list.add("H");
        list.add("J");
        list.add("K");
        list.add("L");
        list.add("Z");
        list.add("X");
        list.add("C");
        list.add("B");
        list.add("N");
        list.add("M");
        return list;
    }

    private String createByList(List<String> list) {
        StringBuilder builder = new StringBuilder();
        for (String s : list) {
            builder.append(s);
        }
        return builder.toString();
    }

    private List<SortBean> createSortBean() {
        List<SortBean> sortBeans = new ArrayList<>();
        sortBeans.add(new SortBean(2, 8));
        sortBeans.add(new SortBean(1.5, 2.4));
        sortBeans.add(new SortBean(1.2, 2.1));
        sortBeans.add(new SortBean(1.8, 2.6));
        sortBeans.add(new SortBean(2, 0.5));
        return sortBeans;
    }

    private String getAreaStr(List<SortBean> sortBeans) {
        StringBuilder builder = new StringBuilder();
        for (SortBean sortBean : sortBeans) {
            builder.append("x=").append(sortBean.getX()).append(",");
            builder.append("y=").append(sortBean.getY()).append(",");
            builder.append("areaValue:=").append(sortBean.getArea()).append("\n");
        }
        return builder.toString();
    }
}
