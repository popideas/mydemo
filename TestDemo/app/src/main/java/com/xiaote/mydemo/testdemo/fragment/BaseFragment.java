package com.xiaote.mydemo.testdemo.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hks360.library.util.ToastUtil;

/**
 * @author Xiaote
 * @date 2018/6/29 14:57
 * @email xiao_t@hks360.com
 */
public abstract class BaseFragment extends Fragment implements View.OnClickListener {
    public View mRootView;

    public abstract int getViewId();

    public abstract void setupView();

    public abstract void initData();

    public abstract void addListener();

    private void initView() {
        setupView();
        initData();
        addListener();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(getViewId(), null);
        initView();
        return mRootView;
    }

    public void showToastMsg(String message) {
        ToastUtil.showToast(getActivity(), message);
    }
}
