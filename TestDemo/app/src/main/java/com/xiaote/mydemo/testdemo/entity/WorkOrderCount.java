package com.xiaote.mydemo.testdemo.entity;

/**
 * @author ：xiaote
 * @date ：2020-05-15 10：09
 * 邮箱   ：xiao_t@hks360.com
 */
public class WorkOrderCount {

    /**
     * repairCount : 749
     * settlementCount : 100
     * paymentCount : 85
     */

    private String repairCount;
    private String settlementCount;
    private String paymentCount;

    public String getRepairCount() {
        return repairCount;
    }

    public void setRepairCount(String repairCount) {
        this.repairCount = repairCount;
    }

    public String getSettlementCount() {
        return settlementCount;
    }

    public void setSettlementCount(String settlementCount) {
        this.settlementCount = settlementCount;
    }

    public String getPaymentCount() {
        return paymentCount;
    }

    public void setPaymentCount(String paymentCount) {
        this.paymentCount = paymentCount;
    }
}
