package com.xiaote.mydemo.testdemo.activity.test;

import android.app.Activity;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.reflect.TypeToken;
import com.hks360.library.jsonformat.util.GsonUtils;
import com.hks360.library.pullrefreshlayout.PullRefreshLayout;
import com.hks360.library.pullrefreshlayout.listener.OnPullRefreshListener;
import com.hks360.library.pullrefreshlayout.widget.ClassicsHeader;
import com.hks360.library.recyclerview.divider.MyDividerItemDecoration;
import com.hks360.library.recyclerview.swipe.SwipeMenu;
import com.hks360.library.recyclerview.swipe.SwipeMenuBridge;
import com.hks360.library.recyclerview.swipe.SwipeMenuCreator;
import com.hks360.library.recyclerview.swipe.SwipeMenuItemClickListener;
import com.hks360.library.recyclerview.swipe.SwipeMenuRecyclerView;
import com.hks360.library.recyclerview.util.SwipeMenuCreatorUtil;
import com.hks360.library.rxeasyhttp.EasyHttp;
import com.hks360.library.rxeasyhttp.callback.ProgressDialogCallBack;
import com.hks360.library.rxeasyhttp.callback.SimpleCallBack;
import com.hks360.library.rxeasyhttp.exception.ApiException;
import com.hks360.library.util.CommonUtil;
import com.hks360.library.util.ErrorHelper;
import com.hks360.library.util.UIUtil;
import com.hks360.library.widget.LoadingDialog;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.adapter.WorkOrderAdapter;
import com.xiaote.mydemo.testdemo.entity.WorkOrder;
import com.xiaote.mydemo.testdemo.entity.WorkOrderCount;
import com.xiaote.mydemo.testdemo.web.Api;
import com.xiaote.mydemo.testdemo.web.WebSite;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：xiaote
 * @date ：2020-05-11 11：41
 * 邮箱   ：xiao_t@hks360.com
 */
public class TestWorkOrderActivity extends BaseActivity {
    private TabLayout mWorkOrderTabLayout;
    private PullRefreshLayout mWorkOrderRefreshLayout;
    private SwipeMenuRecyclerView mWorkOrderRv;

    private WorkOrderAdapter mAdapter;
    private int mPage = 0;
    private int mCount = 0;
    private String mWorkOrderStatus = "0";

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_work_order;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_work_order_list;
    }

    @Override
    public void setupView() {
        mWorkOrderTabLayout = findViewById(R.id.work_order_tab_layout);
        mWorkOrderRefreshLayout = findViewById(R.id.work_order_refresh_layout);
        mWorkOrderRv = findViewById(R.id.work_order_rv);
    }

    private void initTab() {
        mWorkOrderTabLayout.addTab(mWorkOrderTabLayout.newTab().setText(getString(R.string.work_order_repairing, "0")));
        mWorkOrderTabLayout.addTab(mWorkOrderTabLayout.newTab().setText(getString(R.string.work_order_settlement, "0")));
        mWorkOrderTabLayout.addTab(mWorkOrderTabLayout.newTab().setText(getString(R.string.work_order_finished, "0")));
    }

    private void initRefreshLayout() {
        mWorkOrderRefreshLayout.setRefreshEnable(true);
        mWorkOrderRefreshLayout.setLoadMoreEnable(false);
        mWorkOrderRefreshLayout.setHeaderView(new ClassicsHeader(this));
    }

    private void initRecyclerAdapter() {
        mWorkOrderRv.setLayoutManager(new LinearLayoutManager(this));
        mWorkOrderRv.setSwipeMenuCreator(new MyOnSwipeMenuCreator(this));
        mWorkOrderRv.setSwipeMenuItemClickListener(new MyOnSwipeMenuItemClickListener());
        int divider = (getResources().getDimensionPixelOffset(R.dimen.dp_10));
        mWorkOrderRv.addItemDecoration(new MyDividerItemDecoration(divider, 0, divider, divider));
        mAdapter = new WorkOrderAdapter();
        mWorkOrderRv.setAdapter(mAdapter);

    }

    @Override
    public void initData() {
        mPage = 0;
        mWorkOrderStatus = "0";
        initTab();
        initRefreshLayout();
        initRecyclerAdapter();
        getSearchGchCount();
    }

    @Override
    public void addListener() {
        mWorkOrderTabLayout.addOnTabSelectedListener(new MyOnTabSelectedListener());
        mWorkOrderRefreshLayout.setOnRefreshListener(new MyOnPullRefreshListener());
        mAdapter.setOnLoadMoreListener(new MyRequestLoadMoreListener(), mWorkOrderRv);
    }

    @Override
    public void onClick(View view) {

    }

    private class MyOnTabSelectedListener implements TabLayout.OnTabSelectedListener {

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            switch (tab.getPosition()) {
                case 0:
                    mWorkOrderStatus = "0";
                    break;
                case 1:
                    mWorkOrderStatus = "2";
                    break;
                case 2:
                    mWorkOrderStatus = "3";
                    break;
                default:
                    break;
            }
            mWorkOrderRefreshLayout.autoRefresh();
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    }

    private class MyOnSwipeMenuCreator implements SwipeMenuCreator {
        private Activity activity;

        public MyOnSwipeMenuCreator(Activity activity) {
            this.activity = activity;
        }

        @Override
        public void onCreateMenu(SwipeMenu swipeLeftMenu, SwipeMenu swipeRightMenu, int viewType) {
            swipeRightMenu.addMenuItem(SwipeMenuCreatorUtil.createSwipeMenuItem(activity, R.color.swipemenu_yellow,
                    R.string.add_remind, 16, R.color.white, UIUtil.dp2px(85),
                    ViewGroup.LayoutParams.MATCH_PARENT));
        }
    }

    private class MyOnPullRefreshListener extends OnPullRefreshListener {

        @Override
        public void onRefresh() {
            mPage = 0;
            getDataList(true);
        }
    }

    private class MyOnSwipeMenuItemClickListener implements SwipeMenuItemClickListener {

        @Override
        public void onItemClick(SwipeMenuBridge menuBridge) {
            menuBridge.closeMenu();
            int adapterPosition = menuBridge.getAdapterPosition();
            showToastMsg("当前点击位置为:" + adapterPosition);
        }
    }

    private class MyRequestLoadMoreListener implements BaseQuickAdapter.RequestLoadMoreListener {

        @Override
        public void onLoadMoreRequested() {
            mPage++;
            getDataList(false);
        }
    }

    private void refreshData() {
        mWorkOrderRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mWorkOrderRefreshLayout.autoRefresh();
            }
        }, 150);
    }

    //TODO 接口调用在这里

    /**
     * 获取工单数量
     */
    private void getSearchGchCount() {
        Map<String, String> params = new HashMap<>();
        params.put("cid", "88888");
        params.put("username", "");
        EasyHttp.post(Api.SEARCH_GCH_COUNT)
                .baseUrl(WebSite.BASE_STORE_URL_DEBUG)
                .params("data", GsonUtils.toJson(params))
                .accessToken(false)
                .execute(new ProgressDialogCallBack<String>(
                        new LoadingDialog(this), true, false) {

                    @Override
                    public void onError(ApiException e) {
                        super.onError(e);
                        ErrorHelper.onError(TestWorkOrderActivity.this, e);
                    }

                    @Override
                    public void onSuccess(String s) {
                        List<WorkOrderCount> list = GsonUtils.fromJson(s,
                                new TypeToken<List<WorkOrderCount>>() {
                                }.getType());
                        if (!CommonUtil.isEmpty(list)) {
                            WorkOrderCount workOrderCount = list.get(0);
                            TabLayout.Tab tab0 = mWorkOrderTabLayout.getTabAt(0);
                            if (tab0 != null) {
                                tab0.setText(getString(R.string.work_order_repairing, workOrderCount.getRepairCount()));
                            }
                            TabLayout.Tab tab1 = mWorkOrderTabLayout.getTabAt(1);
                            if (tab1 != null) {
                                tab1.setText(getString(R.string.work_order_settlement, workOrderCount.getSettlementCount()));
                            }
                            TabLayout.Tab tab2 = mWorkOrderTabLayout.getTabAt(2);
                            if (tab2 != null) {
                                tab2.setText(getString(R.string.work_order_finished, workOrderCount.getPaymentCount()));
                            }
                            refreshData();
                        }
                    }
                });
    }

    /**
     * 获取工单列表
     */
    private void getDataList(final boolean isRefresh) {
        Map<String, String> params = new HashMap<>();
        params.put("cid", "88888");
        params.put("status", mWorkOrderStatus);
        params.put("username", "");
        params.put("page", String.valueOf(mPage));
        EasyHttp.post(Api.SEARCH_GCH)
                .baseUrl(WebSite.BASE_STORE_URL_DEBUG)
                .params("data", GsonUtils.toJson(params))
                .accessToken(false)
                .execute(new SimpleCallBack<String>() {
                    @Override
                    public void onError(ApiException e) {
                        ErrorHelper.onError(TestWorkOrderActivity.this, e);
                        mWorkOrderRefreshLayout.refreshComplete();
                        mAdapter.loadMoreComplete();
                    }

                    @Override
                    public void onSuccess(String s) {
                        mWorkOrderRefreshLayout.refreshComplete();
                        mAdapter.loadMoreComplete();
                        List<WorkOrder> workOrderList = GsonUtils.fromJson(s,
                                new TypeToken<List<WorkOrder>>() {
                                }.getType());
                        if (isRefresh) {
                            mAdapter.setNewData(workOrderList);
                            if (!CommonUtil.isEmpty(workOrderList)) {
                                mCount = workOrderList.get(0).getCountValue();
                            }
                            switch (mWorkOrderTabLayout.getSelectedTabPosition()) {
                                case 0:
                                    TabLayout.Tab tab0 = mWorkOrderTabLayout.getTabAt(0);
                                    if (tab0 != null) {
                                        tab0.setText(getString(R.string.work_order_repairing, String.valueOf(mCount)));
                                    }
                                    break;
                                case 1:
                                    TabLayout.Tab tab1 = mWorkOrderTabLayout.getTabAt(1);
                                    if (tab1 != null) {
                                        tab1.setText(getString(R.string.work_order_settlement, String.valueOf(mCount)));
                                    }
                                    break;
                                case 2:
                                    TabLayout.Tab tab2 = mWorkOrderTabLayout.getTabAt(2);
                                    if (tab2 != null) {
                                        tab2.setText(getString(R.string.work_order_finished, String.valueOf(mCount)));
                                    }
                                    break;
                                default:
                                    break;
                            }
                        } else {
                            mAdapter.addData(workOrderList);
                        }
                        if (mAdapter.getData().size() >= mCount) {
                            mAdapter.loadMoreEnd();
                        }
                    }
                });
    }
}
