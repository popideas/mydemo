package com.xiaote.mydemo.testdemo.activity.test;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.hks360.library.jsonformat.util.GsonUtils;
import com.hks360.library.util.CommonUtil;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.adapter.MainAdapter;
import com.xiaote.mydemo.testdemo.entity.StandardName;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Xiaote
 */
public class TestAutoCompleteActivity extends BaseActivity {
    private LinearLayout mSaveLayout;
    private TextView mCategoryTv;
    private EditText mNameEt;
    private List<StandardName.PartTerminology> mPartTerminologyList;
    private MainAdapter mAdapter;

    private PopupWindow mPopup;
    private RecyclerView mStandnameRv;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_autocomplete;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_title_autocomplete;
    }

    @Override
    public void setupView() {
        mSaveLayout = findViewById(R.id.service_save_layout);
        mCategoryTv = findViewById(R.id.classification_select_tv);
        mNameEt = findViewById(R.id.service_name_et);
        initPopupWindow();
    }

    private void initPopupWindow() {
        View view = LayoutInflater.from(this).inflate(R.layout.popup_standname,null);
        mStandnameRv = view.findViewById(R.id.standardname_rv);
        mStandnameRv.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new MainAdapter(null);
        mStandnameRv.setAdapter(mAdapter);
        mPopup = new PopupWindow(this);
        mPopup.setBackgroundDrawable(null);
        mPopup.setOutsideTouchable(true);
        mPopup.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        mPopup.setContentView(view);
    }

    @Override
    public void initData() {
        mStandnameRv.setLayoutManager(new LinearLayoutManager(this));
        initList();
    }

    private void initList() {
        String jsonStr = getJsonStr();
        StandardName standardName = GsonUtils.fromJson(jsonStr, StandardName.class);
        mPartTerminologyList = standardName.getPartTerminologyList();
    }

    @Override
    public void addListener() {
        mNameEt.addTextChangedListener(new MyOnTextChangedListener());
        mAdapter.setOnItemClickListener(new MyOnItemClickListener());
    }

    @Override
    public void onClick(View v) {

    }

    private String getJsonStr() {
        try {
            InputStream inputStream = getAssets().open("standardname.json");
            StringBuffer buffer = new StringBuffer();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
            reader.close();
            return buffer.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private class MyOnTextChangedListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!CommonUtil.isEmpty(s.toString())) {
                List<String> list = new ArrayList<>();
                for (int i = 0; i < mPartTerminologyList.size(); i++) {
                    StandardName.PartTerminology partTerminology = mPartTerminologyList.get(i);
                    if (partTerminology.getSearchName().contains(s.toString())) {
                        list.add(partTerminology.getPartTerminologyName());
                    }
                }
                mPopup.setWidth(mNameEt.getMeasuredWidth());
                mPopup.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
                mStandnameRv.getLayoutManager().scrollToPosition(0);
                mStandnameRv.setVisibility(CommonUtil.isEmpty(list) ? View.GONE : View.VISIBLE);
                mAdapter.setNewData(list);
                mPopup.showAsDropDown(mNameEt);
            } else {
                mPopup.dismiss();
                mAdapter.setNewData(null);
                mStandnameRv.setVisibility(View.VISIBLE);
            }
        }
    }

    private class MyOnItemClickListener implements BaseQuickAdapter.OnItemClickListener {

        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            String text = mAdapter.getItem(position);
            mNameEt.setText(text);
            mNameEt.setSelection(text.length());
            mPopup.dismiss();
        }
    }
}
