package com.xiaote.mydemo.testdemo.adapter;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.entity.MaintainProjectCategory;
import com.xiaote.mydemo.testdemo.entity.MaintainProjectItem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：xiaote
 * @date ：2019-08-08 14：21
 * 邮箱   ：xiao_t@hks360.com
 */
public class MaintainProjectAdapter extends BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder> {
    public static final int ITEM_CATEGORY = 1;
    public static final int ITEM_PROJECT = 2;

    public MaintainProjectAdapter() {
        super(null);
        addItemType(ITEM_CATEGORY, R.layout.item_maintain_project_category);
        addItemType(ITEM_PROJECT, R.layout.item_maintain_project);
    }

    public List<MaintainProjectItem> getSelectProjectList() {
        List<MaintainProjectItem> selectedList = new ArrayList<>();
        for (MultiItemEntity entity : mData) {
            if (entity.getItemType() == ITEM_PROJECT) {
                MaintainProjectItem projectItem = (MaintainProjectItem) entity;
                if (projectItem.isSelected()) {
                    selectedList.add(projectItem);
                }
            }
        }
        return selectedList;
    }

    @Override
    protected void convert(BaseViewHolder helper, MultiItemEntity item) {
        switch (item.getItemType()) {
            case ITEM_CATEGORY:
                MaintainProjectCategory category = (MaintainProjectCategory) item;
                helper.setText(R.id.project_category_data_tv, category.getContentValue());
                helper.setImageResource(R.id.project_category_arrow_iv, category.isExpanded() ?
                        R.drawable.list_icon_arrowup : R.drawable.list_icon_dropdown);
                helper.setGone(R.id.project_category_arrow_iv, category.getCount() != 0);
                break;
            case ITEM_PROJECT:
                MaintainProjectItem projectItem = (MaintainProjectItem) item;
                helper.setText(R.id.project_name_tv, projectItem.getProjectName());
                helper.setText(R.id.project_price_tv, mContext.getString(R.string.price_value,
                        projectItem.getPriceValue()));
                helper.setImageResource(R.id.project_select_iv, projectItem.isSelected() ?
                        R.drawable.list_btn_selected_yellow : R.drawable.list_btn_select_for_orange);
                break;
            default:
                break;
        }
    }
}
