package com.xiaote.mydemo.testdemo.adapter;

import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.entity.WorkOrder;

/**
 * @author ：xiaote
 * @date ：2020-05-11 11：34
 * 邮箱   ：xiao_t@hks360.com
 */
public class WorkOrderAdapter extends BaseQuickAdapter<WorkOrder, BaseViewHolder> {

    public WorkOrderAdapter() {
        super(R.layout.item_work_order);
    }

    @Override
    protected void convert(BaseViewHolder helper, WorkOrder item) {
        helper.setText(R.id.work_order_item_license_tv, TextUtils.isEmpty(item.getLicenseNo()) ?
                "无车牌" : item.getLicenseNo());
        helper.setText(R.id.work_order_item_status_tv, item.getStatus());
        switch (item.getStatusIntValue()) {
            case 1:
                helper.setTextColor(R.id.work_order_item_status_tv, ContextCompat.getColor(mContext, R.color.light_blue1));
                break;
            case 2:
                helper.setTextColor(R.id.work_order_item_status_tv, ContextCompat.getColor(mContext, R.color.yellow_color4));
                break;
            case 3:
                helper.setTextColor(R.id.work_order_item_status_tv, ContextCompat.getColor(mContext, R.color.gray_66));
                break;
            default:
                break;
        }
        helper.setText(R.id.work_order_item_vehicletype_tv, item.getVehicleType());
        helper.setGone(R.id.work_order_item_vehicletype_tv, !TextUtils.isEmpty(item.getVehicleType()));
        helper.setText(R.id.work_order_item_gch_tv, item.getGch());
        helper.setText(R.id.work_order_item_indate_tv, String.format("进厂时间：%s", item.getNewInDate()));
        helper.setText(R.id.work_order_item_truename_tv, item.getTrueName());
    }
}
