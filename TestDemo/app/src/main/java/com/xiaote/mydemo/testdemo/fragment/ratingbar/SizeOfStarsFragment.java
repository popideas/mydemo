package com.xiaote.mydemo.testdemo.fragment.ratingbar;

import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import com.hks360.library.widget.SimpleRatingBar;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.fragment.BaseFragment;

public class SizeOfStarsFragment extends BaseFragment {

    private SimpleRatingBar ratingBar1;
    private SimpleRatingBar ratingBar2;
    private SimpleRatingBar ratingBar3;
    private SimpleRatingBar ratingBar4;
    private SimpleRatingBar ratingBar5;

    @Override
    public int getViewId() {
        return R.layout.fragment_size_of_stars;
    }

    @Override
    public void setupView() {
        ratingBar1 = mRootView.findViewById(R.id.ratingBar1);
        ratingBar2 = mRootView.findViewById(R.id.ratingBar2);
        ratingBar3 = mRootView.findViewById(R.id.ratingBar3);
        ratingBar4 = mRootView.findViewById(R.id.ratingBar4);
        ratingBar5 = mRootView.findViewById(R.id.ratingBar5);
    }

    @Override
    public void initData() {
        setHasOptionsMenu(true);
    }

    @Override
    public void addListener() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        float limit = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics());
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_action_refresh:
                if (ratingBar1.getStarSize() <= limit) {
                    ratingBar1.setStarSize(ratingBar1.getStarSize() + 20);
                    ratingBar2.setStarSize(ratingBar2.getStarSize() + 20);
                    ratingBar3.setStarSize(ratingBar3.getStarSize() + 20);
                    ratingBar4.setStarSize(ratingBar4.getStarSize() + 20);
                    ratingBar5.setStarSize(ratingBar5.getStarSize() + 20);
                } else {
                    ratingBar1.setStarSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics()));
                    ratingBar2.setStarSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics()));
                    ratingBar3.setStarSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getResources().getDisplayMetrics()));
                    ratingBar4.setStarSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics()));
                    ratingBar5.setStarSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics()));
                }
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {

    }
}
