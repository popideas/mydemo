package com.xiaote.mydemo.testdemo.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.entity.FlycoBean;

import java.util.List;

/**
 * @author ：xiaote
 * @date ：2020-06-24 17：40
 * 邮箱   ：xiao_t@hks360.com
 */
public class FlycoAdapter extends BaseQuickAdapter<FlycoBean, BaseViewHolder> {
    public FlycoAdapter(@Nullable List<FlycoBean> data) {
        super(R.layout.item_one_text, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, FlycoBean item) {
        helper.setText(R.id.name_tv, item.getTitle());
    }
}
