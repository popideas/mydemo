package com.xiaote.mydemo.testdemo.entity.oss;

import com.google.gson.annotations.SerializedName;

public class AssumedRoleUserData {
    @SerializedName("Arn")
    private String arn;
    @SerializedName("AssumedRoleId")
    private String assumedRoleId;

    public String getArn() {
        return arn;
    }

    public void setArn(String arn) {
        this.arn = arn;
    }

    public String getAssumedRoleId() {
        return assumedRoleId;
    }

    public void setAssumedRoleId(String assumedRoleId) {
        this.assumedRoleId = assumedRoleId;
    }
}
