package com.xiaote.mydemo.testdemo.activity.webview;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.hks360.library.xlog.XLog;
import com.xiaote.mydemo.testdemo.R;

/**
 * @author ：xiaote
 * @date ：2019-03-26 09：24
 * 邮箱   ：xiao_t@hks360.com
 */
public class TestPreventingHotlinkingWebViewActivity extends BaseWebViewActivity {

    @Override
    public int getTitleStringId() {
        return R.string.preventing_hotlinking;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    @JavascriptInterface
    public void loadUrl() {
        //        String loadUrl = "https://hks-saas.oss-cn-qingdao.aliyuncs.com/" +
        //                CommonUtil.getUrlEncodeStr("测试预览") + ".html";
        //        Map<String, String> headersMap = new HashMap<>();
        //        headersMap.put("Referer", "http://www.kuaixiuge.com");
        //        webView.loadUrl(loadUrl, headersMap);
//        MyJsInterface myJsInterface = new MyJsInterface();
//        webView.addJavascriptInterface(myJsInterface, myJsInterface.getInterface());
//        String loadUrl = "http://121.42.248.165:9001/appPage/MessageHandler.html";
//        XLog.e("loadUrl=>" + loadUrl);
//        webView.loadUrl(loadUrl);
        String pdfUrl = "http://hks-techdocs.oss-cn-qingdao.aliyuncs.com/%E5%A4%A7%E4%BC%97.%E9%80%94%E5%B2%B3.%E9%80%94%E5%B2%B3.2019-2020.pdf";
//        String data = "<iframe src='http://docs.google.com/gview?embedded=true&url="+pdfUrl+"'"+" width='100%' height='100%' s	tyle='border: none;'></iframe>";
        webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" +pdfUrl);
    }

    @Override
    public void setWebViewClient() {
        webView.setWebViewClient(new MyWebViewClient());
    }


    private class MyWebViewClient extends BaseWebViewClient {

        @Override
        public boolean modifyOverrideUrlLoading(WebView view, String url) {
            return false;
        }
    }

    private class MyJsInterface {

        @JavascriptInterface
        public void GetAuthorizationList(String data) {
            XLog.e("111111" + data);
        }

        @JavascriptInterface
        public String getInterface() {
            return "android_js_interface";
        }
    }
}
