package com.xiaote.mydemo.testdemo.activity.test;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.fragment.test.TestPickerViewFragment;

/**
 * @author Xiaote
 * @date 2018/6/29 14:54
 * @email xiao_t@hks360.com
 */
public class PickerViewFragmentActivity extends BaseActivity {

    @Override
    public int getLayoutId() {
        return R.layout.activity_pickerview_fragment;
    }

    @Override
    public int getTitleStringId() {
        return 0;
    }

    @Override
    public void setupView() {

    }

    @Override
    public void initData() {
        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_activity_main, new TestPickerViewFragment());
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void addListener() {

    }

    @Override
    public void onClick(View v) {

    }
}
