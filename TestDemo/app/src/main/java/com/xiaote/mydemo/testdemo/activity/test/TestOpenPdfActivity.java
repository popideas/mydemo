package com.xiaote.mydemo.testdemo.activity.test;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;

import es.voghdev.pdfviewpager.library.RemotePDFViewPager;
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;
import es.voghdev.pdfviewpager.library.util.FileUtil;

/**
 * @author ：xiaote
 * @date ：2019-07-08 16：33
 * 邮箱   ：xiao_t@hks360.com
 */
public class TestOpenPdfActivity extends BaseActivity implements DownloadFile.Listener {
    private FrameLayout pdf_root;
    private ProgressBar pb_bar;
    private RemotePDFViewPager remotePDFViewPager;
    private String mUrl = "http://hks-techdocs.oss-cn-qingdao.aliyuncs.com/%E5%A4%A7%E4%BC%97.%E9%80%94%E5%B2%B3.%E9%80%94%E5%B2%B3.2019-2020.pdf";
    private PDFPagerAdapter adapter;
    private TextView pageTv;
    private int total;

    /*设置监听*/
    protected void setDownloadListener() {
        remotePDFViewPager = new RemotePDFViewPager(this, mUrl, this);
        remotePDFViewPager.setId(R.id.pdfViewPager);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_open_pdf;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_title_pdf;
    }

    @Override
    public void setupView() {
        pdf_root = findViewById(R.id.remote_pdf_root);
        pb_bar = findViewById(R.id.pb_bar);
        pageTv = findViewById(R.id.page_tv);
    }

    @Override
    public void initData() {
        setDownloadListener();
    }

    @Override
    public void addListener() {
        remotePDFViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pageTv.setText((position + 1) + "/" + total);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onSuccess(String url, String destinationPath) {
        pb_bar.setVisibility(View.GONE);
        adapter = new PDFPagerAdapter(this, FileUtil.extractFileNameFromURL(url));
        remotePDFViewPager.setAdapter(adapter);
        total = adapter.getCount();
        pageTv.setText(1 + "/" + total);
        updateLayout();
    }

    /*更新视图*/
    private void updateLayout() {
        pdf_root.removeAllViewsInLayout();
        pdf_root.addView(remotePDFViewPager, LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onFailure(Exception e) {
        pb_bar.setVisibility(View.GONE);
        showToastMsg("加载文档失败");
    }

    @Override
    public void onProgressUpdate(int progress, int total) {
        pb_bar.setProgress(progress);
    }


}
