package com.xiaote.mydemo.testdemo.activity.webview;

import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;

import com.xiaote.mydemo.testdemo.R;

/**
 * @author ：xiaote
 * @date ：2020-01-13 16：15
 * 邮箱   ：xiao_t@hks360.com
 */
public class TestDistanceTeachingActivity extends BaseWebViewActivity {

    @Override
    public int getTitleStringId() {
        return R.string.distance_teaching;
    }

    @Override
    public void loadUrl() {
        String loadUrl = "https://m.cctalk.com/inst/sp6ppd39";
        webView.loadUrl(loadUrl);
    }

    @Override
    public void setWebViewClient() {
        webView.setWebViewClient(new MyWebViewClient());
    }

    private class MyWebViewClient extends BaseWebViewClient {

        @Override
        public boolean modifyOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("wtloginmqq://ptlogin/qlogin")) {
                openQQ(url);
                return true;
            }
            return false;
        }

        private void openQQ(String url) {
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            } catch (Exception e) {
                showToastMsg("请安装最新版QQ");
            }
        }
    }

}
