package com.xiaote.mydemo.testdemo.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.chad.library.adapter.base.entity.AbstractExpandableItem;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.google.gson.annotations.SerializedName;
import com.hks360.library.util.CommonUtil;
import com.hks360.library.util.NumberUtil;
import com.xiaote.mydemo.testdemo.adapter.PartsAdapter;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * 配件基本信息
 */
public class PartsInfo extends AbstractExpandableItem implements Parcelable, MultiItemEntity {

    private String pcode;            //配件编号
    private String pname;            //配件名称
    private String origin;            //配件品牌
    private String vehicle;            //适用车型
    private String psize;            //规格型号
    private String remark;            //备注
    private String unit;            //单位
    private String inprice;            //进价
    private String count;                //数量
    private String amount;            //总价
    private String saleprice;        //售价
    private String cid;                //企业代码
    private String classifycode;    //配件分类编号
    private String pym;                //配件的简拼
    private double addNumber;            //添加的数量
    private int type;                //配件适配类型
    private String kwh;
    private String images;            //配件图片
    @SerializedName("OrderNum0")
    private String orderNum0;         //序号
    private boolean checked;

    public static final Creator<PartsInfo> CREATOR = new Creator<PartsInfo>() {

        @Override
        public PartsInfo createFromParcel(Parcel source) {
            return new PartsInfo(source);
        }

        @Override
        public PartsInfo[] newArray(int size) {
            return new PartsInfo[size];
        }
    };

    public PartsInfo() {

    }

    private PartsInfo(Parcel source) {
        this.pcode = source.readString();
        this.pname = source.readString();
        this.origin = source.readString();
        this.vehicle = source.readString();
        this.psize = source.readString();
        this.remark = source.readString();
        this.unit = source.readString();
        this.inprice = source.readString();
        this.count = source.readString();
        this.amount = source.readString();
        this.saleprice = source.readString();
        this.cid = source.readString();
        this.classifycode = source.readString();
        this.pym = source.readString();
        this.type = source.readInt();
        this.kwh = source.readString();
        this.images = source.readString();
        this.addNumber = source.readDouble();
        this.orderNum0 = source.readString();
    }

    public void toggle() {
        this.checked = !this.checked;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getPsize() {
        return psize;
    }

    public void setPsize(String psize) {
        this.psize = psize;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getInprice() {
        return inprice;
    }

    public void setInprice(String inprice) {
        this.inprice = inprice;
    }

    public String getCount() {
        return count;
    }

    public float getPartsInfoCount() {
        return CommonUtil.isEmpty(count) ? 0 : Float.parseFloat(count);
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSaleprice() {
        return saleprice;
    }

    public String getSalePriceValue() {
        return TextUtils.isEmpty(saleprice) ? "" : NumberUtil.formatMoneyValue(Double.parseDouble(saleprice));
    }

    public String getMoneyUnit() {
        return CommonUtil.isEmpty(saleprice) ? "" : "¥" + NumberUtil.formatMoneyValue(Double.parseDouble(saleprice)) + "/" + unit;
    }

    public String getCountUnit() {
        float count = getPartsInfoCount();
        int part1 = (int) count;
        if (count - part1 > 0) {
            return String.valueOf(count) + unit;
        } else {
            return String.valueOf(part1) + unit;
        }
    }

    public String getSalepriceStr() {
        DecimalFormat format = new DecimalFormat("0.00");
        return CommonUtil.isEmpty(saleprice) ? "" : format.format(Double.parseDouble(saleprice));
    }

    public String getInpriceStr() {
        DecimalFormat format = new DecimalFormat("0.00");
        return CommonUtil.isEmpty(inprice) ? "" : format.format(Double.parseDouble(inprice));
    }

    public double getSalePriceMoney() {
        return CommonUtil.isEmpty(saleprice) ? 0 : Double.parseDouble(saleprice);
    }

    public void setSaleprice(String saleprice) {
        this.saleprice = saleprice;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getClassifycode() {
        return classifycode;
    }

    public void setClassifycode(String classifycode) {
        this.classifycode = classifycode;
    }

    public String getPym() {
        return pym;
    }

    public void setPym(String pym) {
        this.pym = pym;
    }

    public String getKwh() {
        return kwh;
    }

    public void setKwh(String kwh) {
        this.kwh = kwh;
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    public double getAddNumber() {
        return addNumber;
    }

    public void setAddNumber(double addNumber) {
        this.addNumber = addNumber;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getInAmount() {
        return NumberUtil.formatMoneyValue(addNumber * Double.parseDouble(inprice));
    }

    public String getOutAmount() {
        return NumberUtil.formatMoneyValue(addNumber * Double.parseDouble(saleprice));
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getOrderNum0() {
        return orderNum0 == null ? "" : orderNum0;
    }

    public void setOrderNum0(String orderNum0) {
        this.orderNum0 = orderNum0;
    }

    public ArrayList<String> getPartsImages() {
        ArrayList<String> list = new ArrayList<String>();
        if (!CommonUtil.isEmpty(images)) {
            images = images.replace("+", "%20");
            String[] arr = images.split(",");
            for (int i = 0; i < arr.length; i++) {
                list.add(arr[i]);
            }
        }
        return list;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pcode);
        dest.writeString(pname);
        dest.writeString(origin);
        dest.writeString(vehicle);
        dest.writeString(psize);
        dest.writeString(remark);
        dest.writeString(unit);
        dest.writeString(inprice);
        dest.writeString(count);
        dest.writeString(amount);
        dest.writeString(saleprice);
        dest.writeString(cid);
        dest.writeString(classifycode);
        dest.writeString(pym);
        dest.writeInt(type);
        dest.writeString(kwh);
        dest.writeString(images);
        dest.writeDouble(addNumber);
        dest.writeString(orderNum0);
    }

    @Override
    public String toString() {
        return "PartsInfo{" +
                ", pcode='" + pcode + '\'' +
                ", pname='" + pname + '\'' +
                ", origin='" + origin + '\'' +
                ", vehicle='" + vehicle + '\'' +
                ", psize='" + psize + '\'' +
                ", remark='" + remark + '\'' +
                ", unit='" + unit + '\'' +
                ", inprice='" + inprice + '\'' +
                ", count='" + count + '\'' +
                ", amount='" + amount + '\'' +
                ", saleprice='" + saleprice + '\'' +
                ", cid='" + cid + '\'' +
                ", classifycode='" + classifycode + '\'' +
                ", pym='" + pym + '\'' +
                ", addNumber=" + addNumber +
                ", type=" + type +
                ", kwh='" + kwh + '\'' +
                ", images='" + images + '\'' +
                '}';
    }

    @Override
    public int getItemType() {
        return PartsAdapter.ITEM_PARTS;
    }

    @Override
    public int getLevel() {
        return PartsAdapter.ITEM_PARTS;
    }
}
