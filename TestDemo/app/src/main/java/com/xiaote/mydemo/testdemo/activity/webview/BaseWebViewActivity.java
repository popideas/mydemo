package com.xiaote.mydemo.testdemo.activity.webview;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.hks360.library.listener.CommonWebChromeClient;
import com.hks360.library.util.CommonUtil;
import com.hks360.library.xlog.XLog;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;

/**
 * @author ：xiaote
 * @date ：2019-03-26 09：16
 * 邮箱   ：xiao_t@hks360.com
 */
public abstract class BaseWebViewActivity extends BaseActivity {
    public WebView webView;
    public ProgressBar progressBar;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_webview;
    }

    @Override
    public void setupView() {
        webView = findViewById(R.id.test_webview);
        progressBar = findViewById(R.id.test_webview_pb);
    }

    private void initWebView() {
        WebSettings webSettings = webView.getSettings();
        webSettings.setSupportZoom(false);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        // 隐藏缩放按钮
        webSettings.setDisplayZoomControls(false);
        //设置WebView对h5页面的支持
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSettings.setDomStorageEnabled(true);
        webSettings.setAppCachePath(CommonUtil.getH5CacheDir(this));
        webSettings.setAllowFileAccess(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setNeedInitialFocus(true);
        webSettings.setLoadWithOverviewMode(true);
        // 隐藏滚动条
        webView.setHorizontalScrollBarEnabled(false);
        webView.setVerticalScrollBarEnabled(false);
    }

    @Override
    public void initData() {
        initWebView();
        loadUrl();
    }

    @Override
    public void addListener() {
        webView.setWebChromeClient(new CommonWebChromeClient(progressBar));
        setWebViewClient();
    }

    @Override
    public void onClick(View v) {

    }

    public abstract void loadUrl();

    public abstract void setWebViewClient();

    public void back(View v) {
        if (webView.canGoBack()) {
            webView.goBack();
            return;
        }
        super.back(v);
    }

    public abstract class BaseWebViewClient extends WebViewClient {
        public abstract boolean modifyOverrideUrlLoading(WebView view, String url);

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            XLog.e("url=>" + url);
            return modifyOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            handler.proceed();
        }

    }
}
