package com.xiaote.mydemo.testdemo.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.entity.PartsInfo;

/**
 * @author ：xiaote
 * @date ：2019-09-10 09：20
 * 邮箱   ：xiao_t@hks360.com
 */
public class PartsInfoAdapter extends BaseQuickAdapter<PartsInfo, BaseViewHolder> {
    public PartsInfoAdapter() {
        super(R.layout.item_parts_info);
    }

    @Override
    protected void convert(BaseViewHolder helper, PartsInfo item) {
        helper.setText(R.id.parts_name_tv, item.getPname());
        helper.setText(R.id.part_count_unit_tv, item.getCountUnit());
        helper.setText(R.id.parts_code_tv, item.getPcode());
        helper.setText(R.id.parts_price_tv, item.getMoneyUnit());
        switch (item.getType()) {
            case -1:
                helper.setBackgroundRes(R.id.parts_info_layout, R.color.gray_e4);
                break;
            case 0:
            default:
                helper.setBackgroundRes(R.id.parts_info_layout, R.color.white);
                break;
            case 1:
                helper.setBackgroundRes(R.id.parts_info_layout, R.color.yellow_color3);
                break;
        }
        helper.addOnClickListener(R.id.parts_code_tv);
    }
}
