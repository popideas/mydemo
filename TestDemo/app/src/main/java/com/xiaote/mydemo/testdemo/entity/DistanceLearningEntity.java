package com.xiaote.mydemo.testdemo.entity;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * @author Xiaote
 * @date 2018-09-12 13：28
 * @email xiao_t@hks360.com
 */
public class DistanceLearningEntity implements MultiItemEntity {
    public static final int ITEM_HEADER  = 1;
    public static final int ITEM_CONTENT = 2;
    public static final int ITEM_FOOTER = 3;

    private int type;
    private String name;
    private boolean show = true;

    public DistanceLearningEntity() {

    }

    public DistanceLearningEntity(int type, String name) {
        this.type = type;
        this.name = name;
    }

    public DistanceLearningEntity(int type, String name,boolean show) {
        this.type = type;
        this.name = name;
        this.show = show;
    }

    public void setItemType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    @Override
    public int getItemType() {
        return type;
    }
}
