package com.xiaote.mydemo.testdemo.activity.test;

import android.support.design.widget.TabLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.hks360.library.util.CommonUtil;
import com.hks360.library.xlog.XLog;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.adapter.DistanceLearningAdapter;
import com.xiaote.mydemo.testdemo.entity.DistanceLearningEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Xiaote
 * @date 2018-09-12 10：19
 * @email xiao_t@hks360.com
 */
public class TestDistanceLearningActivity extends BaseActivity {
    private static final String[] NAMES = {"等级认证课程", "技能提升课程"};

    private TabLayout tabLayout;
    private RecyclerView distanceLearningRv;
    private List<DistanceLearningEntity> distanceLearningList;
    private DistanceLearningAdapter mAdapter;
    private GridLayoutManager mLayoutManager;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_distance_learning;
    }

    @Override
    public int getTitleStringId() {
        return R.string.distance_teaching;
    }

    @Override
    public void setupView() {
        tabLayout = findViewById(R.id.distance_learning_tablayout);
        distanceLearningRv = findViewById(R.id.distance_learning_rv);
    }

    @Override
    public void initData() {
        headTitleBarDivider();
        initTab();
        distanceLearningList = new ArrayList<>();
        initLevelCertificationCourseData();
        mLayoutManager = new GridLayoutManager(this, 2);
        distanceLearningRv.setLayoutManager(mLayoutManager);
        mAdapter = new DistanceLearningAdapter(distanceLearningList);
        distanceLearningRv.setAdapter(mAdapter);
        XLog.e("spanCount->" + mLayoutManager.getSpanCount());
        mAdapter.setSpanSizeLookup(new BaseQuickAdapter.SpanSizeLookup() {
            @Override
            public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
                DistanceLearningEntity entity = distanceLearningList.get(position);
                return entity.getItemType() == DistanceLearningEntity.ITEM_CONTENT ? 1 : mLayoutManager.getSpanCount();
            }
        });
    }

    private void initTab() {
        for (int i = 0; i < NAMES.length; i++) {
            tabLayout.addTab(tabLayout.newTab().setText(NAMES[i]));
        }
    }

    @Override
    public void addListener() {
        tabLayout.addOnTabSelectedListener(new MyOnTabSelectedListener());
        mAdapter.setOnItemClickListener(new MyOnItemClickListener());
    }

    @Override
    public void onClick(View v) {

    }

    private void initLevelCertificationCourseData() {
        distanceLearningList.clear();
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_HEADER, "初级认证课程"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "基础知识"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "零件拆装与检查"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "保养检查"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "考核题库"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_FOOTER, ""));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_HEADER, "中级认证课程"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "单体检查拆装"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "总成检查拆装"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "考核题库"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_FOOTER, ""));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_HEADER, "高级认证课程"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "故障灯报警故障诊断排除"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "发动机系统故障诊断排除"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "底盘系统故障诊断排除"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "电气系统故障诊断排除"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "其他故障诊断排除"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "考核题库"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_FOOTER, "", false));
    }

    private void initSkillUpgradingCourseData() {
        distanceLearningList.clear();
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_HEADER, "保养机修"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "发动机"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "变速器"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "底盘"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "电系"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "空调"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_FOOTER, ""));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_HEADER, "美容"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "美容产品"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "操作技巧"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_FOOTER, ""));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_HEADER, "钣喷"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "钣金"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "喷漆"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_FOOTER, ""));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_HEADER, "流程及管理"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "单体检查拆装"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_CONTENT, "总成检查拆装"));
        distanceLearningList.add(new DistanceLearningEntity(DistanceLearningEntity.ITEM_FOOTER, "", false));
    }

    private class MyOnTabSelectedListener implements TabLayout.OnTabSelectedListener {

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            switch (tab.getPosition()) {
                case 0:
                    initLevelCertificationCourseData();
                    break;
                case 1:
                    initSkillUpgradingCourseData();
                    break;
            }
            mAdapter.setNewData(distanceLearningList);
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    }

    private class MyOnItemClickListener implements BaseQuickAdapter.OnItemClickListener {

        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            int itemType = adapter.getItemViewType(position);
            switch (itemType) {
                case DistanceLearningEntity.ITEM_CONTENT:
                    DistanceLearningEntity child = (DistanceLearningEntity) adapter.getItem(position);
                    if (!CommonUtil.isEmpty(child.getName())) {
                        showToastMsg("点击的内容是：" + child.getName());
                    }
                    break;
            }
        }
    }
}
