package com.xiaote.mydemo.testdemo.entity;

/**
 * @author ：xiaote
 * @date ：2020-06-24 17：40
 * 邮箱   ：xiao_t@hks360.com
 */
public class FlycoBean {
    private String title;
    private Class<?> activityClass;

    public FlycoBean() {
    }

    public FlycoBean(String title, Class<?> activityClass) {
        this.title = title;
        this.activityClass = activityClass;
    }

    public String getTitle() {
        return title == null ? "" : title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Class<?> getActivityClass() {
        return activityClass;
    }

    public void setActivityClass(Class<?> activityClass) {
        this.activityClass = activityClass;
    }
}
