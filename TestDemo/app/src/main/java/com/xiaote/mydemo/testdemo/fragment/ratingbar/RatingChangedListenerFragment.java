package com.xiaote.mydemo.testdemo.fragment.ratingbar;

import android.view.View;
import android.widget.TextView;

import com.hks360.library.widget.SimpleRatingBar;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.fragment.BaseFragment;

import java.util.Arrays;


public class RatingChangedListenerFragment extends BaseFragment implements SimpleRatingBar.OnRatingBarChangeListener, View.OnClickListener {

    private SimpleRatingBar ratingBar1;
    private SimpleRatingBar ratingBar2;
    private SimpleRatingBar ratingBar3;
    private SimpleRatingBar ratingBar4;
    private SimpleRatingBar ratingBar5;
    private TextView rating1;
    private TextView rating2;
    private TextView rating3;
    private TextView rating4;
    private TextView rating5;

    @Override
    public int getViewId() {
        return R.layout.fragment_rating_changed_listener;
    }

    @Override
    public void setupView() {
        ratingBar1 = mRootView.findViewById(R.id.ratingBar1);
        ratingBar2 = mRootView.findViewById(R.id.ratingBar2);
        ratingBar3 = mRootView.findViewById(R.id.ratingBar3);
        ratingBar4 = mRootView.findViewById(R.id.ratingBar4);
        ratingBar5 = mRootView.findViewById(R.id.ratingBar5);
        rating1 = mRootView.findViewById(R.id.rating1);
        rating2 = mRootView.findViewById(R.id.rating2);
        rating3 = mRootView.findViewById(R.id.rating3);
        rating4 = mRootView.findViewById(R.id.rating4);
        rating5 = mRootView.findViewById(R.id.rating5);

    }

    @Override
    public void initData() {
        setHasOptionsMenu(true);

        for (SimpleRatingBar srb : Arrays.asList(ratingBar1, ratingBar2, ratingBar3, ratingBar4, ratingBar5)) {
            srb.setOnRatingBarChangeListener(this);
            // srb.setOnClickListener(this);
        }
    }

    @Override
    public void addListener() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
        String text = String.format("%.2f", rating);
        if (simpleRatingBar == ratingBar1) {
            rating1.setText(text);
        } else if (simpleRatingBar == ratingBar2) {
            rating2.setText(text);
        } else if (simpleRatingBar == ratingBar3) {
            rating3.setText(text);
        } else if (simpleRatingBar == ratingBar4) {
            rating4.setText(text);
        } else {
            rating5.setText(text);
        }
    }

    @Override
    public void onClick(View view) {
        showToastMsg("Clicked!");
    }
}

