package com.xiaote.mydemo.testdemo.fragment.ratingbar;

import android.support.annotation.Dimension;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;

import com.hks360.library.widget.SimpleRatingBar;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.fragment.BaseFragment;

public class StarsSeparationFragment extends BaseFragment {

  private SimpleRatingBar ratingBar1;
  private  SimpleRatingBar ratingBar2;
  private  SimpleRatingBar ratingBar3;
  private  SimpleRatingBar ratingBar4;
  private  SimpleRatingBar ratingBar5;

  @Override
  public int getViewId() {
    return R.layout.fragment_stars_separation;
  }

  @Override
  public void setupView() {
    ratingBar1 = mRootView.findViewById(R.id.ratingBar1);
    ratingBar2 = mRootView.findViewById(R.id.ratingBar2);
    ratingBar3 = mRootView.findViewById(R.id.ratingBar3);
    ratingBar4 = mRootView.findViewById(R.id.ratingBar4);
    ratingBar5 = mRootView.findViewById(R.id.ratingBar5);
  }

  @Override
  public void initData() {

  }

  @Override
  public void addListener() {

  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    float limit = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, getResources().getDisplayMetrics());
    // Handle item selection
    switch (item.getItemId()) {
      case R.id.menu_action_refresh:
        if (ratingBar1.getStarsSeparation() <= limit) {
          ratingBar1.setStarsSeparation(ratingBar1.getStarsSeparation(Dimension.DP) + 4, Dimension.DP);
          ratingBar2.setStarsSeparation(ratingBar2.getStarsSeparation(Dimension.DP) + 4, Dimension.DP);
          ratingBar3.setStarsSeparation(ratingBar3.getStarsSeparation(Dimension.DP) + 4, Dimension.DP);
          ratingBar4.setStarsSeparation(ratingBar4.getStarsSeparation(Dimension.DP) + 4, Dimension.DP);
          ratingBar5.setStarsSeparation(ratingBar5.getStarsSeparation(Dimension.DP) + 4, Dimension.DP);
        } else {
          ratingBar1.setStarsSeparation(4, Dimension.DP);
          ratingBar2.setStarsSeparation(6, Dimension.DP);
          ratingBar3.setStarsSeparation(8, Dimension.DP);
          ratingBar4.setStarsSeparation(10, Dimension.DP);
          ratingBar5.setStarsSeparation(12, Dimension.DP);
        }
        return false;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public void onClick(View v) {

  }
}
