package com.xiaote.mydemo.testdemo.activity.test;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hks360.library.jsonformat.util.GsonUtils;
import com.hks360.library.mndialog.MProgressDialog;
import com.hks360.library.rxeasyhttp.EasyHttp;
import com.hks360.library.rxeasyhttp.callback.SimpleCallBack;
import com.hks360.library.rxeasyhttp.exception.ApiException;
import com.hks360.library.util.ErrorHelper;
import com.hks360.library.util.RetrofitUtil;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.entity.ApiData;
import com.xiaote.mydemo.testdemo.web.Api;
import com.xiaote.mydemo.testdemo.web.WebSite;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Xiaote
 * @date 2018-08-15 14：00
 * @email xiao_t@hks360.com
 */
public class TestRetrofitTokenActivity extends BaseActivity {
    private Button btnGet, btnPost;
    private TextView tvValue;
    private int mRequestTag;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_retrofit;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_title_retrofit_token;
    }

    @Override
    public void setupView() {
        btnGet = findViewById(R.id.btn_retrofit_by_token_get);
        btnPost = findViewById(R.id.btn_retrofit_by_token_post);
        tvValue = findViewById(R.id.tv_retrofit_value);
    }

    @Override
    public void initData() {

    }

    @Override
    public void addListener() {
        btnGet.setOnClickListener(this);
        btnPost.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_retrofit_by_token_get:
                get();
                break;
            case R.id.btn_retrofit_by_token_post:
                post();
                break;
            default:
                break;
        }
    }

    private void get() {
        MProgressDialog.showProgress(this, "加载中...");
        EasyHttp.get(Api.GET_ALL_SERVICES)
                .baseUrl(WebSite.WEB_API_BASE_URL_DEBUG)
                .params("cid", "1088")
                .params("gch", "10881808000081")
                .accessToken(true)
                .execute(new SimpleCallBack<String>() {
                    @Override
                    public void onError(ApiException e) {
                        MProgressDialog.dismissProgress();
                        ErrorHelper.onError(getBaseContext(), e);
                    }

                    @Override
                    public void onSuccess(String s) {
                        MProgressDialog.dismissProgress();
                        ApiData apiData = GsonUtils.fromJson(s, ApiData.class);
                        tvValue.setText(apiData.getRows().toString());
                    }
                });
    }


    private void post() {
        MProgressDialog.showProgress(this, "加载中...");
        JSONObject object = new JSONObject();
        try {
            object.put("cid","1088");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        EasyHttp.post(Api.GET_PACKAGE)
                .baseUrl(WebSite.WEB_API_BASE_URL_DEBUG)
                .requestBody(RetrofitUtil.getJSONRequestBody(object.toString()))
                .execute(new SimpleCallBack<String>() {
                    @Override
                    public void onError(ApiException e) {
                        MProgressDialog.dismissProgress();
                        ErrorHelper.onError(getBaseContext(), e);
                    }

                    @Override
                    public void onSuccess(String s) {
                        MProgressDialog.dismissProgress();
                        ApiData apiData = GsonUtils.fromJson(s, ApiData.class);
                        tvValue.setText(apiData.getRows().toString());
                    }
                });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
