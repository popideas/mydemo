package com.xiaote.mydemo.testdemo.activity.test;

import android.support.v7.widget.SwitchCompat;
import android.text.InputType;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;

/**
 * @author ：xiaote
 * @date ：2019-06-03 13：03
 * 邮箱   ：xiao_t@hks360.com
 */
public class TestPasswordShowHideActivity extends BaseActivity {
    private EditText mPasswordEt;
    private SwitchCompat mShowSwitch;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_password_show_hide;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_password_show_hide;
    }

    @Override
    public void setupView() {
        mPasswordEt = findViewById(R.id.password_et);
        mShowSwitch = findViewById(R.id.show_switch);
    }

    @Override
    public void initData() {
        mPasswordEt.setText("abc123");
        mShowSwitch.setChecked(false);
    }

    @Override
    public void addListener() {
        mShowSwitch.setOnCheckedChangeListener(new MyOnCheckChangedListener());
    }

    @Override
    public void onClick(View view) {

    }

    private class MyOnCheckChangedListener implements SwitchCompat.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
            if(isChecked) {
                mPasswordEt.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            } else {
                mPasswordEt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            }
        }
    }
}
