package com.xiaote.mydemo.testdemo.util;

import com.hks360.library.util.CommonUtil;
import com.hks360.library.util.MD5Util;
import com.xiaote.mydemo.testdemo.BuildConfig;

/**
 * @author Xiaote
 * @date 2018-09-05 10：32
 * @email xiao_t@hks360.com
 */
public class MyUtil {
    public static String getViolationAgency(String cid) {
        StringBuffer buffer = new StringBuffer();
        buffer.append(BuildConfig.VIOLATION_AGENCY_DEBUG_URL);
        buffer.append("?sign=").append(MD5Util.MD5LowerCase(cid + BuildConfig.VIOLATION_AGENCY_AESKEY));
        buffer.append("&userId=").append(cid);
        buffer.append("&pageFrom=HKS");
        return buffer.toString();
    }

    public static boolean isEmptyPartAttribute(String attributeValue) {
        if (CommonUtil.isEmpty(attributeValue) || "-".equals(attributeValue)) {
            return true;
        }
        return false;
    }
}
