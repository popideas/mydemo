package com.xiaote.mydemo.testdemo.activity.parts;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.google.gson.reflect.TypeToken;
import com.hks360.library.jsonformat.util.GsonUtils;
import com.hks360.library.mndialog.MProgressDialog;
import com.hks360.library.recyclerview.divider.DividerListItemDecoration;
import com.hks360.library.rxeasyhttp.EasyHttp;
import com.hks360.library.rxeasyhttp.callback.SimpleCallBack;
import com.hks360.library.rxeasyhttp.exception.ApiException;
import com.hks360.library.rxeasyhttp.model.ApiResult;
import com.hks360.library.util.CommonUtil;
import com.hks360.library.util.ErrorHelper;
import com.hks360.library.util.RetrofitUtil;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.adapter.PartsAdapter;
import com.xiaote.mydemo.testdemo.common.IntentKey;
import com.xiaote.mydemo.testdemo.entity.PartsCategory;
import com.xiaote.mydemo.testdemo.entity.PartsClassify;
import com.xiaote.mydemo.testdemo.entity.PartsInfo;
import com.xiaote.mydemo.testdemo.entity.RequestPartsInfo;
import com.xiaote.mydemo.testdemo.web.Api;
import com.xiaote.mydemo.testdemo.web.WebSite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：xiaote
 * @date ：2019-09-09 09：33
 * 邮箱   ：xiao_t@hks360.com
 */
public class TestPartsInfoActivity extends BaseActivity {
    private RecyclerView mPartsInfoRv;
    private PartsAdapter mPartsAdapter;
    private int mCurrentPosition;
    private String cid = "88888";
    private PartsClassify mCurrentPartsClassify;
    private PartsCategory mCurrentPartsCategory;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_partsinfo;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_parts_info;
    }

    @Override
    public void setupView() {
        mPartsInfoRv = findViewById(R.id.parts_info_rv);
    }

    @Override
    public void initData() {
        mPartsInfoRv.setLayoutManager(new LinearLayoutManager(this));
        mPartsInfoRv.addItemDecoration(new DividerListItemDecoration(this));
        mPartsAdapter = new PartsAdapter();
        mPartsInfoRv.setAdapter(mPartsAdapter);
        getPartsClassify();
    }

    @Override
    public void addListener() {
        mPartsAdapter.setOnItemClickListener(new MyOnItemClickListener());
        mPartsAdapter.setOnItemChildClickListener(new MyOnItemChildClickListener());
    }

    @Override
    public void onClick(View view) {

    }

    private class MyOnItemClickListener implements BaseMultiItemQuickAdapter.OnItemClickListener {

        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            mCurrentPosition = position;
            MultiItemEntity multiItemEntity = mPartsAdapter.getItem(position);
            switch (multiItemEntity.getItemType()) {
                case PartsAdapter.ITEM_CLASSIFY:
                    mCurrentPartsClassify = (PartsClassify) multiItemEntity;
                    if (mCurrentPartsClassify.isAll()) {
                        Intent intent = new Intent(TestPartsInfoActivity.this, PartsInfoAllActivity.class);
                        startActivity(intent);
                    } else {
                        if (mCurrentPartsClassify.isExpanded()) {
                            mPartsAdapter.collapse(position);
                        } else {
                            List<PartsCategory> partsCategories = mCurrentPartsClassify.getSubItems();
                            if (CommonUtil.isEmpty(partsCategories)) {
                                getPartsInfo(mCurrentPartsClassify.getFldm(), mCurrentPartsClassify.getLevel());
                            } else {
                                mPartsAdapter.expand(position);
                            }
                        }
                    }

                    break;
                case PartsAdapter.ITEM_CATEGORY:
                    mCurrentPartsCategory = (PartsCategory) multiItemEntity;
                    if (mCurrentPartsCategory.isCategory()) {
                        if (mCurrentPartsCategory.isExpanded()) {
                            mPartsAdapter.collapse(position);
                        } else {
                            if (mCurrentPartsCategory.getPartsClassify().getCount() > 0 &&
                                    CommonUtil.isEmpty(mCurrentPartsCategory.getSubItems())) {
                                getPartsInfo(mCurrentPartsCategory.getPartsClassify().getFldm(), mCurrentPartsCategory.getLevel());
                            } else {
                                mPartsAdapter.expand(position);
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private class MyOnItemChildClickListener implements BaseQuickAdapter.OnItemChildClickListener {

        @Override
        public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
            MultiItemEntity multiItemEntity = mPartsAdapter.getItem(position);
            switch (view.getId()) {
                case R.id.parts_info_code_tv:
                    if (multiItemEntity != null && multiItemEntity.getItemType() == PartsAdapter.ITEM_CATEGORY) {
                        PartsCategory partsCategory = (PartsCategory) multiItemEntity;
                        if (!partsCategory.isCategory()) {
                            showDetail(partsCategory.getPartsInfo());
                        }
                    }
                    break;
                case R.id.parts_code_tv:
                    if(multiItemEntity != null && multiItemEntity.getItemType() == PartsAdapter.ITEM_PARTS) {
                        PartsInfo partsInfo = (PartsInfo) multiItemEntity;
                        showDetail(partsInfo);
                    }
                    break;
            }

        }
    }

    private void showDetail(PartsInfo partsInfo) {
        Intent intent = new Intent();
        intent.setClass(this, PartsInfoDetailActivity.class);
        intent.putExtra(IntentKey.PARTS_INFO, partsInfo);
        startActivity(intent);
    }


    private void getPartsClassify() {
        MProgressDialog.showProgress(this);
        Map<String, String> params = new HashMap<>();
        params.put("cid", cid);
        params.put("isshowuse", "1");
        params.put("isshowfit", "1");
        EasyHttp.post(Api.GET_PARTS_CASSIFY)
                .baseUrl(WebSite.BASE_STORE_URL_DEBUG)
                .params("data", GsonUtils.toJson(params))
                .accessToken(false)
                .execute(new SimpleCallBack<String>() {
                    @Override
                    public void onError(ApiException e) {
                        mPartsAdapter.loadMoreComplete();
                        MProgressDialog.dismissProgress();
                        ErrorHelper.onError(TestPartsInfoActivity.this, e);
                    }

                    @Override
                    public void onSuccess(String s) {
                        mPartsAdapter.loadMoreComplete();
                        MProgressDialog.dismissProgress();
                        List<PartsClassify> classifyList = GsonUtils.fromJson(s,
                                new TypeToken<List<PartsClassify>>() {
                                }.getType());
                        parseClassify(classifyList);
                    }
                });
    }

    private void parseClassify(List<PartsClassify> classifyList) {
        List<MultiItemEntity> entities = new ArrayList<>();
        for (PartsClassify partsClassify : classifyList) {
            if (TextUtils.equals("0", partsClassify.getNflno())) {
                entities.add(partsClassify);
            }
        }
        for (MultiItemEntity entity : entities) {
            PartsClassify classify = (PartsClassify) entity;
            for (PartsClassify partsClassify : classifyList) {
                if (TextUtils.equals(classify.getFlno(), partsClassify.getNflno())) {
                    PartsCategory partsCategory = new PartsCategory();
                    partsCategory.setCategory(true);
                    partsCategory.setPartsClassify(partsClassify);
                    classify.addSubItem(partsCategory);
                }
            }
        }
        entities.add(createPartsClassifyAll());
        mPartsAdapter.setNewData(entities);
    }

    private PartsClassify createPartsClassifyAll() {
        PartsClassify partsClassify = new PartsClassify();
        partsClassify.setNflno("0");
        partsClassify.setFlno("all01");
        partsClassify.setFldm("isAll");
        partsClassify.setFlname("全部");
        return partsClassify;
    }

    private RequestPartsInfo createRequestPartsInfo(String fldm) {
        RequestPartsInfo partsInfo = new RequestPartsInfo();
        partsInfo.setCid("88888");
        partsInfo.setPcode("");
        partsInfo.setPname("");
        partsInfo.setCount("");
        partsInfo.setValid("1");
        partsInfo.setClassifycode(fldm);
        partsInfo.setSearchinfo("");
        partsInfo.setPym("");
        partsInfo.setCartypecode("");
        partsInfo.setHkscode("");
        String pageId = "";
        partsInfo.setPageId(pageId);
        if (!TextUtils.isEmpty(pageId)) {
            partsInfo.setPageSize("10");
        }
        return partsInfo;
    }

    private void getPartsInfo(final String fldm, final int level) {
        MProgressDialog.showProgress(this);
        RequestPartsInfo requestPartsInfo = createRequestPartsInfo(fldm);
        EasyHttp.post(Api.GET_PARTS_INFO)
                .baseUrl(WebSite.WEB_API_BASE_URL_DEBUG)
                .requestBody(RetrofitUtil.getJSONRequestBodyForBean(requestPartsInfo))
                .accessToken(true)
                .execute(new SimpleCallBack<String>() {
                    @Override
                    public void onError(ApiException e) {
                        mPartsAdapter.loadMoreComplete();
                        MProgressDialog.dismissProgress();
                        ErrorHelper.onError(TestPartsInfoActivity.this, e);
                    }

                    @Override
                    public void onSuccess(String s) {
                        mPartsAdapter.loadMoreComplete();
                        MProgressDialog.dismissProgress();
                        ApiResult<List<PartsInfo>> apiResult = GsonUtils.fromJson(s,
                                new TypeToken<ApiResult<List<PartsInfo>>>() {
                                }.getType());
                        if (apiResult.isSuccess()) {
                            List<PartsInfo> partsInfoList = apiResult.getRows();
                            if (!CommonUtil.isEmpty(partsInfoList)) {
                                parsePartsInfo(partsInfoList, level);
                            }
                        } else {
                            showToastMsg(apiResult.getMsg());
                        }
                    }

                });
    }

    private void parsePartsInfo(List<PartsInfo> partsInfoList, int level) {
        switch (level) {
            case PartsAdapter.ITEM_CLASSIFY:
                for (PartsInfo partsInfo : partsInfoList) {
                    PartsCategory partsCategory = new PartsCategory();
                    partsCategory.setCategory(false);
                    partsCategory.setPartsInfo(partsInfo);
                    mCurrentPartsClassify.addSubItem(partsCategory);
                }
                mPartsAdapter.expand(mCurrentPosition);
                break;
            case PartsAdapter.ITEM_CATEGORY:
                for (PartsInfo partsInfo : partsInfoList) {
                    mCurrentPartsCategory.addSubItem(partsInfo);
                }
                mPartsAdapter.expand(mCurrentPosition);
                break;
            default:
                break;
        }
    }

}
