package com.xiaote.mydemo.testdemo.activity.test;

import android.app.Activity;
import android.view.View;

import com.hks360.library.spinner.MaterialSpinner;
import com.hks360.library.util.ToastUtil;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;

/**
 * @author Xiaote
 * @date 2018/7/4 11:12
 * @email xiao_t@hks360.com
 */
public class TestMaterialSpinnerActivity extends BaseActivity {
    private static final String[] ANDROID_VERSIONS = {
            "Cupcake",
            "Donut",
            "Eclair",
            "Froyo",
            "Gingerbread",
            "Honeycomb",
            "Ice Cream Sandwich",
            "Jelly Bean",
            "KitKat",
            "Lollipop",
            "Marshmallow",
            "Nougat",
            "Oreo",
            "Android P"
    };

    private MaterialSpinner mSpinner;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_materialspinner;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_title_material_spinner;
    }

    @Override
    public void setupView() {
        mSpinner = findViewById(R.id.spinner);
    }

    @Override
    public void initData() {
        mSpinner.setItems(ANDROID_VERSIONS);
    }

    @Override
    public void addListener() {
        mSpinner.setOnItemSelectedListener(new SpinnerItemSelectedListener(this));
    }

    @Override
    public void onClick(View v) {

    }

    private class SpinnerItemSelectedListener implements MaterialSpinner.OnItemSelectedListener<String> {
        private Activity activity;

        public SpinnerItemSelectedListener(Activity activity) {
            this.activity = activity;
        }

        @Override
        public void onItemSelected(MaterialSpinner view, int position, String item) {
            ToastUtil.showToast(activity, "Clicked：" + item);
        }
    }
}
