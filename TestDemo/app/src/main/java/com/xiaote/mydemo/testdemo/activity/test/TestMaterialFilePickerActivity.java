package com.xiaote.mydemo.testdemo.activity.test;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hks360.library.filepicker.MaterialFilePicker;
import com.hks360.library.filepicker.ui.FilePickerActivity;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.common.RequestCodeKey;

/**
 * @author Xiaote
 * @date 2018/7/3 09:21
 * @email xiao_t@hks360.com
 */
public class TestMaterialFilePickerActivity extends BaseActivity {
    private Button btnOpenFilePicker;
    private TextView tvFilePath;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_materialfilepicker;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_title_material_file_picker;
    }

    @Override
    public void setupView() {
        btnOpenFilePicker = findViewById(R.id.filepicker_open_btn);
        tvFilePath = findViewById(R.id.file_path_tv);
    }

    @Override
    public void initData() {

    }

    @Override
    public void addListener() {
        btnOpenFilePicker.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filepicker_open_btn:
                openFilePicker();
                break;
        }
    }

    private void openFilePicker() {
        new MaterialFilePicker()
                .withActivity(this)
                .withRequestCode(RequestCodeKey.OPEN_FILE_PICKER)
                .withHiddenFiles(true)
                .withTitle("文件管理器")
                .start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RequestCodeKey.OPEN_FILE_PICKER:
                    String path = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
                    tvFilePath.setText("文件路径：" + path);
                    break;
            }
        }
    }
}
