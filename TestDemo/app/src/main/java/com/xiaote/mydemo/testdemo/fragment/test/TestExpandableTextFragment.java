package com.xiaote.mydemo.testdemo.fragment.test;

import android.view.View;
import android.widget.TextView;

import com.hks360.library.widget.ExpandableLayout;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.fragment.BaseFragment;

/**
 * @author Xiaote
 * @date 2018/7/2 16:04
 * @email xiao_t@hks360.com
 */
public class TestExpandableTextFragment extends BaseFragment {
    private TextView mExpandLayoutTitle1;
    private ExpandableLayout mExpandLayout1;
    private TextView mExpandLayoutTitle2;
    private ExpandableLayout mExpandLayout2;

    @Override
    public int getViewId() {
        return R.layout.fragment_test_expandable_textview1;
    }

    @Override
    public void setupView() {
        View sample1 = mRootView.findViewById(R.id.sample1);
        mExpandLayoutTitle1 = sample1.findViewById(R.id.expandable_text_title);
        mExpandLayout1 = sample1.findViewById(R.id.expand_text_layout);
        View sample2 = mRootView.findViewById(R.id.sample2);
        mExpandLayoutTitle2 = sample2.findViewById(R.id.expandable_text_title);
        mExpandLayout2 = sample2.findViewById(R.id.expand_text_layout);
    }

    @Override
    public void initData() {
        mExpandLayoutTitle1.setText("Sample1");
        mExpandLayout1.setText(getString(R.string.dummy_text1));
        mExpandLayoutTitle2.setText("Sample2");
        mExpandLayout2.setText(getString(R.string.dummy_text2));
    }

    @Override
    public void addListener() {

    }

    @Override
    public void onClick(View v) {

    }
}
