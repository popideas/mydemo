package com.xiaote.mydemo.testdemo.activity.parts;

import android.view.View;
import android.widget.TextView;

import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.common.IntentKey;
import com.xiaote.mydemo.testdemo.entity.PartsInfo;

/**
 * @author ：xiaote
 * @date ：2019-09-10 10：32
 * 邮箱   ：xiao_t@hks360.com
 */
public class PartsInfoDetailActivity extends BaseActivity {
    private TextView mPartsCodeTv;
    private TextView mPartsNameTv;
    private TextView mPartsSalePriceTv;
    private TextView mPartsCountTv;
    private TextView mPartsUnitTv;
    private TextView mPartsClassifyCodeTv;
    private TextView mPartsLocationTv;
    private TextView mPartsPlaceTv;
    private TextView mPartsSpecificationTv;
    private TextView mPartsSerialTv;
    private TextView mPartsIsEffectTv;
    private TextView mPartsVehicleTv;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_parts_info_detail;
    }

    @Override
    public int getTitleStringId() {
        return R.string.parts_info_detail;
    }

    @Override
    public void setupView() {
        mPartsCodeTv = findViewById(R.id.parts_detail_code_tv);
        mPartsNameTv = findViewById(R.id.parts_detail_name_tv);
        mPartsSalePriceTv = findViewById(R.id.parts_detail_saleprice_tv);
        mPartsCountTv = findViewById(R.id.parts_detail_count_tv);
        mPartsUnitTv = findViewById(R.id.parts_detail_unit_tv);
        mPartsClassifyCodeTv = findViewById(R.id.parts_detail_classifycode_tv);
        mPartsLocationTv = findViewById(R.id.parts_detail_warehouselocation_tv);
        mPartsPlaceTv = findViewById(R.id.parts_detail_place_tv);
        mPartsSpecificationTv = findViewById(R.id.parts_detail_specification_tv);
        mPartsSerialTv = findViewById(R.id.parts_detail_serial_tv);
        mPartsIsEffectTv = findViewById(R.id.parts_detail_iseffect_tv);
        mPartsVehicleTv = findViewById(R.id.parts_detail_vehicle_tv);
    }

    @Override
    public void initData() {
        PartsInfo partsInfo = getIntent().getParcelableExtra(IntentKey.PARTS_INFO);
        if(partsInfo != null) {
            mPartsCodeTv.setText(partsInfo.getPcode());
            mPartsNameTv.setText(partsInfo.getPname());
            mPartsSalePriceTv.setText(partsInfo.getSalePriceValue());
            mPartsCountTv.setText(partsInfo.getCount());
            mPartsUnitTv.setText(partsInfo.getUnit());
            mPartsClassifyCodeTv.setText(partsInfo.getClassifycode());
            mPartsLocationTv.setText(partsInfo.getKwh());
            mPartsPlaceTv.setText(partsInfo.getOrigin());
            mPartsSpecificationTv.setText(partsInfo.getPsize());
            mPartsSerialTv.setText(partsInfo.getOrderNum0());
            mPartsIsEffectTv.setText("是");
            mPartsVehicleTv.setText(partsInfo.getVehicle());
        }
    }

    @Override
    public void addListener() {

    }

    @Override
    public void onClick(View view) {

    }
}
