package com.xiaote.mydemo.testdemo.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.hks360.library.formlayout.view.SyncLinearLayout;
import com.xiaote.mydemo.testdemo.adapter.FormAdapter;
import com.xiaote.mydemo.testdemo.entity.FormBean;

import java.util.List;

/**
 * @author ：xiaote
 * @date ：2020-07-14 17：08
 * 邮箱   ：xiao_t@hks360.com
 */
public class FormSyncLinearLayout extends SyncLinearLayout {
    private FormAdapter mFormAdapter;

    public FormSyncLinearLayout(Context context) {
        this(context,null);
    }

    public FormSyncLinearLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FormSyncLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initFormAdapter();
    }

    private void initFormAdapter() {
        mFormAdapter = new FormAdapter(mColumnCount);
        setFormAdapter(mFormAdapter);
    }

    public void setNewData(List<String> leftTitleList, List<String> topTitleList,
                           List<FormBean> formList, boolean isLoadMoreEnd) {
        super.setNewData(leftTitleList, topTitleList);
        mFormAdapter.setNewData(formList);
        setLoadMoreEnd(isLoadMoreEnd);
    }

    public void addData(List<String> leftTitleList, List<FormBean> formList, boolean isLoadMoreEnd) {
        super.addData(leftTitleList);
        mFormAdapter.addData(formList);
        setLoadMoreEnd(isLoadMoreEnd);
    }

    public List<FormBean> getFormList() {
        return mFormAdapter.getData();
    }
}
