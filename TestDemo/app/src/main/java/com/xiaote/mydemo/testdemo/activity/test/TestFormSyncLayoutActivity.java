package com.xiaote.mydemo.testdemo.activity.test;

import android.view.View;

import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.entity.FormBean;
import com.xiaote.mydemo.testdemo.widget.FormSyncLinearLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：xiaote
 * @date ：2020-07-14 18：59
 * 邮箱   ：xiao_t@hks360.com
 */
public class TestFormSyncLayoutActivity extends BaseActivity {
    private FormSyncLinearLayout mFormSyncLayout;

    private int rowCount = 30;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_form_sync_layout;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_form_sync_layout;
    }

    @Override
    public void setupView() {
        mFormSyncLayout = findViewById(R.id.form_sync_layout);
    }

    private List<String> getLeftTitleList() {
        List<String> leftTitleList = new ArrayList<>();
        for (int i = 0; i < rowCount; i++) {
            leftTitleList.add("客户" + (i + 1));
        }
        return leftTitleList;
    }

    private List<String> getTopTitleList() {
        List<String> topTitleList = new ArrayList<>();
        topTitleList.add("销售额");
        topTitleList.add("销售单数");
        topTitleList.add("单均金额");
        topTitleList.add("毛利");
        return topTitleList;
    }

    private List<FormBean> getFormList() {
        List<FormBean> formList = new ArrayList<>();
        for (int i = 0; i < rowCount; i++) {
            formList.add(new FormBean("100000", "100", "1000", "10000"));
        }
        return formList;
    }

    @Override
    public void initData() {
        mFormSyncLayout.setLeftTitleText("客户名称");
        mFormSyncLayout.setNewData(getLeftTitleList(), getTopTitleList(), getFormList(), true);
    }

    @Override
    public void addListener() {

    }

    @Override
    public void onClick(View v) {

    }
}
