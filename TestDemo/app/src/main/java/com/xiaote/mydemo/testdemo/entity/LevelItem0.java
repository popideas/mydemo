package com.xiaote.mydemo.testdemo.entity;

import com.chad.library.adapter.base.entity.AbstractExpandableItem;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.xiaote.mydemo.testdemo.adapter.ExpandableAdapter;

/**
 * @author Xiaote
 * @date 2018/7/2 11:56
 * @email xiao_t@hks360.com
 */
public class LevelItem0 extends AbstractExpandableItem<LevelItem1> implements MultiItemEntity {
    private String name;

    public LevelItem0() {

    }

    public LevelItem0(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getLevel() {
        return 0;
    }

    @Override
    public int getItemType() {
        return ExpandableAdapter.TYPE_LEVEL_0;
    }
}
