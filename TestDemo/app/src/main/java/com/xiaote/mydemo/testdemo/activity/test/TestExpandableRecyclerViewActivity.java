package com.xiaote.mydemo.testdemo.activity.test;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.adapter.ExpandableAdapter;
import com.xiaote.mydemo.testdemo.entity.LevelItem0;
import com.xiaote.mydemo.testdemo.entity.LevelItem1;
import com.xiaote.mydemo.testdemo.entity.LevelItem2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Xiaote
 * @date 2018/7/2 11:46
 * @email xiao_t@hks360.com
 */
public class TestExpandableRecyclerViewActivity extends BaseActivity {

    private RecyclerView mRecyclerView;
    private ExpandableAdapter mAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_expandable_recyclerview;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_title_expandable_recyclerview;
    }

    @Override
    public void setupView() {
        mRecyclerView = findViewById(R.id.expandable_rv);
    }

    @Override
    public void initData() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new ExpandableAdapter(createData());
        mRecyclerView.setAdapter(mAdapter);
    }

    private List<MultiItemEntity> createData() {
        int level0Count = 9;
        int level1Count = 3;
        int level2Count = 5;
        List<MultiItemEntity> list = new ArrayList<>();
        for (int i = 0; i < level0Count; i++) {
            LevelItem0 levelItem0 = new LevelItem0("This Is Level0 Item：" + (i + 1));
            for (int j = 0; j < level1Count; j++) {
                LevelItem1 levelItem1 = new LevelItem1("This is Level1 Item：" + (j + 1));
                for (int k = 0; k < level2Count; k++) {
                    LevelItem2 levelItem2 = new LevelItem2("Code " + (k + 1), "Name " + (k + 1));
                    levelItem1.addSubItem(levelItem2);
                }
                levelItem0.addSubItem(levelItem1);
            }
            list.add(levelItem0);
        }
        return list;
    }

    @Override
    public void addListener() {
        mAdapter.expandAll(0, false);
        mAdapter.setOnItemClickListener(new MyOnItemClickListener());
    }

    @Override
    public void onClick(View v) {

    }

    private class MyOnItemClickListener implements BaseQuickAdapter.OnItemClickListener {

        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            int viewType = adapter.getItemViewType(position);
            switch (viewType) {
                case ExpandableAdapter.TYPE_LEVEL_0:
                    LevelItem0 levelItem0 = (LevelItem0) adapter.getItem(position);
                    if (levelItem0.isExpanded()) {
                        adapter.collapse(position);
                    } else {
                        adapter.expand(position);
                    }
                    break;
                case ExpandableAdapter.TYPE_LEVEL_1:
                    LevelItem1 levelItem1 = (LevelItem1) adapter.getItem(position);
                    if (levelItem1.isExpanded()) {
                        adapter.collapse(position);
                    } else {
                        adapter.expand(position);
                    }
                    break;
                case ExpandableAdapter.TYPE_LEVEL_2:
                    LevelItem2 levelItem2 = (LevelItem2) adapter.getItem(position);
                    showToastMsg("position=" + position + ",code =" + levelItem2.getCode()
                            + ",name =" + levelItem2.getName());
                    break;
            }
        }
    }
}