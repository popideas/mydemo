package com.xiaote.mydemo.testdemo.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 *  配件标准名称
 */
public class StandardName {
    @SerializedName("errcode")
    private int errCode;
    @SerializedName("errmsg")
    private String errMsg;
    @SerializedName("PartTerminology")
    private List<PartTerminology> partTerminologyList;

    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public List<PartTerminology> getPartTerminologyList() {
        return partTerminologyList;
    }

    public void setPartTerminologyList(List<PartTerminology> partTerminologyList) {
        this.partTerminologyList = partTerminologyList;
    }

    public static class PartTerminology {
        @SerializedName("PartTerminologyName")
        private String partTerminologyName;
        @SerializedName("PartTerminologyEnglish")
        private String partTerminologyEnglish;
        @SerializedName("SearchName")
        private String searchName;

        public String getPartTerminologyName() {
            return partTerminologyName;
        }

        public void setPartTerminologyName(String partTerminologyName) {
            this.partTerminologyName = partTerminologyName;
        }

        public String getPartTerminologyEnglish() {
            return partTerminologyEnglish;
        }

        public void setPartTerminologyEnglish(String partTerminologyEnglish) {
            this.partTerminologyEnglish = partTerminologyEnglish;
        }

        public String getSearchName() {
            return searchName;
        }

        public void setSearchName(String searchName) {
            this.searchName = searchName;
        }

    }
}
