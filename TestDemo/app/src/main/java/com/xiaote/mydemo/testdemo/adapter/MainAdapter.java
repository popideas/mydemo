package com.xiaote.mydemo.testdemo.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaote.mydemo.testdemo.R;

import java.util.List;

/**
 * @author Xiaote
 */
public class MainAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public MainAdapter(@Nullable List<String> data) {
        super(R.layout.item_one_text, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        int index = helper.getAdapterPosition() + 1;
        helper.setText(R.id.name_tv, mContext.getString(R.string.main_content, index, item));
    }
}
