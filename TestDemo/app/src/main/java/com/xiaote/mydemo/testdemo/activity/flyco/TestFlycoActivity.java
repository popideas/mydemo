package com.xiaote.mydemo.testdemo.activity.flyco;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.hks360.library.recyclerview.divider.DividerListItemDecoration;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.adapter.FlycoAdapter;
import com.xiaote.mydemo.testdemo.entity.FlycoBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：xiaote
 * @date ：2020-06-24 17：24
 * 邮箱   ：xiao_t@hks360.com
 */
public class TestFlycoActivity extends BaseActivity {
    private RecyclerView mFlycoRv;

    private FlycoAdapter mAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_flyco;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_flyco_tab_layout;
    }

    @Override
    public void setupView() {
        mFlycoRv = findViewById(R.id.flyco_rv);
    }

    @Override
    public void initData() {
        mFlycoRv.setLayoutManager(new LinearLayoutManager(this));
        mFlycoRv.addItemDecoration(new DividerListItemDecoration(this));
        List<FlycoBean> list = new ArrayList<>();
        list.add(new FlycoBean("SlidingTabLayout", SlidingTabActivity.class));
        list.add(new FlycoBean("CommonTabLayout", CommonTabActivity.class));
        list.add(new FlycoBean("SegmentTabLayout", SegmentTabActivity.class));
        mAdapter = new FlycoAdapter(list);
        mFlycoRv.setAdapter(mAdapter);
    }

    @Override
    public void addListener() {
        mAdapter.setOnItemClickListener(new MyOnItemClickListener(this));
    }

    @Override
    public void onClick(View v) {

    }

    private class MyOnItemClickListener implements BaseQuickAdapter.OnItemClickListener {
        private Activity activity;

        public MyOnItemClickListener(Activity activity) {
            this.activity = activity;
        }


        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            FlycoBean flycoBean = mAdapter.getItem(position);
            if (flycoBean != null) {
                startActivity(new Intent(activity, flycoBean.getActivityClass()));
            }
        }
    }
}
