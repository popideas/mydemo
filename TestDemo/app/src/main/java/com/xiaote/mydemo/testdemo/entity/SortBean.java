package com.xiaote.mydemo.testdemo.entity;

import java.text.DecimalFormat;

/**
 * @author ：xiaote
 * @date ：2020-05-12 11：38
 * 邮箱   ：xiao_t@hks360.com
 */
public class SortBean {
    private double x;
    private double y;

    public SortBean(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getArea() {
        DecimalFormat foramt = new DecimalFormat("0.00");
        return foramt.format(x * y);
    }
}
