package com.xiaote.mydemo.testdemo.activity.test;

import android.view.View;
import android.widget.Button;

import com.hks360.library.flexbox.interfaces.OnFlexboxSubscribeListener;
import com.hks360.library.flexbox.widget.TagFlowLayout;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.adapter.StringTagAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Xiaote
 * @date 2018-09-04 17：57
 * @email xiao_t@hks360.com
 */
public class TestFlexBoxActivity extends BaseActivity {
    private TagFlowLayout tagFlowLayout;
    private Button btnCount;

    private StringTagAdapter adapter;
    private List<String> sourceData;
    private List<String> selectItems;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_flexbox;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_title_flexbox;
    }

    @Override
    public void setupView() {
        btnCount = findViewById(R.id.btn_count);
        tagFlowLayout = findViewById(R.id.flow_layout);
    }

    private void initSourceData() {
        sourceData = new ArrayList<>();
        sourceData.add("程序员");
        sourceData.add("设计师");
        sourceData.add("产品经理");
        sourceData.add("运营");
        sourceData.add("商务");
        sourceData.add("人事经理");
        sourceData.add("项目经理");
        sourceData.add("客户代表");
        sourceData.add("技术主管");
        sourceData.add("测试工程师");
        sourceData.add("前端工程师");
        sourceData.add("Java工程师");
        sourceData.add("Android工程师");
        sourceData.add("iOS工程师");

        selectItems = new ArrayList<>();
        selectItems.add("客户代表");
        selectItems.add("Java工程师");
    }


    @Override
    public void initData() {
        initSourceData();
        adapter = new StringTagAdapter(this, sourceData, selectItems);
        tagFlowLayout.setAdapter(adapter);
        btnCount.setText("已选择" + adapter.getSelectedList().size() + "个");
    }

    @Override
    public void addListener() {
        adapter.setOnSubscribeListener(new MyOnFlexboxSubscribeListener());
    }

    @Override
    public void onClick(View v) {

    }

    private class MyOnFlexboxSubscribeListener implements OnFlexboxSubscribeListener<String> {
        @Override
        public void onSubscribe(List<String> selectedItem) {
            btnCount.setText("已选择" + selectedItem.size() + "个");
        }
    }
}
