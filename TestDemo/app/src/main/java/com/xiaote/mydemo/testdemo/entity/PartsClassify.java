package com.xiaote.mydemo.testdemo.entity;

import android.text.TextUtils;

import com.chad.library.adapter.base.entity.AbstractExpandableItem;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.xiaote.mydemo.testdemo.adapter.PartsAdapter;

/**
 * @author ：xiaote
 * @date ：2019-09-09 09：19
 * 邮箱   ：xiao_t@hks360.com
 */
public class PartsClassify extends AbstractExpandableItem<PartsCategory> implements MultiItemEntity {
    private String flno;            // 分类编号
    private String nflno;            // 上级分类编号
    private String fldm;        //分类代码
    private String flname;        // 分类名称
    private int count;


    public String getFlno() {
        return flno == null ? "" : flno;
    }

    public void setFlno(String flno) {
        this.flno = flno;
    }

    public String getNflno() {
        return nflno == null ? "" : nflno;
    }

    public void setNflno(String nflno) {
        this.nflno = nflno;
    }

    public String getFldm() {
        return fldm == null ? "" : fldm;
    }

    public void setFldm(String fldm) {
        this.fldm = fldm;
    }

    public String getFlname() {
        return flname == null ? "" : flname;
    }

    public void setFlname(String flname) {
        this.flname = flname;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isAll() {
        return TextUtils.equals("全部", getFlname());
    }

    public String getClassifyContent() {
        return isAll() ? flname : fldm + "-" + flname;
    }

    public String getClassifyCount() {
        return fldm + "-" + flname + "(" + count + ")";
    }

    @Override
    public int getLevel() {
        return PartsAdapter.ITEM_CLASSIFY;
    }

    @Override
    public int getItemType() {
        return PartsAdapter.ITEM_CLASSIFY;
    }
}
