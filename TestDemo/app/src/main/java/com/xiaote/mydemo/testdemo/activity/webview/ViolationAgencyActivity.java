package com.xiaote.mydemo.testdemo.activity.webview;

import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;

import com.hks360.library.xlog.XLog;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.util.MyUtil;

/**
 * @author Xiaote
 * @date 2018-09-05 10：25
 * @email xiao_t@hks360.com
 */
public class ViolationAgencyActivity extends BaseWebViewActivity {

    @Override
    public int getTitleStringId() {
        return R.string.violation_agency;
    }

    @Override
    public void loadUrl() {
        String loadUrl = MyUtil.getViolationAgency("6001");
        webView.loadUrl(loadUrl);
    }

    @Override
    public void setWebViewClient() {
        webView.setWebViewClient(new MyWebViewClient());
    }

    private class MyWebViewClient extends BaseWebViewClient {

        @Override
        public boolean modifyOverrideUrlLoading(WebView view, String url) {
            XLog.e("url=>" + url);
            if (url.contains("tel")) {
                goCallPhone(url);
                return true;
            }
            if (url.startsWith("weixin://wap/pay?")) {
                goyPay(1, url);
                return true;
            }
            if (url.startsWith("alipays://platformapi/startApp")) {
                goyPay(2, url);
                return true;
            }
            return false;
        }
    }


    private void goCallPhone(String url) {
        String mobile = url.substring(url.lastIndexOf("/") + 1);
        Intent dialIntent = new Intent(Intent.ACTION_DIAL, Uri.parse(mobile));
        startActivity(dialIntent);
    }

    private void goyPay(int type, String url) {
        try {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            startActivity(intent);
        } catch (Exception e) {
            if (type == 1) {
                showToastMsg("请安装微信最新版");
            }
        }
    }


}
