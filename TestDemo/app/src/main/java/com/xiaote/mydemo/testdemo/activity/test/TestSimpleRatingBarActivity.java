package com.xiaote.mydemo.testdemo.activity.test;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.fragment.ratingbar.AnimatedFragment;
import com.xiaote.mydemo.testdemo.fragment.ratingbar.BackgroundColorsFragment;
import com.xiaote.mydemo.testdemo.fragment.ratingbar.BorderWidthFragment;
import com.xiaote.mydemo.testdemo.fragment.ratingbar.CornerRadiusFragment;
import com.xiaote.mydemo.testdemo.fragment.ratingbar.NumberOfStarsFragment;
import com.xiaote.mydemo.testdemo.fragment.ratingbar.RatingChangedListenerFragment;
import com.xiaote.mydemo.testdemo.fragment.ratingbar.SizeOfStarsFragment;
import com.xiaote.mydemo.testdemo.fragment.ratingbar.StarsSeparationFragment;
import com.xiaote.mydemo.testdemo.fragment.ratingbar.StepSizeAndGravityFragment;
import com.xiaote.mydemo.testdemo.view.NonSwipeableViewPager;

/**
 * @author Xiaote
 * @date 2018-09-05 15：39
 * @email xiao_t@hks360.com
 */
public class TestSimpleRatingBarActivity extends BaseActivity {
    private NonSwipeableViewPager viewPager;
    private TabLayout tabLayout;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_simpleratingbar;
    }

    @Override
    public int getTitleStringId() {
        return 0;
    }

    @Override
    public void setupView() {
        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tab_layout);
    }

    @Override
    public void initData() {
        viewPager.setAdapter(new MyFragmentPagerAdapter(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
        getSupportActionBar().setElevation(0f);
    }

    @Override
    public void addListener() {

    }

    @Override
    public void onClick(View v) {

    }

    private class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new AnimatedFragment();
                case 1:
                    return new NumberOfStarsFragment();
                case 2:
                    return new SizeOfStarsFragment();
                case 3:
                    return new StarsSeparationFragment();
                case 4:
                    return new BorderWidthFragment();
                case 5:
                    return new CornerRadiusFragment();
                case 6:
                    return new StepSizeAndGravityFragment();
                case 7:
                    return new BackgroundColorsFragment();
                default:
                    return new RatingChangedListenerFragment();
            }
        }

        @Override
        public int getCount() {
            return 9;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Animated";
                case 1:
                    return "Number of Stars";
                case 2:
                    return "Size of Stars";
                case 3:
                    return "Separation";
                case 4:
                    return "Border width";
                case 5:
                    return "Corner radius";
                case 6:
                    return "Step size and Gravity";
                case 7:
                    return "Background color";
                default:
                    return "Rating changed Listener";
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }
}
