package com.xiaote.mydemo.testdemo.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.hks360.library.recyclerview.divider.DividerListItemDecoration;
import com.tbruyelle.rxpermissions2.Permission;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.flyco.TestFlycoActivity;
import com.xiaote.mydemo.testdemo.activity.oss.OssTestActivity;
import com.xiaote.mydemo.testdemo.activity.parts.TestPartsInfoActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestAutoCompleteActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestCollectionArraySortActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestDialogUtilActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestDistanceLearningActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestExpandableRecyclerViewActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestExpandableTextActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestFlexBoxActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestFormSyncLayoutActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestGlideActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestMNProgressHUDActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestMaintainProjectActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestMaterialFilePickerActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestMaterialSpinnerActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestNiceDialogActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestNumberProgressBarActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestOpenPdfActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestPasswordShowHideActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestPickerViewActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestRetrofitTokenActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestSimpleRatingBarActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestStateButtonActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestVideoPlayerActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestWebViewActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestWorkBenchActivity;
import com.xiaote.mydemo.testdemo.activity.test.TestWorkOrderActivity;
import com.xiaote.mydemo.testdemo.adapter.MainAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * @author by Xiaote<br>
 */
public class MainActivity extends BaseActivity {
    private RecyclerView mMainRv;
    private MainAdapter mAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public int getTitleStringId() {
        return R.string.main_title;
    }

    @Override
    public void setupView() {
        mMainRv = findViewById(R.id.main_rv);
    }

    @Override
    public void initData() {
        String[] arr = getResources().getStringArray(R.array.main_list);
        List<String> list = new ArrayList<>(Arrays.asList(arr));
        mMainRv.setLayoutManager(new LinearLayoutManager(this));
        mMainRv.addItemDecoration(new DividerListItemDecoration(this));
        mAdapter = new MainAdapter(list);
        mMainRv.setAdapter(mAdapter);
        requestPermission();
    }

    private void requestPermission() {
        RxPermissions rxPermission = new RxPermissions(this);
        Disposable disposable = rxPermission.requestEach(Manifest.permission.WRITE_EXTERNAL_STORAGE,//  Write access
                Manifest.permission.READ_EXTERNAL_STORAGE).subscribe(new Consumer<Permission>() {
            @Override
            public void accept(Permission permission) throws Exception {

            }
        });
        mDisposables.add(disposable);
    }

    @Override
    public void addListener() {
        mAdapter.setOnItemClickListener(new MyOnItemClickListener(this));
    }

    @Override
    public void onClick(View v) {

    }

    private class MyOnItemClickListener implements BaseQuickAdapter.OnItemClickListener {
        private Activity activity;

        public MyOnItemClickListener(Activity activity) {
            this.activity = activity;
        }

        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            Intent intent = new Intent();
            switch (position) {
                case 0:
                    intent.setClass(activity, TestAutoCompleteActivity.class);
                    break;
                case 1:
                    intent.setClass(activity, TestStateButtonActivity.class);
                    break;
                case 2:
                    intent.setClass(activity, TestVideoPlayerActivity.class);
                    break;
                case 3:
                    intent.setClass(activity, TestPickerViewActivity.class);
                    break;
                case 4:
                    intent.setClass(activity, TestExpandableRecyclerViewActivity.class);
                    break;
                case 5:
                    intent.setClass(activity, TestExpandableTextActivity.class);
                    break;
                case 6:
                    intent.setClass(activity, TestNumberProgressBarActivity.class);
                    break;
                case 7:
                    intent.setClass(activity, TestMaterialFilePickerActivity.class);
                    break;
                case 8:
                    intent.setClass(activity, TestMaterialSpinnerActivity.class);
                    break;
                case 9:
                    intent.setClass(activity, TestMNProgressHUDActivity.class);
                    break;
                case 10:
                    intent.setClass(activity, TestGlideActivity.class);
                    break;
                case 11:
                    intent.setClass(activity, TestNiceDialogActivity.class);
                    break;
                case 12:
                    intent.setClass(activity, TestRetrofitTokenActivity.class);
                    break;
                case 13:
                    intent.setClass(activity, TestFlexBoxActivity.class);
                    break;
                case 14:
                    intent.setClass(activity, TestWebViewActivity.class);
                    break;
                case 15:
                    intent.setClass(activity, TestSimpleRatingBarActivity.class);
                    break;
                case 16:
                    intent.setClass(activity, TestDistanceLearningActivity.class);
                    break;
                case 17:
                    intent.setClass(activity, TestWorkBenchActivity.class);
                    break;
                case 18:
                    intent.setClass(activity, TestPasswordShowHideActivity.class);
                    break;
                case 19:
                    intent.setClass(activity, TestOpenPdfActivity.class);
                    break;
                case 20:
                    intent.setClass(activity, TestMaintainProjectActivity.class);
                    break;
                case 21:
                    intent.setClass(activity, TestPartsInfoActivity.class);
                    break;
                case 22:
                    intent.setClass(activity, TestDialogUtilActivity.class);
                    break;
                case 23:
                    intent.setClass(activity, OssTestActivity.class);
                    break;
                case 24:
                    intent.setClass(activity, TestWorkOrderActivity.class);
                    break;
                case 25:
                    intent.setClass(activity, TestCollectionArraySortActivity.class);
                    break;
                case 26:
                    intent.setClass(activity, TestFlycoActivity.class);
                    break;
                case 27:
                    intent.setClass(activity, TestFormSyncLayoutActivity.class);
                    break;
                default:
                    break;
            }
            startActivity(intent);
        }
    }
}
