package com.xiaote.mydemo.testdemo.activity.test;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hks360.library.glide.GlideApp;
import com.hks360.library.util.DateUtil;
import com.hks360.library.util.UIUtil;
import com.hks360.library.xlog.XLog;
import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;

import java.util.Calendar;

/**
 * @author Xiaote
 * @date 2018/7/5 13:52
 * @email xiao_t@hks360.com
 */
public class TestGlideActivity extends BaseActivity {
    private ImageView mImageView1;
    private ImageView mImageView2;
    private ImageView mImageView3;
    private TextView mSaveImageTv;
    private TextView mText1;
    private Button browserOpenBtn;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_glide;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_title_glide;
    }

    @Override
    public void setupView() {
        mImageView1 = findViewById(R.id.glide_img1);
        mImageView2 = findViewById(R.id.glide_img2);
        mImageView3 = findViewById(R.id.glide_img3);
        mSaveImageTv = findViewById(R.id.save_image_tv);
        mText1 = findViewById(R.id.text1);
        browserOpenBtn = findViewById(R.id.btn_browser_open);
    }

    @Override
    public void initData() {
        //带请求头
        String url = "http://inthecheesefactory.com/uploads/source/glidepicasso/cover.jpg";
        GlideApp.with(this).load(url).fitCenter().into(mImageView3);
        //不带请求头
        String url1 = "http://inthecheesefactory.com/uploads/source/glidepicasso/cover.jpg";
        GlideApp.with(this)
                .load(url1)
                .placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher_round)
                .fitCenter().into(mImageView1);
        GlideApp.with(this).load(url1).fitCenter().into(mImageView2);
        showText();
    }

    @Override
    public void addListener() {
        mSaveImageTv.setOnClickListener(this);
        browserOpenBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_image_tv:
                saveImage();
                break;
            case R.id.btn_browser_open:
                browserOpen();
                break;

        }
    }

    private void saveImage() {
        String savePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                DateUtil.parseDateByCurrentTime1() + ".jpg";
        XLog.e("savePath=>" + savePath);
        UIUtil.saveViewCache(mImageView3, savePath);
    }

    private void showText() {
        long createTime = DateUtil.parseStrToDateTime("2019-05-24 15:18:10", "yyyy-MM-dd HH:mm:ss");
        long compareTime1 = DateUtil.parseStrToDateTime("2019-06-01", "yyyy-MM-dd");
        Calendar compareCalendar = Calendar.getInstance();
        compareCalendar.set(2019, 5, 1, 0, 0, 0);
        compareCalendar.add(Calendar.MILLISECOND, 0);
        String text = "创建时间:" + createTime + ",比较时间1：" + compareTime1 + ",比较时间2:" + compareCalendar.getTimeInMillis();
        mText1.setText(text);
    }

    private void browserOpen() {
        Uri uri = Uri.parse("http://kuaixiuge.com/app.html");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
