package com.xiaote.mydemo.testdemo.activity.test;

import android.content.Intent;
import android.view.View;
import android.widget.Button;

import com.xiaote.mydemo.testdemo.R;
import com.xiaote.mydemo.testdemo.activity.BaseActivity;
import com.xiaote.mydemo.testdemo.activity.webview.TestDistanceTeachingActivity;
import com.xiaote.mydemo.testdemo.activity.webview.TestPreventingHotlinkingWebViewActivity;
import com.xiaote.mydemo.testdemo.activity.webview.ViolationAgencyActivity;

/**
 * @author Xiaote
 * @date 2018-09-05 09：54
 * @email xiao_t@hks360.com
 */
public class TestWebViewActivity extends BaseActivity {
    private Button btnViolationAgency;
    private Button btnPreventingHotlinking;
    private Button btnDistanceTeaching;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test_go_webview;
    }

    @Override
    public int getTitleStringId() {
        return R.string.test_title_webview;
    }

    @Override
    public void setupView() {
        btnViolationAgency = findViewById(R.id.btn_test_violation_agency);
        btnPreventingHotlinking = findViewById(R.id.btn_test_preventing_hotlinking_webview);
        btnDistanceTeaching = findViewById(R.id.btn_test_distance_teaching_webview);
    }

    @Override
    public void initData() {

    }

    @Override
    public void addListener() {
        btnViolationAgency.setOnClickListener(this);
        btnPreventingHotlinking.setOnClickListener(this);
        btnDistanceTeaching.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_test_violation_agency:
                goViolationAgency();
                break;
            case R.id.btn_test_preventing_hotlinking_webview:
                goPreventingHotlinkingWebView();
                break;
            case R.id.btn_test_distance_teaching_webview:
                goDistanceTeaching();
                break;
            default:
                break;
        }
    }

    private void goViolationAgency() {
        Intent intent = new Intent();
        intent.setClass(this, ViolationAgencyActivity.class);
        startActivity(intent);
    }

    private void goPreventingHotlinkingWebView() {
        Intent intent = new Intent();
        intent.setClass(this, TestPreventingHotlinkingWebViewActivity.class);
        startActivity(intent);
    }

    private void goDistanceTeaching() {
        Intent intent = new Intent();
        intent.setClass(this, TestDistanceTeachingActivity.class);
        startActivity(intent);
    }
}
