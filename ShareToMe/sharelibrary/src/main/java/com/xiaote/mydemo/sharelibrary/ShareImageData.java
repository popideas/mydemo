package com.xiaote.mydemo.sharelibrary;

import android.support.annotation.NonNull;

public class ShareImageData extends BaseShareData {

    private String name;

    private String path;

    public ShareImageData(String name, String path) {
        this.name = name;
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


    @NonNull
    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", path='" + path;
    }

    @Override
    public ShareDataType getDataType() {
        return ShareDataType.SHARE_IMG_DATA;
    }
}
