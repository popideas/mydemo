package com.xiaote.mydemo.sharelibrary;


import android.support.annotation.NonNull;

import java.util.List;


/**
 * 多组图片
 */
public class ShareMultipleImagesData extends BaseShareData {


    private List<ShareImageData> imgPath;


    public ShareMultipleImagesData() {
    }

    public ShareMultipleImagesData(List<ShareImageData> imgPath) {
        this.imgPath = imgPath;
    }


    public List<ShareImageData> getImgPath() {
        return imgPath;
    }

    public void setImgPath(List<ShareImageData> imgPath) {
        this.imgPath = imgPath;
    }

    @NonNull
    @Override
    public String toString() {
        return "ShareMultipleImagesData{" +
                "imgPath=" + imgPath +
                '}';
    }

    @Override
    public ShareDataType getDataType() {
        return ShareDataType.SHARE_MULTIPLE_IMAGES_DATA;
    }


}
