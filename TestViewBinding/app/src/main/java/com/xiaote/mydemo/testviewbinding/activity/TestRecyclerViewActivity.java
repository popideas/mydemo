package com.xiaote.mydemo.testviewbinding.activity;

import android.os.Bundle;

import com.xiaote.mydemo.testviewbinding.adapter.LanguageAdapter;
import com.xiaote.mydemo.testviewbinding.databinding.ActivityTestRecyclerviewBinding;
import com.xiaote.mydemo.testviewbinding.entity.Language;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * @author ：xiaote
 * @date ：2020-03-03 11：04
 * 邮箱   ：xiao_t@hks360.com
 */
public class TestRecyclerViewActivity extends AppCompatActivity {
    private ActivityTestRecyclerviewBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivityTestRecyclerviewBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());
        initView();
    }

    private void initView() {
        List<Language> dataList = new ArrayList<>();
        dataList.add(new Language("C"));
        dataList.add(new Language("C#"));
        dataList.add(new Language("C++"));
        dataList.add(new Language("Java"));
        dataList.add(new Language("Android"));
        dataList.add(new Language("Kotlin"));
        dataList.add(new Language("Object-C"));
        dataList.add(new Language("Swift"));
        dataList.add(new Language("PHP"));
        dataList.add(new Language("HTML"));
        dataList.add(new Language("Python"));
        dataList.add(new Language("Delphi"));
        LanguageAdapter adapter = new LanguageAdapter(dataList);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recyclerView.setAdapter(adapter);
    }
}
