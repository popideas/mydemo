package com.xiaote.mydemo.testviewbinding.entity;

/**
 * @author ：xiaote
 * @date ：2020-05-28 11：27
 * 邮箱   ：xiao_t@hks360.com
 */
public class Language {
    private String name;

    public Language() {
    }

    public Language(String name) {
        this.name = name;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
