package com.xiaote.mydemo.testviewbinding.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.xiaote.mydemo.testviewbinding.R;
import com.xiaote.mydemo.testviewbinding.entity.Language;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * @author ：xiaote
 * @date ：2020-05-28 11：28
 * 邮箱   ：xiao_t@hks360.com
 */
public class LanguageAdapter extends BaseQuickAdapter<Language, BaseViewHolder> {
    public LanguageAdapter(@Nullable List<Language> data) {
        super(R.layout.item_recyclerview, data);
    }

    @Override

    protected void convert(@NotNull BaseViewHolder helper, Language language) {
        helper.setText(R.id.name_text, language.getName());
    }
}
