package com.xiaote.mydemo.testviewbinding.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.xiaote.mydemo.testviewbinding.databinding.ActivityMainBinding;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());
        mBinding.testOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, TestRecyclerViewActivity.class);
                startActivity(intent);
            }
        });
    }
}
